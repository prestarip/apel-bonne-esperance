<?php
	/**
	 * Created by PhpStorm.
	 * User: Vincent
	 * Date: 12/06/2018
	 * Time: 13:49
	 */

	//Déclaration du namespace
	namespace Apel;

	use Apel\Dll\Controleur\ControleurNews;
	use Apel\Dll\Framework\cls_ConstruitTemplate;
	use Apel\Dll\Framework\Config;
	use Apel\Dll\Framework\Fonctions;

	return new class()
	{
		private $page;
		private $data;

		public function __construct($layout = true)
		{
			if(Fonctions::isConnected())
			{
				$this->getData();
				$this->generatePage($layout);
			}
			else
			{
				$_SESSION['message'] = 'Vous devez être connecté pour voir cette page !';
				header('Location: /index.php');
			}
		}

		private function getData()
		{
			$this->data                   = array();
			$controleurNews               = new ControleurNews();
			$listeActualite               = $controleurNews->recupererActualite(0, false);
			$this->data['listeActualite'] = $listeActualite;
		}

		private function getActualiteHtmlFormat()
		{
			if(count($this->data['listeActualite']) > 0)
			{
				$colonnes     = array_keys($this->data['listeActualite'][0]);
				$tableau      = '';
				$tableau      .= '<table>';
				$colonnesHtml = '';
				$nbColonne    = count($colonnes);
				$i            = 0;
				foreach($colonnes as $value)
				{
					$i++;
					$colonnesHtml .= '<th ' . (($i == $nbColonne) ? 'colspan="2"' : '') . ' >' . $value . '</th>';
				}
				$tableau .= '<thead><tr>' . $colonnesHtml . '</tr></thead><tbody>';

				foreach($this->data['listeActualite'] as $element)
				{
					$tableau .= '<tr>';
					foreach($element as $key => $value)
					{
						$tableau .= '<td data-key="' . $key . '" >' . $value . '</td>';
					}
					$tableau .= '
					<td class="actions">
						<a href="?" class="edit-item" title="Edit">Edit</a>
						<a href="?" class="remove-item" title="Remove">Remove</a>
					</td>';
					$tableau .= '</tr>';

				}
				$tableau .= '</body><tfoot><tr>' . $colonnesHtml . '</tr></tfoot>';
				$tableau .= '</table>';
			}

			return $tableau;
		}

		private function generatePage(bool $layout)
		{
			//Initialisation du template
			$this->page = new cls_ConstruitTemplate(Config::getAdresse('GESTION_EVENEMENT'), $layout);
			$this->page->remplacePage('#begin_section#', (($layout) ? '<section>' : ''));
			$this->page->remplacePage('#title#', 'Gestion des événements');
			$this->page->remplacePage('#tableauevenements#', $this->getActualiteHtmlFormat());

			$this->page->remplacePage('#end_section#', (($layout) ? '</section>' : ''));

			if($layout)
			{
				echo $this->page->getPage();
			}
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return $this->page;
		}
	};