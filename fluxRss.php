<?php
	/**
	 * Created by PhpStorm.
	 * User: pythonirc
	 * Date: 03/07/18
	 * Time: 08:53
	 */

	namespace Apel;

	use Apel\Dll\Framework\
	{
		Config
	};
	use Apel\Dll\Page\cls_rss;

	return new class()
	{
		/** @var \Apel\Dll\Page\cls_rss $page */
		private $page;

		public function __construct($layout = true)
		{
			$this->generatePage($layout);
		}

		private function generatePage(bool $layout)
		{
			// contenus de la page
			$titre   = 'Flux RSS';

			//Initialisation du template
			$this->page = new cls_rss(Config::getAdresse('RSS'), $layout);

			$this->page->remplacePage('#begin_section#', (($layout) ? '<section>' : ''));

			//Appel des différents fonctions permettant de générer le code HTML
			$this->page->titre($titre);

			$this->page->remplacePage('#end_section#', (($layout) ? '</section>' : ''));
			if($layout)
			{
				echo $this->afficherPage();
			}
		}

		private function afficherPage()
		{
			//Affichage de la page
			return $this->page->getPage();
		}

		public function __toString()
		{
			return $this->afficherPage();
		}
	};