<?php
	/**
	 * Created by PhpStorm.
	 * User: Vincent
	 * Date: 12/06/2018
	 * Time: 13:33
	 */

	//Déclaration du namespace
	namespace Apel;

	use Apel\Dll\Framework\cls_ConstruitTemplate;
	use Apel\Dll\Framework\Config;

	return new class()
	{
		private $page;

		public function __construct($layout = true)
		{
			$this->generatePage($layout);
		}

		private function generatePage(bool $layout)
		{
			//Initialisation du template
			$this->page = new cls_ConstruitTemplate(Config::getAdresse('DROITS'), $layout);

			$this->page->remplacePage('#begin_section#', (($layout) ? '<section>' : ''));

			$this->page->remplacePage('#title#', 'Droits');
			$this->page->remplacePage('#boutongestiondroit#', 'Bouton de gestion des droits');
			$this->page->remplacePage('#listedroits#', 'Tableau avec la liste des différents groupes de droit');

			$this->page->remplacePage('#end_section#', (($layout) ? '</section>' : ''));

			if($layout)
			{
				echo $this->page->getPage();
			}
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return $this->page;
		}
	};