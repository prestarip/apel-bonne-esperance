<?php
	/**
	 * Created by PhpStorm.
	 * User: GuillaumeFixe
	 * Date: 03/07/2018
	 * Time: 15:33
	 */

	namespace Apel;

	use Apel\Dll\Business\cls_Compte;
	use Apel\Dll\Framework\cls_ConstruitTemplate;
	use Apel\Dll\Framework\Config;
	use Apel\Dll\Framework\Dao;
	use Apel\Dll\Framework\Fonctions;

	return new class()
	{
		/** @var cls_ConstruitTemplate $aside */
		private $aside;

		public function __construct()
		{
			$this->generatePage();
		}

		private function generatePage()
		{
			// Génération de la page
			/** @var cls_ConstruitTemplate $pageAside */
			$aside = new cls_ConstruitTemplate(Config::getAdresse('SIDE_BAR'), false);

			$aside->remplacePage('#calendrier#', 'vite'); // <img src="Images/calendar.jpg">
			$aside->remplacePage('#chat#', (Fonctions::isConnected() ? $this->generateChat() : null));
			//Affichage du contenu
			$this->aside = $aside->getPage();
		}

		private function generateChat()
		{
			$dao         = new Dao();
			$listeCompte = $dao->getAllCompte();
			$chat        = '';
			/**
			 * @var int        $key
			 * @var cls_Compte $value
			 */
			foreach($listeCompte as $key => $value)
			{
				$element = '<div class="unePersonne" data-id="' . $value->getIdCompte() . '" data-nom="' . $value->getNom() . '" data-prenom="' . $value->getPrenom() . '">
							<figure>
								<img src="" alt=""/>
							</figure>
							<h5>' . $value->getNom() . ' ' . $value->getPrenom() . '</h5>
							<span class="connected" >
							</span>
						</div>' . "\n";
				$chat    .= $element;
			}

			return $chat;
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return $this->aside;
		}
	};