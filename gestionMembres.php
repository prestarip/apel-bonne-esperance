<?php
	/**
	 * Created by PhpStorm.
	 * User: Vincent
	 * Date: 12/06/2018
	 * Time: 14:26
	 */

	//Déclaration du namespace
	namespace Apel;

	use Apel\Dll\Framework\cls_ConstruitTemplate;
	use Apel\Dll\Framework\Config;

	return new class()
	{
		private $page;

		public function __construct($layout = true)
		{
			$this->generatePage($layout);
		}

		private function generatePage(bool $layout)
		{
			//Initialisation du template
			$this->page = new cls_ConstruitTemplate(Config::getAdresse('GESTION_MEMBRES'), $layout);

			$this->page->remplacePage('#begin_section#', (($layout) ? '<section>' : ''));

			$this->page->remplacePage('#title#', 'Gestion des membres');
			$this->page->remplacePage('#boutongestionutil#', 'Boutons pour la gestion des membres');
			$this->page->remplacePage('#listeutilisateurs#', 'tableau avec la liste des utilisateurs');
			$this->page->remplacePage('#generationtoken#', 'cadre avec les boutons pour générer des token d\'inscription');

			$this->page->remplacePage('#end_section#', (($layout) ? '</section>' : ''));

			if($layout)
			{
				echo $this->page->getPage();
			}
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return $this->page;
		}
	};