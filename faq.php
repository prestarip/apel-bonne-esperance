<?php
	/**
	 * Created by PhpStorm.
	 * User: Vincent
	 * Date: 12/06/2018
	 * Time: 13:27
	 */

	//Déclaration du namespace
	namespace Apel;

	use Apel\Dll\Framework\cls_ConstruitTemplate;
	use Apel\Dll\Framework\Config;

	return new class()
	{
		private $page;

		public function __construct($layout = true)
		{
			$this->generatePage($layout);
		}

		private function generatePage(bool $layout)
		{
			//Initialisation du template
			$this->page = new cls_ConstruitTemplate(Config::getAdresse('FAQ'), $layout);

			$this->page->remplacePage('#begin_section#', (($layout) ? '<section>' : ''));

			$this->page->remplacePage('#title#', 'Foire aux questions');

			$this->page->remplacePage('#listes_faq_epingles#', 'liste topic faq épinglés');
			$this->page->remplacePage('#listes_faq_dispo#', 'Liste des topic faq que l\'utilisateur a le droit de voir');

			$this->page->remplacePage('#end_section#', (($layout) ? '</section>' : ''));

			if($layout)
			{
				echo $this->page->getPage();
			}
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return $this->page;
		}
	};