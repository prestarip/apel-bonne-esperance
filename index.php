<?php
	/**
	 * Created by PhpStorm.
	 * User: Guillaume
	 * Date: 22/05/2018
	 * Time: 12:42
	 * Last Updated : Vincent, 12/06/2018
	 */

	//Déclaration du namespace
	namespace Apel;

	use Apel\Dll\Framework\cls_ConstruitTemplate;
	use Apel\Dll\Framework\Config;

	return new class()
	{
		/** @var cls_ConstruitTemplate $page */
		private $page;

		public function __construct($layout = true)
		{
			$layout = (isset($_SESSION['noLayout']) && $_SESSION['noLayout'] <> null) ? false : $layout;
			$this->generatePage($layout);
		}

		private function generatePage(bool $layout)
		{
			//Initialisation du template
			$this->page = new cls_ConstruitTemplate(Config::getAdresse('INDEX'), $layout);

			//Appel des différents fonctions permettant de générer le code HTML
			$this->page->remplacePage('#title#', 'APEL Bonne Esperance');

			$_SESSION['noLayout'] = true;
			$this->page->remplacePage('#news#', Config::getRequireOnce('NEWS_PHP'));
			$_SESSION['noLayout'] = false;
			unset($_SESSION['noLayout']);

			if($layout)
			{
				echo $this->afficherPage();
			}
		}

		private function afficherPage()
		{
			//Affichage de la page
			return $this->page->getPage();
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return $this->afficherPage();
		}
	};