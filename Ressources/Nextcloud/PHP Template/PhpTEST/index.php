<?php

	spl_autoload_register(function($class) {
		require_once './obj/' . $class . '.php';
	}
	);
	/*
	  $Page = new cls_Index('./template/init.html');
	
	  $Page->titre();
	  $Page->formBegin();
	  $Page->login();
	  $Page->password();
	  $Page->submit();
	  $Page->formEnd();
	  $Page->select();
	  $Page->test();
	
	  echo $Page->getPage(); */

	new class()
	{
		private $Page;

		function __construct()
		{
			$this->Page = new cls_Index('./template/init.html');
			$this->generatePage();
		}

		private function generatePage()
		{
			$this->Page->titre();
			$this->Page->formBegin();
			$this->Page->login();
			$this->Page->password();
			$this->Page->submit();
			$this->Page->formEnd();
			$this->Page->select();
			$this->Page->table1();
			$this->Page->table2();
			$this->Page->image();
			$this->Page->a_lien();
			$this->Page->a_img();
			//$this->Page->li();
			$this->Page->ul();

			echo $this->Page->getPage();
		}
	};
