<?php

	class cls_ConstruitTemplate
	{
// <editor-fold desc="Variables de la classe">

		private $c_screen;
		private $c_label_Str;
		private $c_label_For;
		private $c_label_Value;
		private $c_label_Format;
		private $c_optgrp_Str;
		private $c_optgrp_Format;
		private $c_option_Str;
		private $c_option_Value;
		private $c_option_Format;
		private $c_select_Str;
		private $c_select_Format;
		private $c_table_Format;
		private $c_table_Str;
		private $c_table_TH;
		private $c_table_TD;
		private $c_table_Caption;
		private $c_caption_Str;
		private $c_caption_For;
		private $c_caption_Value;
		private $c_caption_Format;
		private $c_li_Str;
		private $c_li_Value;
		private $c_li_Format;
		private $c_ul_Str;
		private $c_ul_Value;
		private $c_ul_Format;
		private $c_img_Str;
		private $c_a_Str;
		private $c_a_Value;
		private $c_a_Format;
// </editor-fold>
// 
// <editor-fold desc="Constantes de la classe">

		const ACTION       = 'action';
		const METHODE      = 'methode';
		const TYPE         = 'type';
		const NOM          = 'name';
		const ID           = 'id';
		const CLASSE       = 'class';
		const REQUIS       = 'required';
		const AUTOFOCUS    = 'autofocus';
		const LIBELLE      = 'label';
		const VALEUR       = 'value';
		const OPTIONS      = 'tabOption';
		const OPTGRP       = 'OPTGRP';
		const SELECTIONNER = 'selected';
		const GRISER       = 'disabled';
		const THEAD        = 'thead';
		const TBODY        = 'tbody';
		const TFOOT        = 'tfoot';
		const CAPTION      = 'caption';
		const TH           = 'th';
		const TD           = 'td';
		const SOURCE       = 'src';
		const ALT          = 'alt';
		const IMAGE        = 'img';
		const HREF         = 'href';
		const TARGET       = 'target';
		const A            = 'a';
		const LI           = 'li';
		const UL           = 'ul';
// </editor-fold>
//
// <editor-fold desc="Constructeur">

		/**
		 * Constructeur
		 *
		 * @param string $pageHtml Page HTML contenant les balises à remplacer
		 *
		 * @author Mathieu
		 */
		function __construct($pageHtml)
		{
			$this->c_screen = file_get_contents($pageHtml);

			$this->c_label_Format   = 'for="%1$s">%2$s</label>';
			$this->c_select_Format  = '>%1$s</select>';
			$this->c_optgrp_Format  = '>%1$s</optgroup>';
			$this->c_option_Format  = '>%1$s</option>';
			$this->c_table_Format   = '%1$s%2$s</table>';
			$this->c_caption_Format = 'for="%1$s">%2$s</caption>';
			$this->c_li_Format      = '>%1$s</li>';
			$this->c_a_Format       = '>%1$s</a>';
			$this->c_ul_Format      = '>%1$s</ul>';

			$this->razVariable();
		}
// </editor-fold>
//  
// <editor-fold desc="Mise en page">

		/**
		 * Retourne la page HTML à afficher
		 * @return string Page HTML à afficher
		 * @author Mathieu
		 */
		function getPage()
		{
			return $this->c_screen;
		}

		/**
		 * Remplace le balise de la page HTML par le contenu
		 * puis remet à vide les variables
		 *
		 * @param type $balise  Balise à remplacer
		 * @param type $contenu Contenu à insérer dans la page
		 *
		 * @author Mathieu
		 */
		function remplacePage($balise, $contenu)
		{
			$this->c_screen = str_replace($balise, $contenu, $this->c_screen);
			$this->razVariable();
		}
//    
// </editor-fold>
//  
// <editor-fold desc="Form">

		/**
		 * Génère une balise <form> (Début)
		 *
		 * @param string $balise Balise à remplacer
		 * @param string $tab    Tableau des arguments de la balise
		 *
		 * @author Mathieu
		 */
		function formBeginBalise($balise, $tab)
		{
			$l_str = '<form ';

			foreach($tab as $key => $value)
			{
				$l_str .= $key . '="' . $value . '" ';
			}

			$l_str .= '>';

			$this->remplacePage($balise, $l_str);
		}

		/**
		 * Génère une balise </form> (Fin)
		 *
		 * @param string $Balise Balise à remplacer
		 *
		 * @author Mathieu
		 */
		function formEndBalise($Balise)
		{
			$this->remplacePage($Balise, '</form>');
		}
// </editor-fold>
//
// <editor-fold desc="Label">

		/**
		 * Créer une balise <label>
		 * qui sera généré par la fonction inputBalise()
		 *
		 * @param array () $Tab Tableau des arguments de la balise
		 *
		 * @author Mathieu
		 */
		function labelBalise($Tab)
		{
			$this->c_label_Str = '<label ';

			foreach($Tab as $key => $value)
			{
				if($key == self::VALEUR)
				{
					$this->c_label_Value = $value;
				}
				else
				{
					$this->c_label_Str .= $key . '="' . $value . '" ';
				}
			}

			$this->c_label_Str .= sprintf($this->c_label_Format, $this->c_label_For, $this->c_label_Value);
		}
// </editor-fold>
//
// <editor-fold desc="Input">

		/**
		 * Génère une balise <input>
		 * et une balise <label> si nécessaire
		 *
		 * @param string $p_Balise Balise à remplacer
		 * @param string $p_Tab    Tableau des arguments de la balise
		 *
		 * @author Mathieu
		 */
		function inputBalise($p_Balise, $p_Tab)
		{
			$l_input = '<input ';

			foreach($p_Tab as $key => $value)
			{
				$l_input .= $this->inputForEach($key, $value);
			}

			$l_input .= '/>';

			$this->remplacePage($p_Balise, $this->c_label_Str . $l_input);

			//$l_str = $this->c_label_Str == '' ? $l_input : $this->c_label_Str . $l_input;
			//$this->remplacePage($p_Balise, $l_str);
		}

//    
// </editor-fold>
//
// <editor-fold desc="Select">
//    
		/**
		 * Génère une balise <select> et les balises <option>
		 *
		 * @param string $balise Balise à remplacer
		 * @param string $tab    Tableau des arguments de la balise
		 *
		 * @author Mathieu
		 */
		function selectBalise($balise, $tab)
		{

			$this->c_select_Str = '<select ';

			foreach($tab as $key => $value)
			{
				$this->selectForEach($key, $value);
			}

			if($this->c_optgrp_Str == '')
			{
				$this->c_select_Str .= sprintf($this->c_select_Format, $this->c_option_Str);
			}
			else
			{
				$this->c_select_Str .= sprintf($this->c_select_Format, $this->c_optgrp_Str);
			}

			$str = $this->c_label_Str == '' ? $this->c_select_Str : $this->c_label_Str . $this->c_select_Str;

			$this->remplacePage($balise, $str);
		}

//    
// </editor-fold>
//
// <editor-fold desc="Table">
//  
		/**
		 * Génère une balise <table>
		 * et toutes les autres balise nécéssaire
		 * <tr>,<th>,<td> ...
		 *
		 * @param type $p_Balise
		 * @param type $p_Tab
		 */
		function tableBalise($p_Balise, $p_Tab)
		{
			$this->c_table_Str = '<table ';

			foreach($p_Tab as $key => $value)
			{
				$this->tableSwitch($key, $value);
			}

			$this->c_table_Str .= '>';

			$this->c_table_Str .= $this->c_caption_Str == '' ? '' : $this->c_caption_Str;

			$this->c_table_Str .= sprintf($this->c_table_Format, $this->c_table_TH, $this->c_table_TD);

			$this->remplacePage($p_Balise, $this->c_table_Str);
		}

//    
// </editor-fold>
//
		function ulBalise($Tab, $p_Balise = '')
		{
			$this->c_ul_Str = '<ul ';

			foreach($Tab as $key => $value)
			{
				$this->ulSwitch($key, $value);
			}

			$l_Value = $this->c_ul_Value == '' ? $this->c_li_Str : $this->c_ul_Value;

			$this->c_ul_Str .= sprintf($this->c_ul_Format, $l_Value);

			if($p_Balise != '')
			{
				$this->remplacePage($p_Balise, $this->c_ul_Str);
			}
		}

//  
		function liBalise($Tab, $p_Balise = '')
		{
			$this->c_li_Str = '<li ';

			foreach($Tab as $key => $value)
			{
				$this->liSwitch($key, $value);
			}

			$l_Value = $this->c_li_Value == '' ? $this->c_a_Str : $this->c_li_Value;

			$this->c_li_Str .= sprintf($this->c_li_Format, $l_Value);

			if($p_Balise != '')
			{
				$this->remplacePage($p_Balise, $this->c_li_Str);
			}
		}

//    
		function aBalise($Tab, $p_Balise = '')
		{
			$this->c_a_Str = '<a ';

			foreach($Tab as $key => $value)
			{
				$this->aSwitch($key, $value);
			}

			$l_Value = $this->c_a_Value == '' ? $this->c_img_Str : $this->c_a_Value;

			$this->c_a_Str .= sprintf($this->c_a_Format, $l_Value);

			if($p_Balise != '')
			{
				$this->remplacePage($p_Balise, $this->c_a_Str);
			}
		}

//
		function imgBalise($Tab, $p_Balise = '')
		{
			$this->c_img_Str = '<img ';

			foreach($Tab as $key => $value)
			{
				$this->c_img_Str .= $key . '="' . $value . '" ';
			}

			$this->c_img_Str .= '/>';

			if($p_Balise != '')
			{
				$this->remplacePage($p_Balise, $this->c_img_Str);
			}
		}
//    
// <editor-fold desc="Contient les fonctions privés de la classe">

		/**
		 * Remet les variables de classe à vide
		 */
		private function razVariable()
		{
			$this->c_label_Str     = '';
			$this->c_label_For     = '';
			$this->c_label_Value   = '';
			$this->c_option_Str    = '';
			$this->c_select_Str    = '';
			$this->c_optgrp_Str    = '';
			$this->c_table_Str     = '';
			$this->c_table_TH      = '';
			$this->c_table_TD      = '';
			$this->c_table_Caption = '';
			$this->c_caption_Str   = '';
			$this->c_caption_For   = '';
			$this->c_caption_Value = '';
			$this->c_li_Str        = '';
			$this->c_li_Value      = '';
			$this->c_img_Str       = '';
			$this->c_a_Str         = '';
			$this->c_a_Value       = '';
		}

		private function ulSwitch($key, $value)
		{
			echo 'ulSW';
			var_dump($key);
			var_dump($value);
			switch($key)
			{
				case self::VALEUR:
					$this->c_ul_Value = $value;
					break;

				case self::LI:
					$this->liBalise($value);
					break;

				case self::A:
					$this->aBalise($value);
					break;

				case self::IMAGE:
					$this->imgBalise($value);
					break;

				default:
					$this->c_ul_Str .= $key . '="' . $value . '" ';
					break;
			}
		}

		private function liSwitch($key, $value)
		{

			switch($key)
			{
				case self::VALEUR:
					$this->c_li_Value = $value;
					break;

				case self::A:
					$this->aBalise($value);
					break;

				case self::IMAGE:
					$this->imgBalise($value);
					break;

				default:
					$this->c_li_Str .= $key . '="' . $value . '" ';
					break;
			}
		}

		private function aSwitch($key, $value)
		{

			switch($key)
			{
				case self::VALEUR:
					$this->c_a_Value = $value;
					break;

				case self::IMAGE:
					$this->imgBalise($value);
					break;

				default:
					$this->c_a_Str .= $key . '="' . $value . '" ';
					break;
			}
		}

		private function thTable($value, $p_balise = '')
		{
			$this->c_table_TH .= $p_balise == '' ? '<tr>' : '<' . $p_balise . '><tr>';

			foreach($value as $valueth)
			{
				$this->c_table_TH .= '<th>' . $valueth . '</th>';
			}

			$this->c_table_TH .= $p_balise == '' ? '</tr>' : '</' . $p_balise . '></tr>';
		}

		private function tdTable($value, $p_balise = '')
		{

			$this->c_table_TD .= $p_balise == '' ? '' : '<' . $p_balise . '>';

			foreach($value as $line)
			{
				$this->c_table_TD .= '<tr>';
				foreach($line as $valuetd)
				{
					$this->c_table_TD .= '<td>' . $valuetd . '</td>';
				}
				$this->c_table_TD .= '</tr>';
			}
			$this->c_table_TD .= $p_balise == '' ? '' : '</' . $p_balise . '>';
		}

		private function tableSwitch($key, $value)
		{
			switch($key)
			{
				case 'th' :
					$this->thTable($value);
					break;

				case 'td' :
					$this->tdTable($value);
					break;

				case 'thead' :
					$this->thTable($value, $key);
					break;

				case 'tfoot':
				case 'tbody':
					$this->tdTable($value, $key);
					break;

				case 'caption':
					$this->captionBalise($value);
					break;

				case 'id':
					$this->c_caption_For = $value;
					$this->c_table_Str   .= $key . '="' . $value . '" ';
					break;

				default:
					$this->c_table_Str .= $key . '="' . $value . '" ';
					break;
			}
		}

		private function inputForEach($key, $value)
		{
			$l_A_Retourner = '';

			switch($key)
			{
				case 'id':
					$this->c_label_For = $value;
					$l_A_Retourner     = $key . '="' . $value . '" ';
					break;

				case 'label' :
					$this->labelBalise($value);
					break;

				default :
					$l_A_Retourner = $key . '="' . $value . '" ';
					break;
			}

			return $l_A_Retourner;
		}

		private function selectForEach($key, $value)
		{
			switch($key)
			{
				case 'id':
					$this->c_label_For  = $value;
					$this->c_select_Str .= $key . '="' . $value . '" ';
					break;

				case 'label':
					$this->labelBalise($value);
					break;

				case 'tabOption':
					$this->constructOption($value);
					break;

				case 'OPTGRP':
					$this->constructOptionGroup($value);
					break;

				default:
					$this->c_select_Str .= $key . '="' . $value . '" ';
					break;
			}
		}

		private function captionBalise($Tab)
		{
			$this->c_caption_Str = '<caption ';

			foreach($Tab as $key => $value)
			{
				if($key == self::VALEUR)
				{
					$this->c_caption_Value = $value;
				}
				else
				{
					$this->c_caption_Str .= $key . '="' . $value . '" ';
				}
			}

			$this->c_caption_Str .= sprintf($this->c_caption_Format, $this->c_caption_For, $this->c_caption_Value);
		}

		private function constructOptionGroup($tab)
		{

			foreach($tab as $cle => $val)
			{
				$this->c_optgrp_Str .= '<optgroup ';

				foreach($val as $key => $value)
				{
					if($key == 'tabOption')
					{
						$this->constructOption($value);
					}
					else
					{
						$this->c_optgrp_Str .= $key . '="' . $value . '" ';
					}
				}

				$this->c_optgrp_Str .= sprintf($this->c_optgrp_Format, $this->c_option_Str);
				$this->c_option_Str = '';
			}
		}

		private function constructOption($tab)
		{
			foreach($tab as $cle => $val)
			{
				$this->c_option_Str .= '<option value="' . $cle . '" ';

				foreach($val as $key => $value)
				{
					if($key == 'value')
					{
						$this->c_option_Value = $value;
					}
					else
					{
						$this->c_option_Str .= $key . '="' . $value . '" ';
					}
				}

				$this->c_option_Str .= sprintf($this->c_option_Format, $this->c_option_Value);
			}
		}
// </editor-fold>
//
// // // <editor-fold desc="OLD CODE - Options avec BDD">
//        /* 	
//          ///////// commenter cas lier a la BDD
//          // Récupération des noms de champs
//          $tKeys = array_keys($tSQL[0]);
//
//          // Génération des listes de valeurs
//          $comboStr = '';
//          foreach ($tSQL as $tEnreg) {
//          $comboStr .= '<option '.$class.' value="'.$tEnreg[$tKeys[0]].'">'.mb_convert_encoding($tEnreg[$tKeys[1]],"UTF-8","ISO-8859-15").'</option>';
//          }
//         */
//         
//           function tableForm($balise, $tSQL, $class = "")
//    {
//
//        // Récupération des noms de champs
//        $tKeys = array_keys($tSQL[0]);
//
//        // Génération de la classe CSS
//        $class = $class != "" ? ' class="' . $class . '" ' : " ";
//
//        // Génération du tableau
//        $tableStr = '<table ' . $class . '>';
//
//        // Entêtes
//        $tableStr .= '<tr>';
//        foreach ($tKeys as $name)
//        {
//            $tableStr .= '<th>' . $name . '</th>';
//        }
//        $tableStr .= '</tr>';
//
//        // Contenu du tableau
//        foreach ($tSQL as $tEnreg)
//        {
//            $tableStr .= '<tr>';
//            foreach ($tEnreg as $value)
//            {
//                $tableStr .= '<td>' . $value . '</td>';
//            }
//            $tableStr .= '</tr>';
//        }
//        $tableStr .= '</table>';
//
//        $this->remplacePage($balise, $tableStr);
//    }
//         
		//</editor-fold>

	}
