<?php

	/**
	 * Description of cls_TestString
	 * @author mathieu
	 */
	class cls_Index extends cls_ConstruitTemplate
	{
// <editor-fold desc="Constantes">

		const TITRE        = '#title#';
		const BEGIN        = '#begin#';
		const LOGIN        = '#login#';
		const PASSWORD     = '#pass#';
		const SELECT       = '#level#';
		const SUBMIT       = '#submit#';
		const END          = '#end#';
		const TABLE1       = '#table1#';
		const TABLE2       = '#table2#';
		const IMAGE        = '#image#';
		const A_LIEN       = '#a_lien#';
		const A_IMG        = '#a_img#';
		const LI           = '#li#';
		const UL           = '#ul#';
		const TITRE_VALEUR = 'INDEX - Test classe';
// </editor-fold>
//
// <editor-fold desc="Form">

		const TAB_BEGIN = [
			parent::ACTION  => 'init.php',
			parent::METHODE => 'POST',
		];

		/**
		 * Permet de générer le début du formulaire
		 */
		function formBegin()
		{
			parent::formBeginBalise(self::BEGIN, self::TAB_BEGIN);
		}

		/**
		 * Permet de générer la fin du formulaire
		 */
		function formEnd()
		{
			parent::formEndBalise(self::END);
		}
		// </editor-fold>
//    
// <editor-fold desc="Login">

		/**
		 * Tableau du Label
		 */
		const TAB_TEXT_LABEL = [
			parent::ID     => 'lblLogin',
			parent::VALEUR => 'Votre login : ',
		];
		/**
		 * Tableau du login (Input Text)
		 */
		const TAB_TEXT = [
			parent::TYPE      => 'text',
			parent::NOM       => 'login',
			parent::ID        => 'login',
			parent::CLASSE    => 'class',
			parent::REQUIS    => '',
			parent::AUTOFOCUS => '',
			parent::LIBELLE   => self::TAB_TEXT_LABEL,
		];

		/**
		 * Permet de générer le login
		 */
		function login()
		{
			parent::inputBalise(self::LOGIN, self::TAB_TEXT);
		}
// </editor-fold>
//
// <editor-fold desc="Password">

		/**
		 * Tableau du Label
		 */
		const TAB_PASSWORD_LABEL = [
			parent::ID     => 'lblPassword',
			parent::VALEUR => 'Votre mot de passe : ',
		];
		/**
		 * Tableau du mot de passe (Input Password)
		 */
		const TAB_PASSWORD = [
			parent::TYPE    => 'password',
			parent::NOM     => 'mdp',
			parent::ID      => 'mdp',
			parent::CLASSE  => 'class',
			parent::REQUIS  => '',
			parent::LIBELLE => self::TAB_PASSWORD_LABEL,
		];

		/**
		 * Permet de générer le password
		 */
		function password()
		{
			parent::inputBalise(self::PASSWORD, self::TAB_PASSWORD);
		}
// </editor-fold>
//    
// <editor-fold desc="Submit">

		/**
		 * Tableau du bouton 'Confirmer' (Input Submit)
		 */
		const TAB_SUBMIT = array(
			parent::TYPE   => 'submit',
			parent::NOM    => 'valider',
			parent::ID     => 'valider',
			parent::CLASSE => 'class',
			parent::VALEUR => 'Confirmer',
		);

		/**
		 * Permet de générer le bouton submit
		 */
		function submit()
		{
			parent::inputBalise(self::SUBMIT, self::TAB_SUBMIT);
		}
// </editor-fold>
//
// <editor-fold desc="Select">

		/**
		 * Tableau du Label
		 */
		const TAB_SELECT_LABEL = [
			parent::ID     => 'lblSelect',
			parent::VALEUR => 'Votre niveau : ',
		];
		/**
		 * Tableau des options
		 */
		/*
		  const TAB_SELECT_OPTION0 = [
		  '1' => [parent::VALEUR => 'facile0'],
		  '2' => [parent::VALEUR => 'moyen0', parent::SELECTIONNER => ''],
		  '3' => [parent::VALEUR => 'difficle0'],
		  ];
		 */

		/**
		 * Tableau des options 1 pour le groupe option 1
		 */
		const TAB_SELECT_OPTION1 = [
			'1' => [parent::VALEUR => 'facile1'],
			'2' => [parent::VALEUR => 'moyen1'],
			'3' => [parent::VALEUR => 'difficile1', parent::SELECTIONNER => ''],
		];
		/**
		 * Tableau des options 2 pour le groupe option 2
		 */
		const TAB_SELECT_OPTION2 = [
			'1' => [parent::VALEUR => 'facile2'],
			'2' => [parent::VALEUR => 'moyen2'],
			'3' => [parent::VALEUR => 'difficile2'],
		];
		/**
		 * Tableau des options 3 pour le groupe option 3
		 */
		const TAB_SELECT_OPTION3 = [
			'1' => [parent::VALEUR => 'facile3'],
			'2' => [parent::VALEUR => 'moyen3'],
			'3' => [parent::VALEUR => 'difficile3', parent::GRISER => ''],
		];
		/**
		 * Tableau des groupes options 1
		 */
		const TAB_OPTGROUP1 = [
			parent::ID      => 'TAB_OPTGROUP1',
			parent::LIBELLE => 'Label_OPT1',
			parent::CLASSE  => 'Class_OPT1',
			parent::OPTIONS => self::TAB_SELECT_OPTION1,
		];
		/**
		 * Tableau des groupes options 2
		 */
		const TAB_OPTGROUP2 = [
			parent::ID      => 'TAB_OPTGROUP2',
			parent::LIBELLE => 'Label_OPT2',
			parent::CLASSE  => 'Class_OPT2',
			parent::GRISER  => '',
			parent::OPTIONS => self::TAB_SELECT_OPTION2,
		];
		/**
		 * Tableau des groupes options 3
		 */
		const TAB_OPTGROUP3 = [
			parent::ID      => 'TAB_OPTGROUP3',
			parent::LIBELLE => 'Label_OPT3',
			parent::CLASSE  => 'Class_OPT3',
			parent::OPTIONS => self::TAB_SELECT_OPTION3,
		];
		/**
		 * Tableau des niveaux (select + opt groupe)
		 */
		const TAB_SELECT = [
			parent::ID      => 'select',
			parent::LIBELLE => self::TAB_SELECT_LABEL,
			parent::OPTGRP  => [self::TAB_OPTGROUP1, self::TAB_OPTGROUP2, self::TAB_OPTGROUP3],
			//parent::OPTIONS => self::TAB_SELECT_OPTION0,
		];

		/**
		 * Permet de générer le choix du level
		 */
		function select()
		{
			parent::selectBalise(self::SELECT, self::TAB_SELECT);
		}
// </editor-fold>
//    
// <editor-fold desc="Fonctions">

		/**
		 * Permet de générer le titre
		 */
		function titre()
		{
			parent::remplacePage(self::TITRE, self::TITRE_VALEUR);
		}
		// </editor-fold>
//
// <editor-fold desc="Table avec thead, tbody, tfoot">

		/**
		 * Tableau du caption
		 */
		const CAPTION = [
			parent::ID     => 'Table_Cap',
			parent::VALEUR => 'Table Caption : ',
		];
		/**
		 * Tableau du thead
		 */
		const TAB_THEAD = [
			'Prénom', 'Nom', 'Age',
		];
		/**
		 * Tableau du tbody
		 */
		const TAB_TBODY = [
			['GuiGui', 'PHP Storm', '22'],
			['Vince', 'Chef', '21'],
		];
		/**
		 * Tableau du tfoot
		 */
		const TAB_TFOOT = [
			['Foot 1', 'Foot 2', 'Foot 3'],
		];
		/**
		 * Tableau de la table 1
		 */
		const TAB_TABLE1 = [
			parent::ID      => 'table_ID',
			parent::THEAD   => self::TAB_THEAD,
			parent::TBODY   => self::TAB_TBODY,
			parent::TFOOT   => self::TAB_TFOOT,
			parent::CAPTION => self::CAPTION,
			parent::CLASSE  => 'Class_Table',
		];

		/**
		 * Permet de générer un tableau
		 */
		function table1()
		{
			parent::tableBalise(self::TABLE1, self::TAB_TABLE1);
		}
//
// </editor-fold>
//
// <editor-fold desc="Table avec th, td">

		/**
		 * Tableau des TH
		 */
		const TAB_TH = [
			'Prénom', 'Nom', 'Age',
		];
		/**
		 * Tableau des TD
		 */
		const TAB_TD = [
			['Papi', 'Marechet', '30'],
			['Mami', 'Marechet', '29'],
		];
		/**
		 * Tableau de la table 2
		 */
		const TAB_TABLE2 = [
			parent::ID      => 'table_ID',
			parent::TH      => self::TAB_TH,
			parent::TD      => self::TAB_TD,
			parent::CAPTION => self::CAPTION,
			parent::CLASSE  => 'Class_Table',
		];

		/**
		 * Permet de générer un tableau
		 */
		function table2()
		{
			parent::tableBalise(self::TABLE2, self::TAB_TABLE2);
		}

//
// </editor-fold>
//

		const TAB_IMG = [
			parent::SOURCE => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAABBVBMVEUhISH/////mgAAAABSUlL/ngD/pgAdHiEeHCKwdgsHEiRUPRkSEhL/mQD/owD/oAASFyPlmQRiRBfQjAc/MB3l5eViYmIGBAA4IgBmQADGhwAXFxf4+PjOzs4pKSmVlZUZDwDDw8OiawA/JgAkJCQzMzPajgB4TQASCgBbNwDX19ePWgDtmABOLwBaWlru7u7sogCysrI5OTl4eHimpqZCQkL/rQCWlpZra2t6enoeEgBJSUlXNAAnFwD0rQCXYgC6urqHh4fqkAB7SgC8eQCpZQDIfwBINRt+WRNtTRWjcw0zKh6bbQ65gQqKWwBxSQChYQC5cADUigBEJgDamwDIkwA5JwBu2j/FAAAHuklEQVR4nO2bC3eaShCAUQT7WECuJWgAX9HEiKLG9yuRxj5u25i28d7//1PuviBCbEVFTc/NnOM5LrC7HzOzM7NwYCJHFuYF4AXgOQHUmy0rtnexWs36SgA9b2WKzAGkmLHy+lOAevYgs1OGbN0PoGcPNz2SrO4DyB92fobJewHqsUMDxOoegGbm0ACZpgegf0APJFLsewAObgFog2cNoHkkHj80QLG0LDA+QopDAgArX3kUw8g3eycZcEiAvhLxSb05AiEb4ncAJ08AIhHDClkHmwJEjFG4BOsB9DoU3U2cepsJ1RPXAxijUqfTGcUGBrmmEgtVBesB8kWARGWZNlaDbrsa0DQAYHx4OmpcA/4VCy8Fqy4NAJChQ4ErogNqAxgUSiPLipUY/1xxUBxZI9xNc2YvXsUsq5MBT8y3AYCmERU0ixpqlPptQ9H1eqU56BAELZNtQukz/ami1/N2SQN9dGBQ7NhTBfqRAo/57bcBAKP1XABNO5m65ZRuDPCtggxSkd7MktVT6QC2jUewDXcR5f0etIkGGJK7m8j2tmeJ1tsMQBrAAAolg6BqFp/Vly41OmBLAI21yLLsxTXVXh4TOwa0AgFwoVqAIQCRZQq953WZ9QDTOKsiAZaBx1D6AMTI/etT227Tv7a6BKBXlIhxtQRg9GMWtUze6wbrASqDVmswsHt5eg9GSY3n8b/KiQZUcDUljQ5wAfKxYmlgQ1dxAJpFFa7CAbaNL45sHIqhZh2yGIsvK02pagEFqFyxmsaiaSiAkQFF5KOYW7F2AtDbMJ7g5ahn6aJ2fKWkUoCsG3AoQBZPqcWbqJ9ysguA0mOAVvLeCSBtqBACUO+7XkYBWvjKOFnGOwDAmAOtzoAOngc5GRa6PJU+1YAS8wHolEjLbgWgNNtYenYrhkMpGFWIyh0AYpJ6iwJURj6A+slOAPkSacY1WhL6AahqXQCj4wewqLNsCQBvm1TFznFqgtKjCfRlE4QP4MtgjtM5pZG7vKgTGld7BqBOVx84AFaFzEs1kN83AENDGlU1IIWK3nYC0QEArvAtR5od5BoZG+MoMScU7x+AATQZGrYVo8+aUJI7HIBWpLt6XXGS/zTjpuMDADgL4VFwUNgbwIoyHGR6S0/59DaOCeDpMuxhABoJwea5QIcyXaEBVOeOaCkSqU8tpyg1UAePBtABxXJCcR21AgPQ7fmvHhsBtRhr2bZ9UgKqcyzj74APlIorW+sB4nhH8uuNmKaSWm1ZL74O+ADwtry7k+f9iOYF4BkAvHoTsjzZH/8WIP4m/jZc+ZR4swFAPPH3OR+uiKdvE4EB4PwNPhqydOVPiaAAia+nUtjzR6N8wfvE9XcAb2VOCB2AO/2aCArwl8yFPn+US757AfjTAASugUUK7JCCUMU9qtEVXTYHkH6mH6CMZ8FjgmTWUBezukKDmwOIpykWSu6mG3R+ThyrqEt6lQm3BygE14CYxgD34Wpgc4DbFwDxuACquSqX7QTASSLM6RI8ihY4/HFI4LqPepqPAFxU4rtd1EMQdgfoNhY/f3z4/uOj3JDQdEJUlhcLWa46ANUFaVKA8g3fWHz88R32WPCcE5S2Bhh++Wdem6CBcw9mkofjSee3NSjjD8TSXNVEzZrJcRTgW3JeGyJVDGtmUqSpfWuA8vhf7FlI1LM7Ht7n+RluzImppeolPnkJDUUAaim3B3tWaBCCrQG8kpJ5gQKUqa9xBEBNOwBeGZo8JggJgL3vRjcDYIcFETvqDgDq2YV5P6HDvReDAOTS84tamfyvnYu7AQxvGtfX1/884EZ51g0A8PAT9rguEGj1RtrJBDAdQiMK/F0Oj2YGADhbdGEPrlsY4uYlGm97gJqMNChwSawCdc6vBVA/4OApSPwYj5A63QVAvafzLGq4ebEeIJUkK0/o3mA3KP+QdokDc5ILOHkcFOBSdgA+YgD1u7hTRUQAJPkyKIBbEfHn2G9Ykw8hG24DIL6nAI19AkgBAPaqAak6fuoD1AR7BVBNjp6o+QAeFj4n/LDLKvg1wC0GEEQaIB4BhjMxurwMh7O9ALDpKoqQUf5u4gNgb1EKFgSxgXXDnqG4ECKAVCXjPiSRaXmZhr8lgOGXrgjLk+ub3CNpiAAcR+YpX8g8LybvSdLzJKPJjSxJ8pwko1xB3CUZPQWI8iaZs1y7mN+naNL1peOzdPqM3D87lqVd0vEqgJ+0OGDL5cdyzVeQqM7fyYzfqSBZARDtmsuVTzmVWwHgCKyqozuVZC7AggDc8yjTz91bVyffZlghqCi9xNVzeqmMndyR+bd5PpC8TEF5mDnZ8BY1U6aIioPu6/EkVy7nhqn70+5dDZ245TjJRH9qp/I8NYRny8PJhUzn3+oRzXssUUfeP7aFaLf6sWCahdfnIr90If3D89UZPHvz+pznha03JnAHIkKR6B0IAoeazj5D4OgzWLT5ksgJegnqIbpnHfw/8CHVC8D/HGAPT8s3APiU3IMGpFnxVUCAOFPgw1aBIDU+J4K+sGAS7+7EkN8Z8dVvXgX8/q1Z4uvn1+HKl78Z7/xr3hvGE69CloT/xeGaF5fx0MU/w3N/dXtogKN/5HL0z3yO/qHT0T/1Ov7Hbkf/3O/4HzxGjv7J55HkBeAF4OgA/wHYFYHJmQylbgAAAABJRU5ErkJggg==',
			parent::ALT    => 'Pornhub',
		];

		function image()
		{
			parent::imgBalise(self::TAB_IMG, self::IMAGE);
		}

		const TAB_A_LIEN = [
			parent::HREF   => 'https://www.youporn.com/',
			parent::TARGET => '_blank',
			parent::VALEUR => 'Il clic ici !',
		];
		const TAB_A_IMG  = [
			parent::HREF   => 'https://www.pornhub.com/',
			parent::TARGET => '_blank',
			parent::IMAGE  => self::TAB_IMG,
		];

		function a_lien()
		{
			parent::aBalise(self::TAB_A_LIEN, self::A_LIEN);
		}

		function a_img()
		{
			parent::aBalise(self::TAB_A_IMG, self::A_IMG);
		}

		const TAB_LI = [
			parent::A => self::TAB_A_IMG,
		];

		function li()
		{
			parent::liBalise(self::TAB_LI, self::LI);
		}

		const TAB_UL = [
			parent::LI => [self::TAB_A_IMG, TAB_A_LIEN]
		];

		function ul()
		{
			parent::ulBalise(self::TAB_UL, self::UL);
		}
	}
