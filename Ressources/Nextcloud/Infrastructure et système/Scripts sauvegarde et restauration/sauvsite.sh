#!/bin/bash

#################################################
#						#
# Script sauvergarde site et base de données	#
# Vincent Sagnard pour Presta'RIP		#
# V1.0 : 15/05/2018				#
#						#
#						#
#################################################

echo "Début de la sauvegarde du site et de la base de données..."

#Définition du dossier source
SOURCE="/var/www/html/nextcloud"

#Définition du dossier de destination
DESTINATION="/home/csia/dest"

#Définition de la date a laquelle est effectuée la sauvegarde
DATE=$(date +%d-%m-%Y_%H-%M-%S)

#Définition du nom de fichier de sauvegarde
NOMFICHIER="sauve-apel-$DATE"

#Définition du nom de fichier de script sql
SCRIPTSQL="sauve-apel-bdd-$DATE.sql"

#Définition du nom de la base de données à suavegarder
USERBDD="admin"

#Définition du nom de la base de données
BDD="nextcloud"


#Vérification de l'existance du dossier de destination de la sauvegarde

if [ -e $SOURCE ]
then
	echo "Votre source existe"
else
	echo "Erreur : le dossier source n'existe pas. Vérifiez le chemin"
	exit 2
fi

#Vérification de l'existance du dossier source

if [ -d $DESTINATION ]
then
	echo "Votre destination existe"
else
	echo "Erreur : votre dossier de destination n'existe pas"
	exit 3
fi


#Vérification si la source est bien un dossier

if [ -d $SOURCE ]
then
	echo "Votre source est bien un dossier"
else
	echo "Erreur : votre source n'est pas un dossier"
	exit 4
fi

#Vérification si la destination est bien un dossier

if [ -d $DESTINATION ]
then
	echo "Votre destination est bien un dossier"
else
	echo "Erreur : votre destination n'est pas un dossier"
	exit 5
fi

#Vérification de la possibilité de lecture dans le dossier source du site

if [ -r $SOURCE ]
then
	echo "Dossier source lisible"
else
	echo "Erreur : lecture impossible de lire la source, verifiez les droits"
	exit 6
fi


#Vérification de la possibilité d'écriture dans le dossier de destination de la sauvegarde

if [ -w $DESTINATION ]
then
	echo "Dossier de destination modifiable"
else
	echo "Erreur : lecture impossible d'écrire dans le dossier de destination, verifiez les droits"
	exit 7
fi


#Sauvegarde dans un script SQL de la base de données (et des utilisateurs)
	#Le mot de passe de la bdd sera demandé lorsque l'admin cliquera sur le bouton suavegarder. Le mot de passe sera donc un paramètre

pg_dump -h localhost -p 5432 -U $USERBDD $BDD > $DESTINATION/$SCRIPTSQL #création du script de la base de données

#Compression des fichiers sources et du script SQL dans un fichier tar.gz (nom : sauve-apel-JJ-MM-AA-HH-mm-ss.tar.gz)
cd $DESTINATION
tar --same-owner -pzcf $NOMFICHIER.tar.gz $SOURCE/* $SCRIPTSQL #compression du contenu de $SOURCE dans un fichier

if [ $? -eq 0 ] # si la création de l'archive s'est bien déroulée
then

	echo "Archive tar créée"
	rm $SCRIPTSQL
else
	echo "Erreur : l'archive n'a pas pu être créée vérifez l'espace disponible " #s'il y a eu une erreur lors de la création de l'archive
	exit 8
fi

exit $? #On retourne le code de retour de la commande tar car si elle s'est mal passée, le script de sauvegarde se sera mal passé

