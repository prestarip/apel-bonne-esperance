<?php
	/**
	 * Created by PhpStorm.
	 * User: Vincent
	 * Date: 12/06/2018
	 * Time: 13:47
	 */

	//Déclaration du namespace
	namespace Apel;

	$page = file_get_contents('Templates/header.html');

	$page .= file_get_contents('Templates/listeTransactions.html');
	$page = str_replace('#titre#', 'Transactions', $page);
	$page = str_replace('#tableaupaiements#', 'tableau listant les transactions (cotisations et dons)', $page);

	$page .= file_get_contents('Templates/footer.html');

	echo $page;