<?php
	/**
	 * Created by PhpStorm.
	 * User: mathieu
	 * Date: 26/07/2018
	 * Time: 11:56
	 */

	namespace Apel;

	use Apel\Dll\Framework\Config;
	use Apel\Dll\Page\cls_InscriptionToken;

	return new class()
	{
		/** @var \Apel\Dll\Page\cls_InscriptionToken $page */
		private $page;

		public function __construct($layout = true)
		{
			$layout = (isset($_SESSION['noLayout']) && $_SESSION['noLayout'] <> null) ? false : $layout;

			$this->generatePage($layout);
		}

		private function generatePage(bool $layout)
		{
			$message = '';

			//Initialisation du template
			$this->page = new cls_InscriptionToken(Config::getAdresse('INSCRIPTION_TOKEN'), $layout);
			$this->page->remplacePage('#begin_section#', (($layout) ? '<section>' : ''));

			if(isset($_SESSION['message']))
			{
				//Récupération du message contenu dans la variable de session
				$message = $_SESSION['message'];
			}

			//Appel des différents fonctions permettant de générer le code HTML
			$this->page->message($message);
			$this->page->formBegin();
			$this->page->cle();
			$this->page->submit();
			$this->page->formEnd();

			$this->page->remplacePage('#end_section#', (($layout) ? '</section>' : ''));

			if($layout)
			{
				echo $this->afficherPage();
			}

			//Vidage de la variable
			unset($_SESSION['message']);

		}

		private function afficherPage()
		{
			//Affichage de la page
			return $this->page->getPage();
		}

		public function __toString() : string
		{
			return $this->afficherPage();
		}
	};