var MonAppli = function ()
{
    var MonAppliObjet = {};
    var cheminControleurAjax = './Dll/Controleur/ControleurAjax.php';

    function getBulleConnexion()
    {
        getBulle('LOGIN_PHP');
    }

    function getBulleInscription()
    {
        getBulle('INSCRIPTION_PHP');
    }

    function getBulleToken()
    {
        getBulle('INSCRIPTION_TOKEN_PHP');
    }

    function getBulleCommunaute()
    {
        let bulle = '<div id="backgroundBulle" >\n' +
            '\t<div id="bulle">\n<div id="closeBulle" onclick="MonAppli.closeBulle()">X</div>' +
            '\n <div class="bulleMenu"> \n' +
            '\n     <a href="/forum.php" title="Forum">Forum</a> \n' +
            '\n     <a href="/gestionPartageDoc.php" title="Partage communautaire">Partage communautaire</a> \n' +
            '\t </div>\n' +
            '\t</div>\n' +
            '</div>';
        $('body').append(bulle);
    }

    function getBulleInformation()
    {
        let bulle = '<div id="backgroundBulle" >\n' +
            '\t<div id="bulle">\n<div id="closeBulle" onclick="MonAppli.closeBulle()">X</div>' +
            '\n <div class="bulleMenu"> \n' +
            '\n     <a href="/news.php" title="Newsletter">Newsletter</a> \n' +
            '\n     <a href="/faq.php" title="FAQ">FAQ</a> \n' +
            '\t </div>\n' +
            '\t</div>\n' +
            '</div>';
        $('body').append(bulle);
    }

    function getBulleAdministratif()
    {
        let bulle = '<div id="backgroundBulle" >\n' +
            '\t<div id="bulle">\n<div id="closeBulle" onclick="MonAppli.closeBulle()">X</div>' +
            '\n <div class="bulleMenu"> \n' +
            '\n     <a href="/bilanFinancier.php" title="Financier" id="linkBulleFinancier">Financier</a> \n' +
            '\n     <a href="/assiduite.php" title="Assiduité">Assiduité</a> \n' +
            '<script>' +
            '\n $(\'#linkBulleFinancier\').click(function (event)\n' +
            '        {\n' +
            '            event.preventDefault();\n' +
            '            MonAppli.getBulleFinancier();\n' +
            '        });';
        '</script>' +
        '\t </div>\n' +
        '\t</div>\n' +
        '</div>';
        $('body').append(bulle);
    }

    function getBulleFinancier()
    {
        closeBulle();
        let bulle = '<div id="backgroundBulle" >\n' +
            '\t<div id="bulle">\n<div id="closeBulle" onclick="MonAppli.closeBulle()">X</div>' +
            '\n <div class="bulleMenu"> \n' +
            '\n     <a href="/bilanFinancier.php" title="Bilan financier">Bilan financier</a> \n' +
            '\n     <a href="/suiviFinancier.php" title="Suivi">Suivi</a> \n' +
            '\t </div>\n' +
            '\t</div>\n' +
            '</div>';
        $('body').append(bulle);
    }

    function getBulleParametrage()
    {
        let bulle = '<div id="backgroundBulle" >\n' +
            '\t<div id="bulle">\n<div id="closeBulle" onclick="MonAppli.closeBulle()">X</div>' +
            '\n <div class="bulleMenu"> \n' +
            '\n     <a href="/gestionMembres.php" title="Gestion Membres">Membres</a> \n' +
            '\n     <a href="/droits.php" title="Droits">Droits</a> \n' +
            '\n     <a href="/save.php" title="Sauver / Charger">Sauver / Charger</a> \n' +
            '\t </div>\n' +
            '\t</div>\n' +
            '</div>';
        $('body').append(bulle);
    }

    function getRoute(pRoute)
    {
        let adresse = null;
        $.ajax(
            {
                url: cheminControleurAjax,
                type: 'POST',
                data: {
                    ajaxTraitement: 'getRoute',
                    route: pRoute
                },
                dataType: 'json',
                async: false,
                success: function (result, statut)
                {
                    adresse = result.objets.adresse;
                },
                error: function (result, statut, erreur)
                {
                    adresse = null;
                    console.log('no result : ' + erreur);
                    console.log(result);
                }
            });
        return adresse;
    }

    function getBulle(pRoute)
    {
        var lien = getRoute(pRoute);
        $.ajax(
            {
                url: cheminControleurAjax,
                type: 'POST',
                data: {
                    ajaxTraitement: 'getBulle',
                    lien: lien
                },
                dataType: 'json',
                async: true,
                success: function (result, statut)
                {
                    var bulle = '<div id="backgroundBulle" >\n' +
                        '\t<div id="bulle">\n<div id="closeBulle" onclick="MonAppli.closeBulle()">X</div>' +
                        '\n' + result.objets.bulle + '\n' +
                        '\t</div>\n' +
                        '</div>';
                    $('body').append(bulle);
                },
                error: function (result, statut, erreur)
                {
                    console.log('no result : ' + erreur);
                    console.log(result);
                }
            });
    }

    function closeBulle()
    {
        $('#backgroundBulle').remove();
    }

    function confirmToken(pThis, event)
    {
        event.preventDefault();
        let cle = $(pThis).children('#cle').val();
        let recaptcha = $('[name="g-recaptcha-response"]').val();
        $.ajax(
            {
                url: cheminControleurAjax,
                type: 'POST',
                data: {
                    ajaxTraitement: 'confirmToken',
                    cle: cle,
                    'g-recaptcha-response': recaptcha
                },
                dataType: 'json',
                async: true,
                success: function (result, statut)
                {
                    closeBulle();
                    getBulle(result.objets.redirection);
                },
                error: function (result, statut, erreur)
                {
                    console.log('no result : ' + erreur);
                    console.log(result);
                }
            });
    }

    function confirmInscription(event)
    {
        let formResponse = [];

        let formulaireInscription = event.currentTarget;
        $.each(formulaireInscription, function ()
        {
            formResponse[$(this).attr('name')] = $(this).val();
        });
        console.log(formResponse);

        $.ajax(
            {
                url: cheminControleurAjax,
                type: 'POST',
                data: {
                    ajaxTraitement: 'confirmInscription',
                    formResponse: formResponse
                },
                dataType: 'json',
                async: true,
                success: function (result, statut)
                {
                    console.log(statut);
                },
                error: function (result, statut, erreur)
                {
                    console.log('no result : ' + erreur);
                    console.log(result);
                }
            });
    }

    function openMessagerieInstantane(pData)
    {
        var data = pData;
        var ouvert = false;
        var listeChat = $('.chatWindows');
        if (listeChat.length > 0)
        {
            listeChat.each(function ()
            {
                if (this.id === ('chat' + data.id))
                {
                    ouvert = true;
                }
            });
        }
        if (listeChat.length === 0 || ouvert === false)
        {
            getChat(data);
        }
    }

    function getChat(pData)
    {
        var data = pData;
        $.ajax(
            {
                url: cheminControleurAjax,
                type: 'POST',
                data: {
                    ajaxTraitement: 'getChat',
                    data: data
                },
                dataType: 'json',
                async: false,
                success: function (result, statut)
                {
                    afficherChat(data, result);
                },
                error: function (result, statut, erreur)
                {
                    console.log('no result : ' + erreur);
                    console.log(result);
                }
            });
    }

    function afficherChat(data, result)
    {

        var chat = '' +
            '\t<div class="chatWindows"  id="chat' + data.id + '" data-id="' + data.id + '">\n' +
            '\t\t<div class="barreHauteChat">\n' +
            '\t\t\t<div>' + data.nom + ' ' + data.prenom + '</div>\n' +
            '\t\t\t<div class="closeChat" onclick="MonAppli.closeMessagerieInstantane(this, event)">X</div>\n' +
            '\t\t</div>\n' +
            '\t\t<div class="discussionChat">\n';
        if (result.objets.length > 0)
        {
            $.each(result.objets, function ()
            {
                chat = chat +
                    '\t\t\t<p' + (data.id === this.fk_idcompte_to ? ' class="contact" ' : '') + ' data-dateenvoi="' + this.dateenvoi + '" data-id="' + this.idmessageinstant + '" >\n' +
                    '\t\t\t\t' + this.message + '\n' +
                    '\t\t\t</p>\n';
            });
        }
        else
        {
            // Message simple pour dire qu'il n'y a aucun message entre ces deux personnes
        }
        chat =
            chat +
            '\t\t</div>\n' +
            '\t\t<div class="barreBasseChat">\n' +
            '\t\t\t<form action="#" method="post" onsubmit="event.preventDefault();MonAppli.sendMessageMessagerieInstantane(this)">\n' +
            '\t\t\t\t<textarea name="texteMessagerieInstantane" onkeypress="let pressKeyValue = event.keyCode;if(pressKeyValue === 13){        event.preventDefault();let elem = $(this).parent();MonAppli.sendMessageMessagerieInstantane(elem);}" ></textarea>\n' +
            '\t\t\t\t<button name="sendMessageMessagerieInstantane">↑</button>\n' +
            '\t\t\t</form>\n' +
            '\t\t</div>\n' +
            '\t</div>';
        $('#divAllChatWindows').append(chat);
        let param = '#chat' + data.id;
        getNewMessages(param);
    }

    function addMessageChat(pData, pMessage, infoLastMessage = null)
    {
        message = '\t\t\t<p class="contact" data-dateenvoi="' + infoLastMessage.dateenvoi + '" data-id="' + infoLastMessage.id + '" >\n' +
            '\t\t\t\t' + pMessage + '\n' +
            '\t\t\t</p>\n';
        chat = $('#chat' + pData.id);
        chat.children('.discussionChat').append(message);
        chat.children('.barreBasseChat').children('form').children('textarea').val('');
    }

    function sendMessageMessagerieInstantane(pThis)
    {
        var dataConversation = $(pThis).parent().parent().data();
        var message = $(pThis).children('textarea').val();
        // console.log($('#chat' + dataConversation.id).children('.barreBasseChat').children('form').children('textarea')[0].onkeyup);
        $.ajax(
            {
                url: cheminControleurAjax,
                type: 'POST',
                data: {
                    ajaxTraitement: 'sendMessageMessagerieInstantane',
                    data: dataConversation,
                    message: message
                },
                dataType: 'json',
                async: true,
                success: function (result, statut)
                {
                    if (result.objets !== '')
                    {
                        addMessageChat(dataConversation, message, result.objets.infoLastMessage);
                    }
                },
                error: function (result, statut, erreur)
                {
                    console.log(statut);
                    console.log('no result : ' + erreur);
                    console.log(result);
                }
            });
    }

    function closeMessagerieInstantane(pThis, event)
    {
        event.preventDefault();
        $(pThis).parent().parent().remove();
    }

    function closeMessageInfo(pThis, event)
    {
        event.preventDefault();
        $(pThis).parent().remove();
    }

    function getNewMessages(pThis)
    {
        setInterval(function ()
        {
            let chat = $(pThis);
            let idContact = chat.data().id;
            let idLastMessage = $(pThis + ' .discussionChat p:last').data().id;
            $.ajax(
                {
                    url: cheminControleurAjax,
                    type: 'POST',
                    data: {
                        ajaxTraitement: 'getNewChat',
                        idContact: idContact,
                        idLastMessage: idLastMessage
                    },
                    dataType: 'json',
                    async: false,
                    success: function (result, statut)
                    {
                        let messages = '';
                        if (result.objets.length > 0)
                        {
                            $.each(result.objets, function ()
                            {
                                messages = messages +
                                    '\t\t\t<p' + (idContact === this.fk_idcompte_to ? ' class="contact" ' : '') + ' data-dateenvoi="' + this.dateenvoi + '" data-id="' + this.idmessageinstant + '" >\n' +
                                    '\t\t\t\t' + this.message + '\n' +
                                    '\t\t\t</p>\n';
                            });
                            chat.children('.discussionChat').append(messages);
                        }
                    },
                    error: function (result, statut, erreur)
                    {
                        console.log(statut);
                        console.log('no result : ' + erreur);
                        console.log(result);
                    }
                });
        }, 5000);

    }

    MonAppliObjet =
        {
            getBulleConnexion: getBulleConnexion,
            confirmToken: confirmToken,
            confirmInscription: confirmInscription,
            getBulleInscription: getBulleInscription,
            getBulleToken: getBulleToken,
            getBulleCommunaute: getBulleCommunaute,
            getBulleInformation: getBulleInformation,
            getBulleAdministratif: getBulleAdministratif,
            getBulleParametrage: getBulleParametrage,
            getBulleFinancier: getBulleFinancier,
            getRoute: getRoute,
            closeBulle: closeBulle,
            openMessagerieInstantane: openMessagerieInstantane,
            closeMessagerieInstantane: closeMessagerieInstantane,
            closeMessageInfo: closeMessageInfo,
            sendMessageMessagerieInstantane: sendMessageMessagerieInstantane,
            getNewMessages: getNewMessages
        };

    return MonAppliObjet;
}();
// document.addEventListener("keyup", function(event) {
//     console.log(event.which);
// });