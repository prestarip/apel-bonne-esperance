<?php
	/**
	 * Created by PhpStorm.
	 * User: pythonirc
	 * Date: 18/07/18
	 * Time: 09:46
	 */

	//Déclaration du namespace

	namespace Apel;

	use Apel\Dll\Framework\Config;
	use Apel\Dll\Page\cls_faireDon;

	return new class()
	{
		private $page;

		public function __construct($layout = true)
		{
			$this->generatePage($layout);
		}

		private function generatePage(bool $layout)
		{
			//Initialisation du template
			$this->page = new cls_faireDon(Config::getAdresse('FAIRE_DON'), $layout);
			$this->page->remplacePage('#begin_section#', (($layout) ? '<section>' : ''));
			$titre = 'Faire un don';

			$this->page->titre($titre);

			$this->page->remplacePage('#end_section#', (($layout) ? '</section>' : ''));
			if($layout)
			{
				echo $this->page->getPage();
			}
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return $this->page;
		}
	};
