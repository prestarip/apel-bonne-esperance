<?php

	/*
	spl_autoload_register(function ($class)
	{
		require_once './Dll/Page/' . $class . '.php';
	});
	*/

	require_once 'Dll/Page/cls_Exemple.php';

	new class()
	{
		private $Page;

		function __construct()
		{
			$this->Page = new \Apel\Dll\Page\cls_Exemple('Templates/exemple.html');
			$this->generatePage();
		}

		private function generatePage()
		{
			$this->Page->titre();
			$this->Page->formBegin();
			$this->Page->login();
			$this->Page->password();
			$this->Page->submit();
			$this->Page->formEnd();
			$this->Page->select();
			$this->Page->table1();
			$this->Page->table2();
			$this->Page->image();
			$this->Page->a_lien();
			$this->Page->a_img();
			$this->Page->nav();
			$this->Page->textArea();

			echo $this->Page->getPage();

		}
	};
