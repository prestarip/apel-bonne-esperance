<?php
	/**
	 * Created by PhpStorm.
	 * User: Vincent
	 * Date: 12/06/2018
	 * Time: 13:33
	 */

	//Déclaration du namespace
	namespace Apel;

	use Apel\Dll\Framework\cls_ConstruitTemplate;
	use Apel\Dll\Framework\Config;

	return new class()
	{
		private $page;

		public function __construct($layout = true)
		{
			$this->generatePage($layout);
		}

		private function generatePage(bool $layout)
		{
			//Initialisation du template
			$this->page = new cls_ConstruitTemplate(Config::getAdresse('GESTION_PARTAGE_DOC'), $layout);
			$this->page->remplacePage('#begin_section#', (($layout) ? '<section>' : ''));
			$this->page->remplacePage('#title#', 'Gestion du partage documentaire');
			$this->page->remplacePage('#boutonsgestiondoc#', 'Boutons de gestion documentaire');
			$this->page->remplacePage('#tableaulistedoc#', 'liste des documents partagés');
			$this->page->remplacePage('#end_section#', (($layout) ? '</section>' : ''));

			if($layout)
			{
				echo $this->page->getPage();
			}
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return $this->page;
		}
	};