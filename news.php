<?php
	/**
	 * Created by PhpStorm.
	 * User: pythonirc
	 * Date: 03/07/18
	 * Time: 08:53
	 */

	//Déclaration du namespace

	namespace Apel;

	use Apel\Dll\Controleur\ControleurNews;
	use Apel\Dll\Framework\Config;
	use Apel\Dll\Page\cls_news;

	return new class()
	{
		/** @var \Apel\Dll\Page\cls_rss $page */
		private $page;

		public function __construct($layout = true)
		{
			$layout = (isset($_SESSION['noLayout']) && $_SESSION['noLayout'] <> null) ? false : $layout;
			$this->generatePage($layout);
		}

		private function generatePage(bool $layout)
		{
			//Initialisation du template
			$this->page     = new cls_news(Config::getAdresse('NEWS'), $layout);
			$controleurNews = new ControleurNews;
			$this->page->remplacePage('#begin_section#', (($layout) ? '<section>' : ''));
			$titre = 'Actualite';
			$this->page->titre($titre);
			$limit = false;

			if( !isset($_GET['id']))
			{
				// contenus de la page
				if($_SERVER['PHP_SELF'] == "/index.php")
				{
					$limit = true;
				}

				$listeActualite = $controleurNews->recupererActualite(0, $limit);

				//Appel des différents fonctions permettant de générer le code HTML
				$listeActualite = $this->page->formaterListeActus($listeActualite);
				$this->page->listeNews($listeActualite);
			}
			else
			{
				if($controleurNews->isExist($_GET['id']))
				{
					$actualite = $controleurNews->recupererActualite($_GET['id'], $limit);
					$actualite = $this->page->formaterActu($actualite);
					$this->page->listeNews($actualite);
				}
				else
				{
					$this->page->listeNews("Cet article n'existe pas.");
				}
			}

			$this->page->remplacePage('#end_section#', (($layout) ? '</section>' : ''));

			if($layout)
			{
				echo $this->afficherPage();
			}
		}

		private function afficherPage()
		{
			//Affichage de la page
			return $this->page->getPage();
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return $this->afficherPage();
		}
	};
