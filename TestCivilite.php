<?php
	/**
	 * Created by PhpStorm.
	 * User: GuillaumeFixe
	 * Date: 11/06/2018
	 * Time: 18:52
	 */

	namespace Apel;

	use Apel\Dll\Page\cls_Exemple;

	new class()
	{
		/** @var \Apel\Dll\Framework\Dao $dao */
		private $dao;
		private $test;
		private $listeCivilite;

		/**
		 *  constructor.
		 */
		public function __construct()
		{
			$this->importDll();
			$this->connexionBDD();
			$this->getData();
			echo $this->genPage();
		}

		private function importDll()
		{
			require_once 'Dll/Framework/Dll.php'; //Importe toutes les classes
		}

		private function connexionBDD()
		{
			$this->dao = new \Apel\Dll\Framework\Dao();
		}

		private function getData()
		{
			//$this->test          = $this->dao->getTest();
			$this->listeCivilite = $this->dao->getAllCivilite();
		}

		private function genPage()
		{
			$page = new cls_Exemple('./Templates/Civilite.html');
//			$page = new cls_ConstruitTemplate('./Templates/Civilite.html');
//			var_dump($this->test);
//			$page->remplacePage('#testRequeteSimple#', array_unique($this->test));

//			$msg = '<ul>';
//
//			foreach(array_unique($this->listeCivilite) as $key => $value)
//			{
//				$msg .= '<li>' . $key . ' : ' . $value . '</li>';
//			}
//			$msg .= '</ul>';

			$msg = var_dump($this->listeCivilite);
			var_dump($this->test);
			$page->remplacePage('#testRequeteSimple#', array_unique($this->test));

//			$page->remplacePage('#AfficherCivilite#', $msg);

			return $page->getPage();

			//çA MARCHE UN COUP SUR DEUX, J'ABANDONNE POUR CE SOIR !
		}
	};