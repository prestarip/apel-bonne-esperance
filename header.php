<?php
	/**
	 * Created by PhpStorm.
	 * User: Guillaume
	 * Date: 29/06/2018
	 * Time: 00:29
	 */

	namespace Apel;

	use Apel\Dll\Framework\cls_ConstruitTemplate;
	use Apel\Dll\Framework\Config;
	use Apel\Dll\Framework\Fonctions;

	return new class()
	{
		private $header;

		public function __construct()
		{
			$this->generatePage();
		}

		private function generatePage()
		{
			// Génération de la page
			/** @var cls_ConstruitTemplate $pageHeader */
			$pageHeader = new cls_ConstruitTemplate(Config::getAdresse('HEADER'), false);

			$pageHeader->remplacePage('#connexion#', (Fonctions::isConnected() ? null : '<li><a href="login.php" id="linkBulleLogin" title="">Connexion</a></li>'));
			$pageHeader->remplacePage('#inscription#', (Fonctions::isConnected() ? null : '<li><a href="inscriptionToken.php" id="linkBulleToken"  title="">Inscription</a></li>'));
			$pageHeader->remplacePage('#compte#', (Fonctions::isConnected() ? '<li><a href="monCompte.php" title="">Temp : Mon Compte</a></li>' : null));
			$pageHeader->remplacePage('#deconnexion#', (Fonctions::isConnected() ? '<li><a href="/Dll/Controleur/ControleurLogin.php?action=deconnexion" title="">Déconnexion</a></li>' : null));
			$pageHeader->remplacePage('#nav_compte#', (Fonctions::isConnected() ? Config::getRequireOnce('NAV_COMPTE_PHP') : null));

			//Affichage du contenu
			$this->header = $pageHeader->getPage();
		}

		/**
		 * @return string
		 */
		public function __toString()
		{
			return $this->header;
		}
	};