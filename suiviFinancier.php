<?php
	/**
	 * Created by PhpStorm.
	 * User: Vincent
	 * Date: 12/06/2018
	 * Time: 14:15
	 */

	//Déclaration du namespace
	namespace Apel;

	use Apel\Dll\Framework\cls_ConstruitTemplate;
	use Apel\Dll\Framework\Config;

	return new class()
	{
		private $page;

		public function __construct($layout = true)
		{
			$this->generatePage($layout);
		}

		private function generatePage(bool $layout)
		{
			//Initialisation du template
			$this->page = new cls_ConstruitTemplate(Config::getAdresse('SUIVI_FINANCIER'), $layout);
			
			$this->page->remplacePage('#begin_section#', (($layout) ? '<section>' : ''));

			$this->page->remplacePage('#title#', 'Suivi Financier');
			$this->page->remplacePage('#formulairetri#', 'Formulaire permettant de trier les éléments affiché dans le tableau du suivi financier');
			$this->page->remplacePage('#tableausuivi#', 'Tableau avec les différentes transaction de don et cotisation');

			$this->page->remplacePage('#end_section#', (($layout) ? '</section>' : ''));

			if($layout)
			{
				echo $this->page->getPage();
			}
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return $this->page;
		}
	};