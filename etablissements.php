<?php
	/**
	 * Created by PhpStorm.
	 * User: pythonirc
	 * Date: 18/07/18
	 * Time: 09:46
	 */

	//Déclaration du namespace

	namespace Apel;

	use Apel\Dll\Framework\cls_ConstruitTemplate;
	use Apel\Dll\Framework\Config;

	return new class()
	{
		private $page;

		public function __construct($layout = true)
		{
			$this->generatePage($layout);
		}

		private function generatePage(bool $layout)
		{
			//Initialisation du template
			$this->page = new cls_ConstruitTemplate(Config::getAdresse('ETABLISSEMENT'), $layout);
			$this->page->remplacePage('#begin_section#', (($layout) ? '<section>' : ''));
			$this->page->remplacePage('#title#', 'Etablissements');
			$this->page->remplacePage('#tableau_liste_etablissements#', 'Tableau liste etablissements généré par la classe de papi et la dao');
			$this->page->remplacePage('#infos_etablissement_selectionne#', 'Cadre dinfo genere par la classe de papi et la dao');
			$this->page->remplacePage('#openstreetmap_coord#', '&amp;marker=45.4570%2C4.4001');
			$this->page->remplacePage('#adresse_etablissement#', 'adresse de l\'etablissement selectionné');
			$this->page->remplacePage('#end_section#', (($layout) ? '</section>' : ''));

			if($layout)
			{
				echo $this->page->getPage();
			}
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return $this->page;
		}
	};
