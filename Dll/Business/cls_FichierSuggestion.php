<?php
	/**
	 * Created by PhpStorm.
	 * User: GuillaumeFixe
	 * Date: 04/06/2018
	 * Time: 17:41
	 */

	namespace Apel\Dll\Business;

	class cls_FichierSuggestion
	{
		/** @var integer $idFichierSuggestion */
		private $idFichierSuggestion;
		/** @var string $chemin */
		private $chemin;

		/**
		 * cls_FichierSuggestion constructor.
		 *
		 * @param int    $idFichierSuggestion
		 * @param string $chemin
		 */
		public function __construct($idFichierSuggestion, $chemin)
		{
			$this->idFichierSuggestion = $idFichierSuggestion;
			$this->chemin              = $chemin;
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdFichierSuggestion()
		{
			return $this->idFichierSuggestion;
		}

		/**
		 * @param int $idFichierSuggestion
		 */
		public function setIdFichierSuggestion($idFichierSuggestion)
		{
			$this->idFichierSuggestion = $idFichierSuggestion;
		}

		/**
		 * @return string
		 */
		public function getChemin()
		{
			return $this->chemin;
		}

		/**
		 * @param string $chemin
		 */
		public function setChemin($chemin)
		{
			$this->chemin = $chemin;
		}
		//</editor-fold>

		/**
		 * @return string
		 */
		public function __toString()
		{
			return ' Fichier N°' . $this->idFichierSuggestion . ' : ' . $this->chemin;
		}
	}