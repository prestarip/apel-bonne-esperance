<?php
	/**
	 * Created by PhpStorm.
	 * User: Guillaume
	 * Date: 01/06/2018
	 * Time: 15:19
	 */

	namespace Apel\Dll\Business;

	class cls_CategorieEtab
	{
		private $idCategorieEtab;
		private $libelle;

		/**
		 * cls_CategorieEtab constructor.
		 *
		 * @param $idCategorieEtab
		 * @param $libelle
		 */
		public function __construct($idCategorieEtab, $libelle)
		{
			$this->idCategorieEtab = $idCategorieEtab;
			$this->libelle         = $libelle;
		}

		//<editor-fold desc="Accesseur">

		/**
		 * @return mixed
		 */
		public function getIdCategorieEtab()
		{
			return $this->idCategorieEtab;
		}

		/**
		 * @param mixed $idCategorieEtab
		 */
		public function setIdCategorieEtab($idCategorieEtab)
		{
			$this->idCategorieEtab = $idCategorieEtab;
		}

		/**
		 * @return mixed
		 */
		public function getLibelle()
		{
			return $this->libelle;
		}

		/**
		 * @param mixed $libelle
		 */
		public function setLibelle($libelle)
		{
			$this->libelle = $libelle;
		}
		//</editor-fold>

		/**
		 * @return string
		 */
		public function __toString()
		{
			return $this->libelle;
		}
	}