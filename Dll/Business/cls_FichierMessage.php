<?php
	/**
	 * Created by PhpStorm.
	 * User: GuillaumeFixe
	 * Date: 04/06/2018
	 * Time: 19:15
	 */

	namespace Apel\Dll\Business;

	class cls_FichierMessage
	{
		/** @var integer $idFichierMessage */
		private $idFichierMessage;
		/** @var string $chemin */
		private $chemin;

		/**
		 * cls_FichierMessage constructor.
		 *
		 * @param int    $idFichierMessage
		 * @param string $chemin
		 */
		public function __construct($idFichierMessage, $chemin)
		{
			$this->idFichierMessage = $idFichierMessage;
			$this->chemin           = $chemin;
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdFichierMessage()
		{
			return $this->idFichierMessage;
		}

		/**
		 * @param int $idFichierMessage
		 */
		public function setIdFichierMessage($idFichierMessage)
		{
			$this->idFichierMessage = $idFichierMessage;
		}

		/**
		 * @return string
		 */
		public function getChemin()
		{
			return $this->chemin;
		}

		/**
		 * @param string $chemin
		 */
		public function setChemin($chemin)
		{
			$this->chemin = $chemin;
		}
		//</editor-fold>

		/**
		 * @return string
		 */
		public function __toString()
		{
			// TODO: Implement __toString() method.
			return '';
		}
	}