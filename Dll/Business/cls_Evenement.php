<?php
	/**
	 * Created by PhpStorm.
	 * User: GuillaumeFixe
	 * Date: 04/06/2018
	 * Time: 17:49
	 */

	namespace Apel\Dll\Business;

	class cls_Evenement
	{
		/** @var integer $idEvenement */
		private $idEvenement;
		/** @var string $libelle */
		private $libelle;
		/** @var \DateTime $dateEvenement */
		private $dateEvenement;
		/** @var \DateTime $dateCreation */
		private $dateCreation;
		/** @var integer $nbInscrit */
		private $nbInscrit;
		/** @var integer $nbParticipant */
		private $nbParticipant;
		/** @var cls_Compte $compte */
		private $compte;

		/**
		 * cls_Evenement constructor.
		 *
		 * @param int        $idEvenement
		 * @param string     $libelle
		 * @param \DateTime  $dateEvenement
		 * @param \DateTime  $dateCreation
		 * @param cls_Compte $compte
		 */
		public function __construct($idEvenement, $libelle, \DateTime $dateEvenement, \DateTime $dateCreation, cls_Compte $compte)
		{
			$this->idEvenement   = $idEvenement;
			$this->libelle       = $libelle;
			$this->dateEvenement = $dateEvenement;
			$this->dateCreation  = $dateCreation;
			$this->compte        = $compte;
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdEvenement()
		{
			return $this->idEvenement;
		}

		/**
		 * @param int $idEvenement
		 */
		public function setIdEvenement($idEvenement)
		{
			$this->idEvenement = $idEvenement;
		}

		/**
		 * @return string
		 */
		public function getLibelle()
		{
			return $this->libelle;
		}

		/**
		 * @param string $libelle
		 */
		public function setLibelle($libelle)
		{
			$this->libelle = $libelle;
		}

		/**
		 * @return \DateTime
		 */
		public function getDateEvenement()
		{
			return $this->dateEvenement;
		}

		/**
		 * @param \DateTime $dateEvenement
		 */
		public function setDateEvenement($dateEvenement)
		{
			$this->dateEvenement = $dateEvenement;
		}

		/**
		 * @return \DateTime
		 */
		public function getDateCreation()
		{
			return $this->dateCreation;
		}

		/**
		 * @param \DateTime $dateCreation
		 */
		public function setDateCreation($dateCreation)
		{
			$this->dateCreation = $dateCreation;
		}

		/**
		 * @return int
		 */
		public function getNbInscrit()
		{
			return $this->nbInscrit;
		}

		/**
		 * @param int $nbInscrit
		 */
		public function setNbInscrit($nbInscrit)
		{
			$this->nbInscrit = $nbInscrit;
		}

		/**
		 * @return int
		 */
		public function getNbParticipant()
		{
			return $this->nbParticipant;
		}

		/**
		 * @param int $nbParticipant
		 */
		public function setNbParticipant($nbParticipant)
		{
			$this->nbParticipant = $nbParticipant;
		}

		/**
		 * @return cls_Compte
		 */
		public function getCompte()
		{
			return $this->compte;
		}

		/**
		 * @param cls_Compte $compte
		 */
		public function setCompte($compte)
		{
			$this->compte = $compte;
		}
		//</editor-fold>

		/**
		 * @return string
		 */
		public function __toString()
		{
			return $this->libelle . ' - le ' . $this->dateEvenement->format('Y-m-d H:i:s');
		}
	}