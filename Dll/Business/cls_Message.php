<?php
	/**
	 * Created by PhpStorm.
	 * User: GuillaumeFixe
	 * Date: 04/06/2018
	 * Time: 19:14
	 */

	namespace Apel\Dll\Business;

	class cls_Message
	{
		/** @var integer $idMessage */
		private $idMessage;
		/** @var string $contenu */
		private $contenu;
		/** @var \DateTime $dateEnvoi */
		private $dateEnvoi;
		/** @var array $listeDestinataire */
		private $listeDestinataire;
//		private $chemin;

		/**
		 * cls_Message constructor.
		 *
		 * @param int       $idMessage
		 * @param string    $contenu
		 * @param \DateTime $dateEnvoi
		 * @param array     $listeDestinataire
		 */
		public function __construct($idMessage, $contenu, \DateTime $dateEnvoi, array $listeDestinataire)
		{
			$this->idMessage         = $idMessage;
			$this->contenu           = $contenu;
			$this->dateEnvoi         = $dateEnvoi;
			$this->listeDestinataire = $listeDestinataire;
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdMessage()
		{
			return $this->idMessage;
		}

		/**
		 * @param int $idMessage
		 */
		public function setIdMessage($idMessage)
		{
			$this->idMessage = $idMessage;
		}

		/**
		 * @return string
		 */
		public function getContenu()
		{
			return $this->contenu;
		}

		/**
		 * @param string $contenu
		 */
		public function setContenu($contenu)
		{
			$this->contenu = $contenu;
		}

		/**
		 * @return \DateTime
		 */
		public function getDateEnvoi()
		{
			return $this->dateEnvoi;
		}

		/**
		 * @param \DateTime $dateEnvoi
		 */
		public function setDateEnvoi($dateEnvoi)
		{
			$this->dateEnvoi = $dateEnvoi;
		}

		/**
		 * @return array
		 */
		public function getListeDestinataire()
		{
			return $this->listeDestinataire;
		}

		/**
		 * @param array $listeDestinataire
		 */
		public function setListeDestinataire($listeDestinataire)
		{
			$this->listeDestinataire = $listeDestinataire;
		}
		//</editor-fold>

		/**
		 * @return string
		 */
		public function __toString()
		{
			return $this->contenu;
		}
	}