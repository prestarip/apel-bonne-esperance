<?php
	/**
	 * Created by PhpStorm.
	 * User: GuillaumeFixe
	 * Date: 04/06/2018
	 * Time: 18:36
	 */

	namespace Apel\Dll\Business;

	class cls_MouvementFinancier
	{
		/** @var integer $idMouvementFinancier */
		private $idMouvementFinancier;
		/** @var \DateTime $dateMouvement */
		private $dateMouvement;
		/** @var integer $montant */
		private $montant;
		/** @var cls_TypeMouvement $typeMouvement */
		private $typeMouvement;
		/** @var cls_Compte $compte */
		private $compte;

		/**
		 * cls_MouvementFinancier constructor.
		 *
		 * @param int               $idMouvementFinancier
		 * @param \DateTime         $dateMouvement
		 * @param int               $montant
		 * @param cls_TypeMouvement $typeMouvement
		 * @param cls_Compte        $compte
		 */
		public function __construct($idMouvementFinancier, \DateTime $dateMouvement, $montant, cls_TypeMouvement $typeMouvement, cls_Compte $compte)
		{
			$this->idMouvementFinancier = $idMouvementFinancier;
			$this->dateMouvement        = $dateMouvement;
			$this->montant              = $montant;
			$this->typeMouvement        = $typeMouvement;
			$this->compte               = $compte;
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdMouvementFinancier()
		{
			return $this->idMouvementFinancier;
		}

		/**
		 * @param int $idMouvementFinancier
		 */
		public function setIdMouvementFinancier($idMouvementFinancier)
		{
			$this->idMouvementFinancier = $idMouvementFinancier;
		}

		/**
		 * @return \DateTime
		 */
		public function getDateMouvement()
		{
			return $this->dateMouvement;
		}

		/**
		 * @param \DateTime $dateMouvement
		 */
		public function setDateMouvement($dateMouvement)
		{
			$this->dateMouvement = $dateMouvement;
		}

		/**
		 * @return int
		 */
		public function getMontant()
		{
			return $this->montant;
		}

		/**
		 * @param int $montant
		 */
		public function setMontant($montant)
		{
			$this->montant = $montant;
		}

		/**
		 * @return cls_TypeMouvement
		 */
		public function getTypeMouvement()
		{
			return $this->typeMouvement;
		}

		/**
		 * @param cls_TypeMouvement $typeMouvement
		 */
		public function setTypeMouvement($typeMouvement)
		{
			$this->typeMouvement = $typeMouvement;
		}

		/**
		 * @return cls_Compte
		 */
		public function getCompte()
		{
			return $this->compte;
		}

		/**
		 * @param cls_Compte $compte
		 */
		public function setCompte($compte)
		{
			$this->compte = $compte;
		}
		//</editor-fold>

		/**
		 * @return string
		 */
		public function __toString()
		{
			return 'Mouvement de type ' . $this->typeMouvement->getLibelle() . ' fait le ' . $this->dateMouvement->format('Y-m-d H:i:s') . ' par ' . $this->compte;
		}
	}