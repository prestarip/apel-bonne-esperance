<?php
	/**
	 * Created by PhpStorm.
	 * User: Guillaume
	 * Date: 21/05/2018
	 * Time: 14:13
	 */

	namespace Apel\Dll\Business;

	/**
	 * Class cls_ForumMessage
	 * @package Apel\Dll\Business
	 */
	class cls_ForumMessage
	{
		/** @var integer $idForumMessage */
		private $idForumMessage;
		/** @var string $message */
		private $message;
		/** @var \DateTime $dateMessage */
		private $dateMessage;
		/** @var cls_ForumTopic $forumTopic */
		private $forumTopic;
		/** @var cls_Compte $compte */
		private $compte;

		/**
		 * cls_ForumMessage constructor.
		 *
		 * @param int            $idForumMessage
		 * @param string         $message
		 * @param \DateTime      $dateMessage
		 * @param cls_ForumTopic $forumTopic
		 * @param cls_Compte     $compte
		 */
		public function __construct($idForumMessage, $message, \DateTime $dateMessage, cls_ForumTopic $forumTopic, cls_Compte $compte)
		{
			$this->idForumMessage = $idForumMessage;
			$this->message        = $message;
			$this->dateMessage    = $dateMessage;
			$this->forumTopic     = $forumTopic;
			$this->compte         = $compte;
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdForumMessage()
		{
			return $this->idForumMessage;
		}

		/**
		 * @param int $idForumMessage
		 */
		public function setIdForumMessage($idForumMessage)
		{
			$this->idForumMessage = $idForumMessage;
		}

		/**
		 * @return string
		 */
		public function getMessage()
		{
			return $this->message;
		}

		/**
		 * @param string $message
		 */
		public function setMessage($message)
		{
			$this->message = $message;
		}

		/**
		 * @return \DateTime
		 */
		public function getDateMessage()
		{
			return $this->dateMessage;
		}

		/**
		 * @param \DateTime $dateMessage
		 */
		public function setDateMessage($dateMessage)
		{
			$this->dateMessage = $dateMessage;
		}

		/**
		 * @return cls_ForumTopic
		 */
		public function getForumTopic()
		{
			return $this->forumTopic;
		}

		/**
		 * @param cls_ForumTopic $forumTopic
		 */
		public function setForumTopic($forumTopic)
		{
			$this->forumTopic = $forumTopic;
		}

		/**
		 * @return cls_Compte
		 */
		public function getCompte()
		{
			return $this->compte;
		}

		/**
		 * @param cls_Compte $compte
		 */
		public function setCompte($compte)
		{
			$this->compte = $compte;
		}
		//</editor-fold>

		/**
		 * @return string
		 */
		public function __toString()
		{
			return $this->message;
		}
	}