<?php
	/**
	 * Created by PhpStorm.
	 * User: GuillaumeFixe
	 * Date: 04/06/2018
	 * Time: 19:15
	 */

	namespace Apel\Dll\Business;

	class cls_Destinataire
	{
		/** @var cls_Compte $compte */
		private $compte;
		/** @var cls_Message $Message */
		private $Message;
		/** @var cls_TypeDestinataire $typeDestinataire */
		private $typeDestinataire;
	}