<?php
	/**
	 * Created by PhpStorm.
	 * User: GuillaumeFixe
	 * Date: 04/06/2018
	 * Time: 19:06
	 */

	namespace Apel\Dll\Business;

	class cls_Document
	{
		/** @var integer $idDocument */
		private $idDocument;
		/** @var string $chemin */
		private $chemin;
		/** @var string $description */
		private $description;
		/** @var \DateTime $dateUpdate */
		private $dateUpdate;
		/** @var cls_TypeDocument $typeDocument */
		private $typeDocument;
		/** @var cls_Compte $compte */
		private $compte;

		/**
		 * cls_Document constructor.
		 *
		 * @param int              $idDocument
		 * @param string           $chemin
		 * @param string           $description
		 * @param \DateTime        $dateUpdate
		 * @param cls_TypeDocument $typeDocument
		 * @param cls_Compte       $compte
		 */
		public function __construct($idDocument, $chemin, $description, \DateTime $dateUpdate, cls_TypeDocument $typeDocument, cls_Compte $compte)
		{
			$this->idDocument   = $idDocument;
			$this->chemin       = $chemin;
			$this->description  = $description;
			$this->dateUpdate   = $dateUpdate;
			$this->typeDocument = $typeDocument;
			$this->compte       = $compte;
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdDocument()
		{
			return $this->idDocument;
		}

		/**
		 * @param int $idDocument
		 */
		public function setIdDocument($idDocument)
		{
			$this->idDocument = $idDocument;
		}

		/**
		 * @return string
		 */
		public function getChemin()
		{
			return $this->chemin;
		}

		/**
		 * @param string $chemin
		 */
		public function setChemin($chemin)
		{
			$this->chemin = $chemin;
		}

		/**
		 * @return string
		 */
		public function getDescription()
		{
			return $this->description;
		}

		/**
		 * @param string $description
		 */
		public function setDescription($description)
		{
			$this->description = $description;
		}

		/**
		 * @return \DateTime
		 */
		public function getDateUpdate()
		{
			return $this->dateUpdate;
		}

		/**
		 * @param \DateTime $dateUpdate
		 */
		public function setDateUpdate($dateUpdate)
		{
			$this->dateUpdate = $dateUpdate;
		}

		/**
		 * @return cls_TypeDocument
		 */
		public function getTypeDocument()
		{
			return $this->typeDocument;
		}

		/**
		 * @param cls_TypeDocument $typeDocument
		 */
		public function setTypeDocument($typeDocument)
		{
			$this->typeDocument = $typeDocument;
		}

		/**
		 * @return cls_Compte
		 */
		public function getCompte()
		{
			return $this->compte;
		}

		/**
		 * @param cls_Compte $compte
		 */
		public function setCompte($compte)
		{
			$this->compte = $compte;
		}
		//</editor-fold>

		/**
		 * @return string
		 */
		public function __toString()
		{
			// TODO: Implement __toString() method.
			return '';
		}
	}