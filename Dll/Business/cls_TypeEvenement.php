<?php
	/**
	 * Created by PhpStorm.
	 * User: GuillaumeFixe
	 * Date: 04/06/2018
	 * Time: 17:50
	 */

	namespace Apel\Dll\Business;

	class cls_TypeEvenement
	{
		/** @var integer $idTypeEvenement */
		private $idTypeEvenement;
		/** @var string $libelle */
		private $libelle;

		/**
		 * clsTypeEvenement constructor.
		 *
		 * @param int    $idTypeEvenement
		 * @param string $libelle
		 */
		public function __construct($idTypeEvenement, $libelle)
		{
			$this->idTypeEvenement = $idTypeEvenement;
			$this->libelle         = $libelle;
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdTypeEvenement()
		{
			return $this->idTypeEvenement;
		}

		/**
		 * @param int $idTypeEvenement
		 */
		public function setIdTypeEvenement($idTypeEvenement)
		{
			$this->idTypeEvenement = $idTypeEvenement;
		}

		/**
		 * @return string
		 */
		public function getLibelle()
		{
			return $this->libelle;
		}

		/**
		 * @param string $libelle
		 */
		public function setLibelle($libelle)
		{
			$this->libelle = $libelle;
		}
		//</editor-fold>

		/**
		 * @return string
		 */
		public function __toString()
		{
			return $this->libelle;
		}
	}