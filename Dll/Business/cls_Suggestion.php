<?php
	/**
	 * Created by PhpStorm.
	 * User: GuillaumeFixe
	 * Date: 04/06/2018
	 * Time: 17:40
	 */

	namespace Apel\Dll\Business;

	class cls_Suggestion
	{
		/** @var integer $idSuggestion */
		private $idSuggestion;
		/** @var string $contenu */
		private $contenu;
		/** @var \DateTime $dateSuggestion */
		private $dateSuggestion;
		/** @var cls_FichierSuggestion $fichierSuggestion */
		private $fichierSuggestion;
		/** @var cls_Compte $compte */
		private $compte;

		/**
		 * cls_Suggestion constructor.
		 *
		 * @param int                   $idSuggestion
		 * @param string                $contenu
		 * @param \DateTime             $dateSuggestion
		 * @param cls_FichierSuggestion $fichierSuggestion
		 * @param cls_Compte            $compte
		 */
		public function __construct($idSuggestion, $contenu, \DateTime $dateSuggestion, cls_FichierSuggestion $fichierSuggestion, cls_Compte $compte)
		{
			$this->idSuggestion      = $idSuggestion;
			$this->contenu           = $contenu;
			$this->dateSuggestion    = $dateSuggestion;
			$this->fichierSuggestion = $fichierSuggestion;
			$this->compte            = $compte;
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdSuggestion()
		{
			return $this->idSuggestion;
		}

		/**
		 * @param int $idSuggestion
		 */
		public function setIdSuggestion($idSuggestion)
		{
			$this->idSuggestion = $idSuggestion;
		}

		/**
		 * @return string
		 */
		public function getContenu()
		{
			return $this->contenu;
		}

		/**
		 * @param string $contenu
		 */
		public function setContenu($contenu)
		{
			$this->contenu = $contenu;
		}

		/**
		 * @return \DateTime
		 */
		public function getDateSuggestion()
		{
			return $this->dateSuggestion;
		}

		/**
		 * @param \DateTime $dateSuggestion
		 */
		public function setDateSuggestion($dateSuggestion)
		{
			$this->dateSuggestion = $dateSuggestion;
		}

		/**
		 * @return cls_FichierSuggestion
		 */
		public function getFichierSuggestion()
		{
			return $this->fichierSuggestion;
		}

		/**
		 * @param cls_FichierSuggestion $fichierSuggestion
		 */
		public function setFichierSuggestion($fichierSuggestion)
		{
			$this->fichierSuggestion = $fichierSuggestion;
		}

		/**
		 * @return cls_Compte
		 */
		public function getCompte()
		{
			return $this->compte;
		}

		/**
		 * @param cls_Compte $compte
		 */
		public function setCompte($compte)
		{
			$this->compte = $compte;
		}

		//</editor-fold>

		/**
		 * @return string
		 */
		public function __toString()
		{
			return 'le ' . $this->dateSuggestion->format('Y-m-d H:i:s') . ' - ' . $this->contenu;
		}
	}