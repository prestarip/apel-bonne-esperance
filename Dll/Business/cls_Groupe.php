<?php
	/**
	 * Created by PhpStorm.
	 * User: Guillaume
	 * Date: 01/06/2018
	 * Time: 13:12
	 */

	namespace Apel\Dll\Business;

	class cls_Groupe
	{
		/** @var integer $idGroupe */
		private $idGroupe;
		/** @var string $codeGroupe */
		private $codeGroupe;
		/** @var string $libelle */
		private $libelle;
		/** @var array $listeRole */
		private $listeRole;

		/**
		 * cls_Groupe constructor.
		 *
		 * @param int    $idGroupe
		 * @param string $codeGroupe
		 * @param string $libelle
		 */
		public function __construct($idGroupe, $codeGroupe, $libelle)
		{
			$this->idGroupe   = $idGroupe;
			$this->codeGroupe = $codeGroupe;
			$this->libelle    = $libelle;
			$this->listeRole  = array();
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdGroupe()
		{
			return $this->idGroupe;
		}

		/**
		 * @param int $idGroupe
		 */
		public function setIdGroupe($idGroupe)
		{
			$this->idGroupe = $idGroupe;
		}

		/**
		 * @return string
		 */
		public function getCodeGroupe()
		{
			return $this->codeGroupe;
		}

		/**
		 * @param string $codeGroupe
		 */
		public function setCodeGroupe($codeGroupe)
		{
			$this->codeGroupe = $codeGroupe;
		}

		/**
		 * @return string
		 */
		public function getLibelle()
		{
			return $this->libelle;
		}

		/**
		 * @param string $libelle
		 */
		public function setLibelle($libelle)
		{
			$this->libelle = $libelle;
		}

		/**
		 * @return array
		 */
		public function getListeRole()
		{
			return $this->listeRole;
		}

		/**
		 * @param array $listeRole
		 */
		public function setListeRole($listeRole)
		{
			$this->listeRole = $listeRole;
		}

		//</editor-fold>

		public function __toString()
		{
			return $this->libelle;
		}
	}