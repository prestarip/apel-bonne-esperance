<?php
	/**
	 * Created by PhpStorm.
	 * User: GuillaumeFixe
	 * Date: 04/06/2018
	 * Time: 19:15
	 */

	namespace Apel\Dll\Business;

	class cls_TypeDestinataire
	{
		/** @var integer $idTypeDestinataire */
		private $idTypeDestinataire;
		/** @var string $libelle */
		private $libelle;

		/**
		 * cls_TypeDestinataire constructor.
		 *
		 * @param int    $idTypeDestinataire
		 * @param string $libelle
		 */
		public function __construct($idTypeDestinataire, $libelle)
		{
			$this->idTypeDestinataire = $idTypeDestinataire;
			$this->libelle            = $libelle;
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdTypeDestinataire()
		{
			return $this->idTypeDestinataire;
		}

		/**
		 * @param int $idTypeDestinataire
		 */
		public function setIdTypeDestinataire($idTypeDestinataire)
		{
			$this->idTypeDestinataire = $idTypeDestinataire;
		}

		/**
		 * @return string
		 */
		public function getLibelle()
		{
			return $this->libelle;
		}

		/**
		 * @param string $libelle
		 */
		public function setLibelle($libelle)
		{
			$this->libelle = $libelle;
		}
		//</editor-fold>

		/**
		 * @return string
		 */
		public function __toString()
		{
			return $this->libelle;
		}
	}