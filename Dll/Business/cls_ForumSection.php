<?php
	/**
	 * Created by PhpStorm.
	 * User: Guillaume
	 * Date: 22/05/2018
	 * Time: 13:02
	 */

	namespace Apel\Dll\Business;

	/**
	 * Class cls_ForumSection
	 * @package Apel\Dll\Business
	 */
	class cls_ForumSection
	{
		/** @var integer $idForumSection */
		private $idForumSection;
		/** @var string $libelle */
		private $libelle;

		/**
		 * cls_ForumSection constructor.
		 *
		 * @param int    $idForumSection
		 * @param string $libelle
		 */
		public function __construct($idForumSection, $libelle)
		{
			$this->idForumSection = $idForumSection;
			$this->libelle        = $libelle;
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdForumSection()
		{
			return $this->idForumSection;
		}

		/**
		 * @param int $idForumSection
		 */
		public function setIdForumSection($idForumSection)
		{
			$this->idForumSection = $idForumSection;
		}

		/**
		 * @return string
		 */
		public function getLibelle()
		{
			return $this->libelle;
		}

		/**
		 * @param string $libelle
		 */
		public function setLibelle($libelle)
		{
			$this->libelle = $libelle;
		}
		//</editor-fold>

		/**
		 * @return string
		 */
		public function __toString()
		{
			return $this->libelle;
		}
	}