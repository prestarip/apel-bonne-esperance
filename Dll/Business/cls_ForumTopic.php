<?php
	/**
	 * Created by PhpStorm.
	 * User: Guillaume
	 * Date: 22/05/2018
	 * Time: 12:41
	 */

	namespace Apel\Dll\Business;

	/**
	 * Class cls_ForumTopic
	 * @package Apel\Dll\Business
	 */
	class cls_ForumTopic
	{
		/** @var integer $idForumTopic */
		private $idForumTopic;
		/** @var string $libelle */
		private $libelle;
		/** @var cls_ForumSection $forumSection */
		private $forumSection;

		/**
		 * cls_ForumTopic constructor.
		 *
		 * @param int              $idForumTopic
		 * @param string           $libelle
		 * @param cls_ForumSection $forumSection
		 */
		public function __construct($idForumTopic, $libelle, cls_ForumSection $forumSection)
		{
			$this->idForumTopic = $idForumTopic;
			$this->libelle      = $libelle;
			$this->forumSection = $forumSection;
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdForumTopic()
		{
			return $this->idForumTopic;
		}

		/**
		 * @param int $idForumTopic
		 */
		public function setIdForumTopic($idForumTopic)
		{
			$this->idForumTopic = $idForumTopic;
		}

		/**
		 * @return string
		 */
		public function getLibelle()
		{
			return $this->libelle;
		}

		/**
		 * @param string $libelle
		 */
		public function setLibelle($libelle)
		{
			$this->libelle = $libelle;
		}

		/**
		 * @return cls_ForumSection
		 */
		public function getForumSection()
		{
			return $this->forumSection;
		}

		/**
		 * @param cls_ForumSection $forumSection
		 */
		public function setForumSection($forumSection)
		{
			$this->forumSection = $forumSection;
		}
		//</editor-fold>

		/**
		 * @return string
		 */
		public function __toString()
		{
			return $this->libelle;
		}
	}