<?php
	/**
	 * Created by PhpStorm.
	 * User: GuillaumeFixe
	 * Date: 04/06/2018
	 * Time: 19:06
	 */

	namespace Apel\Dll\Business;

	class cls_TypeDocument
	{
		/** @var integer $idTypeDocument */
		private $idTypeDocument;
		/** @var string $libelle */
		private $libelle;

		/**
		 * cls_TypeDocument constructor.
		 *
		 * @param int    $idTypeDocument
		 * @param string $libelle
		 */
		public function __construct($idTypeDocument, $libelle)
		{
			$this->idTypeDocument = $idTypeDocument;
			$this->libelle        = $libelle;
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdTypeDocument()
		{
			return $this->idTypeDocument;
		}

		/**
		 * @param int $idTypeDocument
		 */
		public function setIdTypeDocument($idTypeDocument)
		{
			$this->idTypeDocument = $idTypeDocument;
		}

		/**
		 * @return string
		 */
		public function getLibelle()
		{
			return $this->libelle;
		}

		/**
		 * @param string $libelle
		 */
		public function setLibelle($libelle)
		{
			$this->libelle = $libelle;
		}
		//</editor-fold>

		/**
		 * @return string
		 */
		public function __toString()
		{
			return $this->libelle;
		}
	}