<?php
	/**
	 * Created by PhpStorm.
	 * User: GuillaumeFixe
	 * Date: 24/05/2018
	 * Time: 19:34
	 */

	namespace Apel\Dll\Business;

	/**
	 * Class cls_Compte
	 * @package Apel\Dll\Business
	 */
	class cls_Compte
	{
		//<editor-fold desc="Propriétés">
		/** @var integer $idCompte */
		private $idCompte;
		/** @var string $nom */
		private $nom;
		/** @var string $prenom */
		private $prenom;
		/** @var string $mail */
		private $mail;
		/** @var string $motDePasse */
		private $motDePasse;
		/** @var \DateTime $dateNaissance */
		private $dateNaissance;
		/** @var \DateTime $dateInscription */
		private $dateInscription;
		/** @var cls_Civilite $civilite */
		private $civilite;
		/** @var cls_Groupe $groupe */
		private $groupe;
		/** @var cls_Token $token */
//		private $token;
		//</editor-fold>

		/**
		 * cls_Compte constructor.
		 *
		 * @param int          $idCompte
		 * @param string       $nom
		 * @param string       $prenom
		 * @param string       $mail
		 * @param string       $motDePasse
		 * @param \DateTime    $dateNaissance
		 * @param \DateTime    $dateInscription
		 * @param cls_Civilite $civilite
		 * @param cls_Groupe   $groupe
		 * //         * @param cls_Token $token
		 */
		public function __construct($idCompte, $nom, $prenom, $mail, $motDePasse, \DateTime $dateNaissance, \DateTime $dateInscription, cls_Civilite $civilite, cls_Groupe $groupe/*, cls_Token $token*/)
		{
			$this->idCompte        = $idCompte;
			$this->nom             = $nom;
			$this->prenom          = $prenom;
			$this->mail            = $mail;
			$this->motDePasse      = $motDePasse;
			$this->dateNaissance   = $dateNaissance;
			$this->dateInscription = $dateInscription;
			$this->civilite        = $civilite;
			$this->groupe          = $groupe;
//			$this->token           = $token;
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdCompte()
		{
			return $this->idCompte;
		}

		/**
		 * @param int $idCompte
		 */
		public function setIdCompte($idCompte)
		{
			$this->idCompte = $idCompte;
		}

		/**
		 * @return string
		 */
		public function getNom()
		{
			return $this->nom;
		}

		/**
		 * @param string $nom
		 */
		public function setNom($nom)
		{
			$this->nom = $nom;
		}

		/**
		 * @return string
		 */
		public function getPrenom()
		{
			return $this->prenom;
		}

		/**
		 * @param string $prenom
		 */
		public function setPrenom($prenom)
		{
			$this->prenom = $prenom;
		}

		/**
		 * @return string
		 */
		public function getMail()
		{
			return $this->mail;
		}

		/**
		 * @param string $mail
		 */
		public function setMail($mail)
		{
			$this->mail = $mail;
		}

		/**
		 * @return string
		 */
		public function getMotDePasse()
		{
			return $this->motDePasse;
		}

		/**
		 * @param string $motDePasse
		 */
		public function setMotDePasse($motDePasse)
		{
			$this->motDePasse = $motDePasse;
		}

		/**
		 * @return \DateTime
		 */
		public function getDateNaissance()
		{
			return $this->dateNaissance;
		}

		/**
		 * @param \DateTime $dateNaissance
		 */
		public function setDateNaissance($dateNaissance)
		{
			$this->dateNaissance = $dateNaissance;
		}

		/**
		 * @return \DateTime
		 */
		public function getDateInscription()
		{
			return $this->dateInscription;
		}

		/**
		 * @param \DateTime $dateInscription
		 */
		public function setDateInscription($dateInscription)
		{
			$this->dateInscription = $dateInscription;
		}

		/**
		 * @return cls_Civilite
		 */
		public function getCivilite() : cls_Civilite
		{
			return $this->civilite;
		}

		/**
		 * @param cls_Civilite $civilite
		 */
		public function setCivilite(cls_Civilite $civilite)
		{
			$this->civilite = $civilite;
		}

		/**
		 * @return string
		 */
		public function getAdresse() : string
		{
			return $this->adresse;
		}

		/**
		 * @param string $adresse
		 */
		public function setAdresse(string $adresse)
		{
			$this->adresse = $adresse;
		}

		/**
		 * @return cls_Groupe
		 */
		public function getGroupe() : cls_Groupe
		{
			return $this->groupe;
		}

		/**
		 * @param cls_Groupe $groupe
		 */
		public function setGroupe(cls_Groupe $groupe)
		{
			$this->groupe = $groupe;
		}

		//</editor-fold>

		public static function getPropertiesClass()
		{
			return get_class_vars(self::class);
		}

		public function getDateNaissanceString($format)
		{
			$dateString = $this->dateNaissance->format($format);

			return $dateString;
		}

		/**
		 * @return string
		 */
		public function __toString()
		{
			return $this->nom . ' ' . $this->prenom;
		}
	}