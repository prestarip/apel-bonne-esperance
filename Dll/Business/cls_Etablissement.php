<?php
	/**
	 * Created by PhpStorm.
	 * User: Guillaume
	 * Date: 01/06/2018
	 * Time: 15:19
	 */

	namespace Apel\Dll\Business;

	class cls_Etablissement
	{
		/** @var integer $idEtablissement */
		private $idEtablissement;
		/** @var string $nom */
		private $nom;
		/** @var string $information */
		private $information;
		/** @var cls_CategorieEtab $categorieEtab */
		private $categorieEtab;
		/** @var clsAdresse $adresse */
		private $adresse;

		/**
		 * cls_Etablissement constructor.
		 *
		 * @param int               $idEtablissement
		 * @param string            $nom
		 * @param string            $information
		 * @param cls_CategorieEtab $categorieEtab
		 * @param clsAdresse        $adresse
		 */
		public function __construct($idEtablissement, $nom, $information, cls_CategorieEtab $categorieEtab, clsAdresse $adresse)
		{
			$this->idEtablissement = $idEtablissement;
			$this->nom             = $nom;
			$this->information     = $information;
			$this->categorieEtab   = $categorieEtab;
			$this->adresse         = $adresse;
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdEtablissement()
		{
			return $this->idEtablissement;
		}

		/**
		 * @param int $idEtablissement
		 */
		public function setIdEtablissement($idEtablissement)
		{
			$this->idEtablissement = $idEtablissement;
		}

		/**
		 * @return string
		 */
		public function getNom()
		{
			return $this->nom;
		}

		/**
		 * @param string $nom
		 */
		public function setNom($nom)
		{
			$this->nom = $nom;
		}

		/**
		 * @return string
		 */
		public function getInformation()
		{
			return $this->information;
		}

		/**
		 * @param string $information
		 */
		public function setInformation($information)
		{
			$this->information = $information;
		}

		/**
		 * @return cls_CategorieEtab
		 */
		public function getCategorieEtab()
		{
			return $this->categorieEtab;
		}

		/**
		 * @param cls_CategorieEtab $categorieEtab
		 */
		public function setCategorieEtab($categorieEtab)
		{
			$this->categorieEtab = $categorieEtab;
		}

		/**
		 * @return clsAdresse
		 */
		public function getAdresse()
		{
			return $this->adresse;
		}

		/**
		 * @param clsAdresse $adresse
		 */
		public function setAdresse($adresse)
		{
			$this->adresse = $adresse;
		}
		//</editor-fold>

		/**
		 * @return string
		 */
		public function __toString()
		{
			// TODO: Implement __toString() method.
			return "";
		}
	}