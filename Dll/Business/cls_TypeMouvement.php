<?php
	/**
	 * Created by PhpStorm.
	 * User: GuillaumeFixe
	 * Date: 04/06/2018
	 * Time: 18:36
	 */

	namespace Apel\Dll\Business;

	class cls_TypeMouvement
	{
		/** @var integer $idTypeMouvement */
		private $idTypeMouvement;
		/** @var string $libelle */
		private $libelle;

		/**
		 * cls_TypeMouvement constructor.
		 *
		 * @param int    $idTypeMouvement
		 * @param string $libelle
		 */
		public function __construct($idTypeMouvement, $libelle)
		{
			$this->idTypeMouvement = $idTypeMouvement;
			$this->libelle         = $libelle;
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdTypeMouvement()
		{
			return $this->idTypeMouvement;
		}

		/**
		 * @param int $idTypeMouvement
		 */
		public function setIdTypeMouvement($idTypeMouvement)
		{
			$this->idTypeMouvement = $idTypeMouvement;
		}

		/**
		 * @return string
		 */
		public function getLibelle()
		{
			return $this->libelle;
		}

		/**
		 * @param string $libelle
		 */
		public function setLibelle($libelle)
		{
			$this->libelle = $libelle;
		}
		//</editor-fold>

		/**
		 * @return string
		 */
		public function __toString()
		{
			return $this->libelle;
		}
	}