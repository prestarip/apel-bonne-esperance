<?php
	/**
	 * Created by PhpStorm.
	 * User: Guillaume
	 * Date: 01/06/2018
	 * Time: 13:45
	 */

	namespace Apel\Dll\Business;

	class cls_Fonction
	{
		/** @var integer $idFonction */
		private $idFonction;
		/** @var string $codeFonction */
		private $codeFonction;
		/** @var string $libelle */
		private $libelle;

		/**
		 * cls_Fonction constructor.
		 *
		 * @param int    $idFonction
		 * @param string $codeFonction
		 * @param string $libelle
		 */
		public function __construct($idFonction, $codeFonction, $libelle)
		{
			$this->idFonction   = $idFonction;
			$this->codeFonction = $codeFonction;
			$this->libelle      = $libelle;
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdFonction()
		{
			return $this->idFonction;
		}

		/**
		 * @param int $idFonction
		 */
		public function setIdFonction($idFonction)
		{
			$this->idFonction = $idFonction;
		}

		/**
		 * @return string
		 */
		public function getCodeFonction()
		{
			return $this->codeFonction;
		}

		/**
		 * @param string $codeFonction
		 */
		public function setCodeFonction($codeFonction)
		{
			$this->codeFonction = $codeFonction;
		}

		/**
		 * @return string
		 */
		public function getLibelle()
		{
			return $this->libelle;
		}

		/**
		 * @param string $libelle
		 */
		public function setLibelle($libelle)
		{
			$this->libelle = $libelle;
		}
		//</editor-fold>

		/**
		 * @return string
		 */
		public function __toString()
		{
			return $this->libelle;
		}
	}
