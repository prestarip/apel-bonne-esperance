<?php
	/**
	 * Created by PhpStorm.
	 * User: Guillaume
	 * Date: 22/05/2018
	 * Time: 13:14
	 */

	namespace Apel\Dll\Business;

	/**
	 * Class cls_MessageInstant
	 * @package Apel\Dll\Business
	 */
	class cls_MessageInstant
	{
		/** @var integer $idMessageInstant */
		private $idMessageInstant;
		/** @var string $message */
		private $message;
		/** @var \DateTime $dateEnvoi */
		private $dateEnvoi;
		/** @var cls_Compte $compteA */
		private $compteA;
		/** @var cls_Compte $compteB */
		private $compteB;

		/**
		 * cls_MessageInstant constructor.
		 *
		 * @param int        $idMessageInstant
		 * @param string     $message
		 * @param \DateTime  $dateEnvoi
		 * @param cls_Compte $compteA
		 * @param cls_Compte $compteB
		 */
		public function __construct($idMessageInstant, $message, \DateTime $dateEnvoi, cls_Compte $compteA, cls_Compte $compteB)
		{
			$this->idMessageInstant = $idMessageInstant;
			$this->message          = $message;
			$this->dateEnvoi        = $dateEnvoi;
			$this->compteA          = $compteA;
			$this->compteB          = $compteB;
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdMessageInstant()
		{
			return $this->idMessageInstant;
		}

		/**
		 * @param int $idMessageInstant
		 */
		public function setIdMessageInstant($idMessageInstant)
		{
			$this->idMessageInstant = $idMessageInstant;
		}

		/**
		 * @return string
		 */
		public function getMessage()
		{
			return $this->message;
		}

		/**
		 * @param string $message
		 */
		public function setMessage($message)
		{
			$this->message = $message;
		}

		/**
		 * @return \DateTime
		 */
		public function getDateEnvoi()
		{
			return $this->dateEnvoi;
		}

		/**
		 * @param \DateTime $dateEnvoi
		 */
		public function setDateEnvoi($dateEnvoi)
		{
			$this->dateEnvoi = $dateEnvoi;
		}

		/**
		 * @return cls_Compte
		 */
		public function getCompteA()
		{
			return $this->compteA;
		}

		/**
		 * @param cls_Compte $compteA
		 */
		public function setCompteA($compteA)
		{
			$this->compteA = $compteA;
		}

		/**
		 * @return cls_Compte
		 */
		public function getCompteB()
		{
			return $this->compteB;
		}

		/**
		 * @param cls_Compte $compteB
		 */
		public function setCompteB($compteB)
		{
			$this->compteB = $compteB;
		}
		//</editor-fold>

		/**
		 * @return string
		 */
		public function __toString()
		{
			// TODO: Implement __toString() method.
			return "";
		}
	}