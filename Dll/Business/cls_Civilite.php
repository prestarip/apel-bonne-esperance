<?php
	/**
	 * Created by PhpStorm.
	 * User: GuillaumeFixe
	 * Date: 24/05/2018
	 * Time: 19:46
	 */

	namespace Apel\Dll\Business;

	/**
	 * Class cls_Civilite
	 * @package Apel\Dll\Business
	 */
	class cls_Civilite
	{
		/** @var integer $idCivilite */
		private $idCivilite;
		/** @var string $libelle */
		private $libelle;

		/**
		 * cls_Civilite constructor.
		 *
		 * @param int    $idCivilite
		 * @param string $libelle
		 */
		public function __construct($idCivilite, $libelle)
		{
			$this->idCivilite = $idCivilite;
			$this->libelle    = $libelle;
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdCivilite()
		{
			return $this->idCivilite;
		}

		/**
		 * @param int $idCivilite
		 */
		public function setIdCivilite($idCivilite)
		{
			$this->idCivilite = $idCivilite;
		}

		/**
		 * @return string
		 */
		public function getLibelle()
		{
			return $this->libelle;
		}

		/**
		 * @param string $libelle
		 */
		public function setLibelle($libelle)
		{
			$this->libelle = $libelle;
		}
		//</editor-fold>

		public static function getPropertiesClass()
		{
			return get_class_vars(self::class);
		}

		/**
		 * @return string
		 */
		public function __toString()
		{
			return $this->libelle;
		}
	}