<?php
	/**
	 * Created by PhpStorm.
	 * User: GuillaumeFixe
	 * Date: 24/05/2018
	 * Time: 21:30
	 */

	namespace Apel\Dll\Business;

	class cls_Token
	{
		/** @var integer $idToken */
		private $idToken;
		/** @var string $token */
		private $token;
		/** @var \DateTime $datGeneration */
		private $datGeneration;
		/** @var boolean $isUse */
		private $isUse;
		/** @var cls_Groupe $groupe */
		private $groupe;

		/**
		 * cls_Token constructor.
		 *
		 * @param int        $idToken
		 * @param string     $token
		 * @param \DateTime  $datGeneration
		 * @param bool       $isUse
		 * @param cls_Groupe $groupe
		 */
		public function __construct($idToken, $token, \DateTime $datGeneration, $isUse, $groupe)
		{
			$this->idToken       = $idToken;
			$this->token         = $token;
			$this->datGeneration = $datGeneration;
			$this->isUse         = $isUse;
			$this->groupe        = $groupe;
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdToken()
		{
			return $this->idToken;
		}

		/**
		 * @param int $idToken
		 */
		public function setIdToken($idToken)
		{
			$this->idToken = $idToken;
		}

		/**
		 * @return string
		 */
		public function getToken()
		{
			return $this->token;
		}

		/**
		 * @param string $token
		 */
		public function setToken($token)
		{
			$this->token = $token;
		}

		/**
		 * @return \DateTime
		 */
		public function getDatGeneration()
		{
			return $this->datGeneration;
		}

		/**
		 * @param \DateTime $datGeneration
		 */
		public function setDatGeneration($datGeneration)
		{
			$this->datGeneration = $datGeneration;
		}

		/**
		 * @return bool
		 */
		public function isUse()
		{
			return $this->isUse;
		}

		/**
		 * @param bool $isUse
		 */
		public function setIsUse($isUse)
		{
			$this->isUse = $isUse;
		}

		/**
		 * @return cls_Groupe
		 */
		public function getGroupe()
		{
			return $this->groupe;
		}

		/**
		 * @param cls_Groupe $groupe
		 */
		public function setCompte($groupe)
		{
			$this->groupe = $groupe;
		}

		//</editor-fold>

		public static function getPropertiesClass()
		{
			return get_class_vars(self::class);
		}

		/**
		 * @return string
		 */
		public function __toString()
		{
			// TODO: Implement __toString() method.
			return "";
		}
	}