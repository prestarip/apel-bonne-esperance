<?php
	/**
	 * Created by PhpStorm.
	 * User: Guillaume
	 * Date: 01/06/2018
	 * Time: 15:19
	 */

	namespace Apel\Dll\Business;

	class cls_Adresse
	{
		/** @var integer $idAdresse */
		private $idAdresse;
		/** @var string $adresse */
		private $adresse;
		/** @var string $codePostal */
		private $codePostal;
		/** @var string $ville */
		private $ville;

		/**
		 * clsAdresse constructor.
		 *
		 * @param int    $idAdresse
		 * @param string $adresse
		 * @param string $codePostal
		 * @param string $ville
		 */
		public function __construct($idAdresse, $adresse, $codePostal, $ville)
		{
			$this->idAdresse  = $idAdresse;
			$this->adresse    = $adresse;
			$this->codePostal = $codePostal;
			$this->ville      = $ville;
		}

		//<editor-fold desc="Accesseur">

		/**
		 * @return int
		 */
		public function getIdAdresse()
		{
			return $this->idAdresse;
		}

		/**
		 * @param int $idAdresse
		 */
		public function setIdAdresse($idAdresse)
		{
			$this->idAdresse = $idAdresse;
		}

		/**
		 * @return string
		 */
		public function getAdresse()
		{
			return $this->adresse;
		}

		/**
		 * @param string $adresse
		 */
		public function setAdresse($adresse)
		{
			$this->adresse = $adresse;
		}

		/**
		 * @return string
		 */
		public function getCodePostal()
		{
			return $this->codePostal;
		}

		/**
		 * @param string $codePostal
		 */
		public function setCodePostal($codePostal)
		{
			$this->codePostal = $codePostal;
		}

		/**
		 * @return string
		 */
		public function getVille()
		{
			return $this->ville;
		}

		/**
		 * @param string $ville
		 */
		public function setVille($ville)
		{
			$this->ville = $ville;
		}
		//</editor-fold>

		/**
		 * @return string
		 */
		public function __toString()
		{
			return $this->adresse . ' ' . $this->codePostal . ' - ' . $this->ville;
		}
	}