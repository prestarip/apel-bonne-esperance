<?php
	/**
	 * Created by PhpStorm.
	 * User: Guillaume
	 * Date: 01/06/2018
	 * Time: 13:16
	 */

	namespace Apel\Dll\Business;

	class cls_Role
	{
		/** @var integer $idRole */
		private $idRole;
		/** @var string $libelle */
		private $libelle;
		/** @var array $listeFonction */
		private $listeFonction;

		/**
		 * cls_Role constructor.
		 *
		 * @param int    $idRole
		 * @param string $libelle
		 * @param array  $listeFonction
		 */
		public function __construct($idRole, $libelle, array $listeFonction)
		{
			$this->idRole        = $idRole;
			$this->libelle       = $libelle;
			$this->listeFonction = array();
		}

		//<editor-fold desc="Accesseurs">

		/**
		 * @return int
		 */
		public function getIdRole()
		{
			return $this->idRole;
		}

		/**
		 * @param int $idRole
		 */
		public function setIdRole($idRole)
		{
			$this->idRole = $idRole;
		}

		/**
		 * @return string
		 */
		public function getLibelle()
		{
			return $this->libelle;
		}

		/**
		 * @param string $libelle
		 */
		public function setLibelle($libelle)
		{
			$this->libelle = $libelle;
		}

		/**
		 * @return array
		 */
		public function getListeFonction()
		{
			return $this->listeFonction;
		}

		/**
		 * @param array $listeFonction
		 */
		public function setListeFonction($listeFonction)
		{
			$this->listeFonction = $listeFonction;
		}
		//</editor-fold>

		/**
		 * @return string
		 */
		public function __toString()
		{
			return $this->libelle;
		}
	}