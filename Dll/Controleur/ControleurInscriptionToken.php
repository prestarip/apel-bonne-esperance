<?php
	/**
	 * Created by PhpStorm.
	 * User: mathieu
	 * Date: 26/07/2018
	 * Time: 21:00
	 */

	namespace Apel\Dll\Controleur;

	use Apel\Dll\Business\cls_Groupe;
	use Apel\Dll\Business\cls_Token;
	use Apel\Dll\Framework\Config;
	use Apel\Dll\Framework\Dao;

//	new ControleurInscriptionToken();

	class ControleurInscriptionToken
	{
		//<editor-fold desc="Requetes">
		const GET_TOKEN_BY_TOKEN = 'SELECT * FROM tb_token WHERE token=?';
		const GET_GROUPE_BY_ID   = 'SELECT t.* FROM public.tb_groupe t WHERE idgroupe=?';
		//</editor-fold>

		//<editor-fold desc="Variables privées">
		/** @var \Apel\Dll\Framework\Dao $dao */
		private $dao;
		private $connexion;
		private $cle;
		private $captcha;
		public  $pageRedirection;
		CONST PRIVATE_CAPTCHA = '6Lc-oWYUAAAAAPjmhJ3TKN3Hj0EOXvK1yFk2vV1y';
		//</editor-fold>

		//<editor-fold desc="Constructeur">
		public function __construct()
		{
			//Connexion à la BDD
			$this->dao = new Dao();

			//Récupération de la connexion
			$this->connexion = $this->dao->getConnexion();

			$this->cle = $_POST['cle'];

			//Vérification du captcha
			$this->verifCaptcha();

			//Formatage des valeurs du formulaire (suppression espace ...)
			$this->formatInput();

			//Vérification des données de saisies
			$this->verifToken();
		}
		//</editor-fold>

		//<editor-fold desc="Formatage des données">
		//Formatage des valeurs du formulaire (suppression espace ...)
		private function formatInput()
		{
			//Suppression des espaces avant et après
			$this->cle = trim($this->cle);
		}
		//</editor-fold>

		//<editor-fold desc="Fonctions de traitements">
		private function verifToken()
		{
			//Execution de la requete
			$res = self::getTokenByToken();

			$tokenVal = $res['token'];
			$idtoken  = $res['idtoken'];
			$idGroupe = $res['fk_idgroupe'];
			$dateGen  = $res['dategeneration'];

			//Si la réponse de la requete n'est pas vide
			if( !empty($tokenVal))
			{
				//Récupération du groupe
				$groupe = self::getGroupeByID($idGroupe);

				//Création de l'objet token
				$token = new cls_Token($idtoken, $tokenVal, new \DateTime($dateGen), true, $groupe);

				//Stockage du token en variable de session
				$_SESSION['token'] = serialize($token);

				$this->pageRedirection = 'INSCRIPTION_PHP';
			}
			else
			{
				//Message d'échec de l'authentification
				$_SESSION['message'] = 'La clé d\'activation n\'existe pas';

				$this->pageRedirection = 'INSCRIPTION_TOKEN_PHP';
			}
		}
		//</editor-fold>

		//<editor-fold desc="Fonctions privées">
		//Récupération du Captcha
		private function getTokenByToken()
		{
			$res = $this->connexion->prepare(self::GET_TOKEN_BY_TOKEN);
			$res->bindParam(1, $this->cle);
			$res->execute();

			$res = $res->fetch();

			return $res;
		}

		//Récupération du groupe
		private function getGroupeByID($idGroupe)
		{
			$res = $this->connexion->prepare(self::GET_GROUPE_BY_ID);
			$res->bindParam(1, $idGroupe);
			$res->execute();

			$res = $res->fetch();

			$groupe = new cls_Groupe($res['idgroupe'], $res['codegroupe'], $res['libelle']);

			return $groupe;
		}
		//</editor-fold>

		//<editor-fold desc="Fonctions Vérif Captcha">
		private function verifCaptcha()
		{
			$this->captcha = $_POST['g-recaptcha-response'];
			$ip            = $_SERVER['REMOTE_ADDR'];
			$response      = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . self::PRIVATE_CAPTCHA . "&response=" . $this->captcha . "&remoteip=" . $ip);
			$responseKeys  = json_decode($response, true);

			if(intval($responseKeys["success"]) !== 1)
			{
				//Message d'échec de l'authentification
				$_SESSION['message'] = 'Vous êtes un robot !!!';

				$this->pageRedirection = 'INSCRIPTION_TOKEN_PHP';
			}

		}
		//</editor-fold>
	}