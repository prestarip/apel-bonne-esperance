<?php
	/**
	 * Created by PhpStorm.
	 * User: mathieu
	 * Date: 25/06/2018
	 * Time: 14:27
	 */

	namespace Apel\Dll\Controleur;

	use Apel\Dll\Framework\Dao;

//	new ControleurInscription();

	class ControleurInscription
	{
		//<editor-fold desc="Requetes">
		const GET_ALL_CIVILITE = 'SELECT idCivilite, libelle FROM tb_Civilite';
		const INSERT_COMPTE    = '	INSERT INTO tb_compte (idcompte, nom, prenom, fk_idcivilite, mail, mdp, datenaissance, dateinscription, fk_idgroupe, fk_idtoken) 
									VALUES (DEFAULT,?,?,?,?,?,?,?,?,?)';
		//</editor-fold>

		//<editor-fold desc="Variables privées">
		/** @var \Apel\Dll\Framework\Dao $dao */
		private $dao;
		private $connexion;
		private $email;
		private $emailConf;
		private $mdp;
		private $mdpConf;
		private $idCivite;
		private $nom;
		private $prenom;
		private $dateNaissance;
		private $dateInscription;
		private $listeCivilite;
		/** @var \Apel\Dll\Business\cls_Token $token */
		private $token;
		public  $pageRedirection;
		//</editor-fold>

		//<editor-fold desc="Constructeur">
		public function __construct()
		{
			//Connexion à la BDD
			$this->dao = new Dao();

			//Récupération de la connexion
			$this->connexion = $this->dao->getConnexion();

			if(isset($_POST['formResponse']['valider']) == false)
			{
				$this->recupCivilites();
			}
			else
			{
				//Récupération des données du formulaire
				$this->email           = $_POST['formResponse']['email'];
				$this->emailConf       = $_POST['formResponse']['emailconf'];
				$this->mdp             = $_POST['formResponse']['pass'];
				$this->mdpConf         = $_POST['formResponse']['passconf'];
				$this->idCivite        = $_POST['formResponse']['selectCiv'];
				$this->nom             = $_POST['formResponse']['nom'];
				$this->prenom          = $_POST['formResponse']['prenom'];
				$this->dateNaissance   = $_POST['formResponse']['datenai'];
				$this->dateInscription = date('Y-m-d');
				$this->token           = unserialize($_SESSION['token']);

				//Formatage des valeurs du formulaire (suppression espace ...)
				$this->formatInput();

				//Vérification des données de saisies
				$this->verifDonnees();
			}

		}
		//</editor-fold>

		//<editor-fold desc="Formatage des données">
		//Formatage des valeurs du formulaire (suppression espace ...)
		private function formatInput()
		{
			//Suppression des espaces avant et après
			$valeursFormulaireInscription  = [];
			$valeursFormulaireInscription['email']           = $this->email = trim($this->email);
			$valeursFormulaireInscription['emailConf']       = $this->emailConf = trim($this->emailConf);
			$valeursFormulaireInscription['mdp']             = $this->mdp = trim($this->mdp);
			$valeursFormulaireInscription['mdpConf']         = $this->mdpConf = trim($this->mdpConf);
			$valeursFormulaireInscription['idCiv']           = $this->idCivite = trim($this->idCivite);
			$valeursFormulaireInscription['nom']             = $this->nom = trim($this->nom);
			$valeursFormulaireInscription['prenom']          = $this->prenom = trim($this->prenom);
			$valeursFormulaireInscription['dateNaissance']   = $this->dateNaissance = trim($this->dateNaissance);
			$valeursFormulaireInscription['dateInscription'] = $this->dateInscription = trim($this->dateInscription);
			$_SESSION['valeursFormulaireInscription'] = $valeursFormulaireInscription;
		}
		//</editor-fold>

		//<editor-fold desc="Fonctions de traitements">
		private function verifDonnees()
		{
			//Vérification des emails
			if($this->email != $this->emailConf)
			{
				$_SESSION['message'] = 'Les adresses emails ne sont pas identiques';
				$this->pageRedirection = 'INSCRIPTION_PHP';
				exit();
			}
			else
			{
				//Vérification des mdp
				if($this->mdp != $this->mdpConf)
				{
					$_SESSION['message'] = 'Les mots de passes ne sont pas identiques';
					$this->pageRedirection = 'INSCRIPTION_PHP';
					exit();
				}
				else
				{
					//TODO insert + session Login + redirection

					self::insertCompte();

					//Stockage du login en variable de session
					//$_SESSION['compte'] = serialize($compte);

					//Vidage de la variable de session $_SESSION['loginOK']
					unset($_SESSION['message']);
				}
			}
		}
		//</editor-fold>

		//<editor-fold desc="Fonctions privées">
		private function recupCivilites()
		{
			//Execution de la requete
			$res = $this->connexion->query(self::GET_ALL_CIVILITE);
			$res->setFetchMode(\PDO::FETCH_ASSOC);
			$this->listeCivilite = $res->fetchAll();

			$_SESSION['listeciv'] = $this->listeCivilite;
		}

		private function insertCompte()
		{
			$res = $this->connexion->prepare(self::INSERT_COMPTE);
			$res->bindParam(1, $this->nom);
			$res->bindParam(2, $this->prenom);
			$res->bindParam(3, $this->idCivite);
			$res->bindParam(4, $this->email);
			$res->bindParam(5, $this->mdp);
			$res->bindParam(6, $this->dateNaissance);
			$res->bindParam(7, $this->dateInscription);
			$res->bindParam(8, $this->token->getGroupe()->getIdGroupe());
			$res->bindParam(9, $this->token->getIdToken());
			$res->execute();
		}
		//</editor-fold>
	}