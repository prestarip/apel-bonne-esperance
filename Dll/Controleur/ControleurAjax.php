<?php
	/**
	 * Created by PhpStorm.
	 * User: Guillaume
	 * Date: 28/06/2018
	 * Time: 14:46
	 */

	namespace Apel\Dll\Controleur;

	use Apel\Dll\Framework\cls_ConstruitTemplate;
	use Apel\Dll\Framework\Config;
	use Apel\Dll\Framework\Dao;

	return new class()
	{
		/** @var cls_ConstruitTemplate $aside */
		private $result;

		public function __construct()
		{
			$this->result = ['objets',
							 'erreur'
			];

			$this->action();

			// uniformisation des résultats
			$this->result[0] = $this->result['objets'];
			$this->result[1] = $this->result['erreur'];

			if(false)
			{
				ob_clean();
				echo '<pre>';
				var_dump($this->result);
				//print_r();
				echo '</pre>';
				die();
			}

			echo json_encode($this->result);
		}

		private function action()
		{
			if(isset($_POST) && empty($_POST) == false)
			{
				if(isset($_POST['ajaxTraitement']) && empty($_POST['ajaxTraitement']) == false)
				{
					switch($_POST['ajaxTraitement'])
					{
						case 'getRoute':
							$this->getRoute();
							break;
						case 'getBulle':
							$this->getBulle();
							break;
						case 'getChat':
							$this->getChat();
							break;
						case 'getNewChat':
							$this->getNewChat();
							break;
						case 'sendMessageMessagerieInstantane':
							$this->sendMessageMessagerieInstantane();
							break;
						case 'confirmToken':
							$this->confirmToken();
							break;
						case 'confirmInscription':
							$this->confirmInscription();
							break;
						default:
							$this->result['erreur'] = 'Ce traitement n\'existe pas';
							break;
					}
				}
				else
				{
					$this->result['erreur'] = 'Aucun traitement n\'a été demandé';
				}
			}
		}

		function getRoute()
		{
			$route                  = $_POST['route'] ?? null;
			$adresse                = Config::getAdresse($route) ?? null;
			$this->result['erreur'] = empty($adresse) == true ? 'addresse vide' : 0;
			$this->result['objets'] = ["adresse" => $adresse];
		}

		function getBulle()
		{
			$lien                            = $_POST['lien'] ?? null;
			$_SESSION['noLayout']            = true;
			$this->result['objets']          = ['bulle' => ""];
			$this->result['objets']['bulle'] .= require_once($_SERVER['DOCUMENT_ROOT'] . $lien);
//		$this->result['objets']['bulle'] .= Config::getRequireOnce($lien);
			$_SESSION['noLayout'] = false;
			unset($_SESSION['noLayout']);
			$this->result['erreur'] = empty($lien) == true ? 1 : 0;
		}

		function getChat()
		{
			$dao  = new Dao();
			$data = $_POST['data'] ?? null;
			if($data <> null)
			{

				$this->result['objets'] = $dao->getAllMessageChat($data['id']);
				$this->result['erreur'] = (empty($this->result['objets'])) ? 'Aucun messages entre ces personnes' : 0;
			}
			else
			{
				$this->result['objets'] = '';
				$this->result['erreur'] = 'Paramètre de requête pour le chat non reçu';
			}
		}

		function getNewChat()
		{
			$dao           = new Dao();
			$idLastMessage = $_POST['idLastMessage'] ?? null;
			$idContact     = $_POST['idContact'] ?? null;
			if($idContact <> null && $idLastMessage <> null)
			{

				$this->result['objets'] = $dao->getAllMessageChat($idContact, $idLastMessage);
				$this->result['erreur'] = (empty($this->result['objets'])) ? 'Aucun nouveau message entre ces personnes' : 0;
			}
			else
			{
				$this->result['objets'] = '';
				$this->result['erreur'] = 'Paramètre de requête pour le chat non reçu';
			}
		}

		function sendMessageMessagerieInstantane()
		{
			$dao     = new Dao();
			$data    = $_POST['data'] ?? null;
			$message = $_POST['message'] ?? null;
			if($data <> null AND $message <> null)
			{
				$this->result['objets']['infoLastMessage']['id']        = $dao->addChatMessage($data['id'], $message);
				$this->result['objets']['infoLastMessage']['dateenvoi'] = date("Y-m-d H:i:s");
				$this->result['erreur']                                 = 0;
//			$this->result['erreur'] = ($this->result['objets'] <> 1) ? $this->result['objets'] : 0;
//			$this->result['objets'] = ($this->result['objets'] <> 1) ? '' : $this->result['objets'];
			}
			else
			{
				$this->result['objets'] = '';
				$this->result['erreur'] = 'Paramètre de requête pour l\'envoi de message chat non reçu';
			}
		}

		function confirmToken()
		{
			$redirection = new ControleurInscriptionToken();

			$this->result['objets']['redirection'] = $redirection->pageRedirection;
			$this->result['erreur']                = 0;
		}

		function confirmInscription()
		{
			$redirection = new ControleurInscription();

			$this->result['objets']['redirection'] = $redirection->pageRedirection;
			$this->result['erreur']                = 0;
		}
	};


