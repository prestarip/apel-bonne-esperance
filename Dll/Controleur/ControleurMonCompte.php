<?php
	/**
	 * Created by PhpStorm.
	 * User: mathieu
	 * Date: 31/07/2018
	 * Time: 08:53
	 */

	namespace Apel\Dll\Controleur;

	use Apel\Dll\Framework\Dao;

	new ControleurMonCompte();

	class ControleurMonCompte
	{
		/** @var \Apel\Dll\Framework\Dao $dao */
		private $dao;
		private $email;
		private $emailConf;
		private $mdp;
		private $mdpConf;
		private $idCivite;
		private $nom;
		private $prenom;
		private $dateNaissance;
		private $listeCivilite;

		public function __construct()
		{
			//Connexion à la BDD
			$this->dao = new Dao();

			if( isset($_POST['validerCompte']) == false)
			{
				$this->recupCivilites();
			}
			else
			{
				//Récupération des données du formulaire
				$this->email           = $_POST['email'];
				$this->emailConf       = $_POST['emailconf'];
				$this->mdp             = $_POST['pass'];
				$this->mdpConf         = $_POST['passconf'];
				$this->idCivite        = $_POST['selectCiv'];
				$this->nom             = $_POST['nom'];
				$this->prenom          = $_POST['prenom'];
				$this->dateNaissance   = $_POST['datenai'];

				//Formatage des valeurs du formulaire (suppression espace ...)
				//$this->formatInput();

				//Vérification des données de saisies
				//$this->verifDonnees();
			}

		}

		public function recupCivilites()
		{
			//Execution de la requete
			$_SESSION['listeciv'] = $this->listeCivilite = $this->dao->getAllCivilite();
		}
	}