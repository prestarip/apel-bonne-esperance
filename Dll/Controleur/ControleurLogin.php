<?php
	/**
	 * Created by PhpStorm.
	 * User: mathieu
	 * Date: 13/06/2018
	 * Time: 21:10
	 */

	/*
 * @todo Guillaume -> redirection ligne 99 à faire en pop up
 */

	namespace Apel\Dll\Controleur;

	use Apel\Dll\Business\cls_Civilite;
	use Apel\Dll\Business\cls_Compte;
	use Apel\Dll\Business\cls_Groupe;
	use Apel\Dll\Framework\Dao;
	use Apel\Dll\Framework\Fonctions;

	new ControleurLogin();

	class ControleurLogin
	{
		//<editor-fold desc="Requetes">
		const GET_LOGIN_BY_MAIL    = 'SELECT idcompte,mdp FROM tb_Compte WHERE mail=?';
		const GET_ALL_COMPTE_BY_ID = 'SELECT t.* FROM public.tb_compte t WHERE idcompte=?';
		const GET_CIVILITE_BY_ID   = 'SELECT t.* FROM public.tb_civilite t WHERE idcivilite=?';
		const GET_GROUPE_BY_ID     = 'SELECT t.* FROM public.tb_groupe t WHERE idgroupe=?';
		//</editor-fold>

		//<editor-fold desc="Variables privées">
		/** @var \Apel\Dll\Framework\Dao $dao */
		private $dao;
		private $connexion;
		private $login;
		private $pass;
		//</editor-fold>

		//<editor-fold desc="Constructeur">
		public function __construct()
		{
			$action = $_GET['action'] ?? null;
			if($action <> null && $action == 'deconnexion')
			{
				Fonctions::deconnexion();
				header('Location:/index.php');
				exit();
			}
			else
			{
				//Récupération du login (mail) et mdp du formulaire
				$this->login = $_POST['login'];
				$this->pass  = $_POST['pass'];

				//Connexion à la BDD
				$this->dao = new Dao();

				//Récupération de la connexion
				$this->connexion = $this->dao->getConnexion();

				//Formatage des valeurs du formulaire (suppression espace ...)
				$this->formatInput();

				//Récupération et vérification du mdp
				$this->verifieMdp();
			}
		}
		//</editor-fold>

		//<editor-fold desc="Formatage des données">
		//Formatage des valeurs du formulaire (suppression espace ...)
		private function formatInput()
		{
			//Suppression des espaces avant et après
			$this->login = trim($this->login);
			$this->pass  = trim($this->pass);
		}
		//</editor-fold>

		//<editor-fold desc="Fonctions de traitements">
		//Vérification du mdp
		//Redirection (TEMPORAIRE) vers la page login.php
		private function verifieMdp()
		{
			//Execution de la requete
			$res = self::getMdpByMail();

			$idCompte = $res['idcompte'];
			$passBdd  = $res['mdp'];

			//Si la réponse de la requete n'est pas vide
			if( !empty($passBdd))
			{
				//Si le mdp de la base = mdp du formulaire
				if($passBdd == $this->pass)
				{
					//Récupération du compte
					$compte = self::getCompteByID($idCompte);

					//Stockage du login en variable de session
					$_SESSION['compte'] = serialize($compte);

					//Message de réussite de l'authentification (peut être optionnel, à voir plus tard)
					$_SESSION['message'] = 'Identification réussi';

					//Booleen si log OK ou NOK
					$_SESSION['loginOK'] = true;
				}
				//Si le mdp de la base <> mdp du formulaire
				else
				{
					//Vidage du login sotcker en variable de session
					unset($_SESSION['compte']);

					//Message d'échec de l'authentification
					$_SESSION['message'] = 'Identification incorrect (NE PAS AFFICHER : mdp incorrect)';

					//Booleen si log OK ou NOK
					$_SESSION['loginOK'] = false;
				}
			}
			else
			{
				//Vidage du login sotcker en variable de session
				unset($_SESSION['compte']);

				//Message d'échec de l'authentification
				$_SESSION['message'] = 'Identification incorrect (NE PAS AFFICHER : mauvais mail)';

				//Booleen si log OK ou NOK
				$_SESSION['loginOK'] = false;
			}

			//On verra plus tard vers où on redirige selon $_SESSION['loginOK'] OK ou NOK
			header('Location:/index.php');
			exit();

		}
		//</editor-fold>

		//<editor-fold desc="Fonctions privées">
		//Récupération du mot de passe et ID
		private function getMdpByMail()
		{
			$res = $this->connexion->prepare(self::GET_LOGIN_BY_MAIL);
			$res->bindParam(1, $this->login);
			$res->execute();

			$res = $res->fetch();

			return $res;
		}

		//Récupération du compte
		private function getCompteByID($idCompte)
		{
			$res = $this->connexion->prepare(self::GET_ALL_COMPTE_BY_ID);
			$res->bindParam(1, $idCompte);
			$res->execute();

			$res = $res->fetch();

			$civilite = self::getCiviliteByID($res['fk_idcivilite']);
			$groupe   = self::getGroupeByID($res['fk_idgroupe']);

			$compte = new cls_Compte($idCompte, $res['nom'], $res['prenom'], $res['mail'], $res['mdp'], new \DateTime($res['datenaissance']), new \DateTime($res['dateinscription']), $civilite, $groupe);

			return $compte;
		}

		//Récupération de la civilité
		private function getCiviliteByID($idCivite)
		{
			$res = $this->connexion->prepare(self::GET_CIVILITE_BY_ID);
			$res->bindParam(1, $idCivite);
			$res->execute();

			$res = $res->fetch();

			$civilite = new cls_Civilite($res['idcivilite'], $res['libelle']);

			return $civilite;
		}

		//Récupération du groupe
		private function getGroupeByID($idGroupe)
		{
			$res = $this->connexion->prepare(self::GET_GROUPE_BY_ID);
			$res->bindParam(1, $idGroupe);
			$res->execute();

			$res = $res->fetch();

			$groupe = new cls_Groupe($res['idgroupe'], $res['codegroupe'], $res['libelle']);

			return $groupe;
		}
		//</editor-fold>
	}
