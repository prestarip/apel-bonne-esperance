<?php
	/**
	 * Created by PhpStorm.
	 * User: mathieu
	 * Date: 13/06/2018
	 * Time: 21:10
	 */

	namespace Apel\Dll\Controleur;

	use Apel\Dll\Framework\Dao;

	new ControleurPaiement();

	class ControleurPaiement
	{
		private $dao;

		public function __construct()
		{
			//Connexion à la BDD
			$this->dao = new Dao();
		}

		public function insererPaiement($item_name,$payment_amount,$date,$idUser)
		{
			$this->dao->insertPayment($item_name,$payment_amount,$date,$idUser);
		}
	}
