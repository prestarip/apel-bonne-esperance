<?php
	/**
	 * Created by PhpStorm.
	 * User: mathieu
	 * Date: 13/06/2018
	 * Time: 21:10
	 */

	namespace Apel\Dll\Controleur;

	use Apel\Dll\Framework\Dao;

	new ControleurNews();

	class ControleurNews
	{
		private $dao;

		public function __construct()
		{
			//Connexion à la BDD
			$this->dao = new Dao();
		}

		public function recupererActualite($idActu, $limit)
		{
			$listeActualite = $this->dao->getNews($idActu, $limit);
			return $listeActualite;
		}

		public function isExist($idActu)
		{
			$bool    = false;
			$listeId = $this->dao->getNewsId();
			foreach($listeId as $item)
			{
				if($idActu == $item['idactualite'])
				{
					$bool = true;
				}
			}
			return $bool;
		}
	}
