<?php
	/**
	 * Created by PhpStorm.
	 * User: pythonirc
	 * Date: 11/07/18
	 * Time: 20:55
	 */

	namespace Apel\Dll\Page;

	use Apel\Dll\Framework\cls_ConstruitTemplate;

	class cls_news extends cls_ConstruitTemplate
	{
		const    TITRE      = '#title#';
		const    LISTE_NEWS = '#liste_news#';

		function titre($contenu)
		{
			parent::remplacePage(self::TITRE, $contenu);
		}

		function listeNews($contenu)
		{
			parent::remplacePage(self::LISTE_NEWS, $contenu);
		}

		function formaterListeActus($contenu)
		{
			$chaine = '';
			foreach($contenu as $item)
			{
				$chaine .= '<article>';

				$chaine .= '<h2> <a href="/news.php?id=' . $item['idactualite'] . '">' . $item['titre'] . '</a></h2>';
				$chaine .= '<div><a href="/news.php?id=' . $item['idactualite'] . '">' . $item['prenom'] . ' ' . $item['nom'] . ' ' . $item['dateactualite'] . '</a></div>';

				$chaine .= '</article>';
			}
			return $chaine;
		}

		function formaterActu($contenu)
		{
			$chaine = '';

			foreach($contenu as $item)
			{
				$chaine .= '<article>';

				$chaine .= '<h2>' . $item['titre'] . '</h2>';
				$chaine .= '<div>' . $item['prenom'] . ' ' . $item['nom'] . ' ' . $item['dateactualite'] . '</div>';
				$chaine .= '<div>' . $item['contenu'] . '</div>';

				$chaine .= '</article>';
			}

			return $chaine;
		}
	}