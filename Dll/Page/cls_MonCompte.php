<?php
	/**
	 * Created by PhpStorm.
	 * User: mathieu
	 * Date: 31/07/2018
	 * Time: 08:53
	 */

	namespace Apel\Dll\Page;

	use Apel\Dll\Controleur\ControleurMonCompte;
	use Apel\Dll\Framework\cls_ConstruitTemplate;

	class cls_MonCompte extends cls_ConstruitTemplate
	{
		//<editor-fold desc="Constante contenant les balises du template">
		const IMAGE         = '#image#';
		const TITRE         = '#title#';
		const BEGIN         = '#begin#';
		const MAIL          = '#email#';
		const MAILCONF      = '#emailConf#';
		const PASSWORD      = '#password#';
		const PASSWORDCONF  = '#passwordConf#';
		const CIVILITE      = '#civilite#';
		const NOM           = '#nom#';
		const PRENOM        = '#prenom#';
		const DATE          = '#date#';
		const SUBMIT        = '#submit#';
		const END           = '#end#';
		const BEGIN_AVATAR  = '#beginAvatar#';
		const CHEMIN_AVATAR = '#chemin#';
		const SUBMIT_AVATAR = '#submitAvatar#';
		const END_AVATAR    = '#endAvatar#';
		const TRANSACTION   = '#transactions#';
		const MESSAGE       = '#message#';
		//</editor-fold>

		//<editor-fold desc="Constante TABLEAU">
		//Tableau pour l'image
		const TAB_IMAGE = [
			parent::SOURCE => '\Images\avatar.png',
			parent::ALT    => 'Avatar',
			parent::CLASSE => '',
		];
		//Tableau pour le début du formulaire
		const TAB_BEGIN = [
			parent::ACTION  => '\Dll\Controleur\ControleurMonCompte.php',
			parent::METHODE => 'POST',
			parent::NOM     => 'formCompte',
		];
		//Tableau pour le label du mail
		const TAB_MAIL_LABEL = [
			parent::ID     => 'lblmail',
			parent::VALEUR => 'Adresse Email : ',
		];
		//Tableau pour le label du mailconf
		const TAB_MAILCONF_LABEL = [
			parent::ID     => 'lblmailconf',
			parent::VALEUR => 'Confirmation Email : ',
		];
		//Tableau pour le label du password
		const TAB_PASSWORD_LABEL = [
			parent::ID     => 'lblPassword',
			parent::VALEUR => 'Votre mot de passe : ',
		];
		//Tableau pour le label du password
		const TAB_PASSWORDCONF_LABEL = [
			parent::ID     => 'lblPasswordconf',
			parent::VALEUR => 'Confirmer mot de passe : ',
		];
		//Tableau pour la label de a cilivite
		const TAB_CIV_LABEL = [
			parent::ID     => 'lblcivilite',
			parent::VALEUR => 'Civilité : ',
		];
		//Tableau pour le label du nom
		const TAB_NOM_LABEL = [
			parent::ID     => 'lblNom',
			parent::VALEUR => 'Votre nom : ',
		];
		//Tableau pour le label du prénom
		const TAB_PRENOM_LABEL = [
			parent::ID     => 'lblPrenom',
			parent::VALEUR => 'Votre prénom : ',
		];
		//Tableau pour le label de la date
		const TAB_DATE_LABEL = [
			parent::ID     => 'lbldatenai',
			parent::VALEUR => 'Votre date de naissance : ',
		];
		//Tableau pour le bouton submit
		const TAB_SUBMIT = [
			parent::TYPE   => 'submit',
			parent::NOM    => 'validerCompte',
			parent::ID     => 'validerCompte',
			parent::CLASSE => 'class',
			parent::VALEUR => 'Confirmer les changements',
		];
		//Tableau pour le début du formulaire
		const TAB_BEGIN_AVATAR = [
			parent::ACTION  => '\Dll\Controleur\ControleurMonCompte.php',
			parent::METHODE => 'POST',
			parent::NOM     => 'formAvatar',
		];
		//Tableau pour le label du chemin
		const TAB_CHEMIN_LABEL = [
			parent::ID     => 'lblmail',
			parent::VALEUR => 'Chemin : ',
		];
		//Tableau du chemin
		const TAB_CHEMIN = [
			parent::TYPE    => 'text',
			parent::NOM     => 'chemin',
			parent::ID      => 'chemin',
			parent::CLASSE  => 'class',
			parent::REQUIS  => '',
			parent::LIBELLE => self::TAB_CHEMIN_LABEL,
		];
		//Tableau pour le bouton submit
		const TAB_SUBMIT_AVATAR = [
			parent::TYPE   => 'submit',
			parent::NOM    => 'validerAvatar',
			parent::ID     => 'validerAvatar',
			parent::CLASSE => 'class',
			parent::VALEUR => 'Confirmer l\'avatar',
		];
		//Tableau pour le lien transaction
		const TAB_TRANSACTION = [
			self::HREF   => 'transaction.php',
			self::VALEUR => 'Mes transactions',
		];
		//</editor-fold>

		//<editor-fold desc="variables privés">
		private $tabMail;
		private $tabMailConf;
		private $tabPass;
		private $tabPassConf;
		private $tabNom;
		private $tabPrenom;
		private $tabDate;
		private $tabCivSQL;
		private $valeurMail;
		private $valeurMailConf;
		private $valeurMdp;
		private $valeurMdpConf;
		private $valeurIdCiv;
		private $valeurNom;
		private $valeurPrenom;
		private $valeurDateNaissance;
		private $controleur;
		/** @var \Apel\Dll\Business\cls_Compte $compte */
		private $compte;

		//</editor-fold>

		public function __construct(string $pageHtml, bool $layout)
		{
			parent::__construct($pageHtml, $layout);

			$this->controleur = new ControleurMonCompte();

			$this->compte = unserialize($_SESSION['compte']);

			$this->setValeurs();
			$this->createTab();
		}

		private function setValeurs()
		{
			$this->valeurMail          = $this->compte->getMail() ?? "";
			$this->valeurMailConf      = $this->compte->getMail() ?? "";
			$this->valeurMdp           = $this->compte->getMotDePasse() ?? "";
			$this->valeurMdpConf       = $this->compte->getMotDePasse() ?? "";
			$this->valeurIdCiv         = $this->compte->getCivilite()->getIdCivilite() ?? "";
			$this->valeurNom           = $this->compte->getNom() ?? "";
			$this->valeurPrenom        = $this->compte->getPrenom() ?? "";
			$this->valeurDateNaissance = $this->compte->getDateNaissanceString('Y-d-m') ?? "";
		}

		private function createTab()
		{
			$this->createTabMail();
			$this->createTabMailConf();
			$this->createTabPass();
			$this->createTabPassConf();
			$this->createTabNom();
			$this->createTabPrenom();
			$this->createTabDate();
			$this->tabCivSQL = $_SESSION['listeciv'];
		}

		//<editor-fold desc="Fonction création des tableaux">
		private function createTabMail()
		{
			$this->tabMail = [
				parent::TYPE      => 'email',
				parent::NOM       => 'email',
				parent::ID        => 'email',
				parent::VALEUR    => $this->valeurMail,
				parent::CLASSE    => 'class',
				parent::REQUIS    => '',
				parent::AUTOFOCUS => '',
				parent::LIBELLE   => self::TAB_MAIL_LABEL,
			];
		}

		private function createTabMailConf()
		{
			$this->tabMailConf = [
				parent::TYPE    => 'email',
				parent::NOM     => 'emailconf',
				parent::ID      => 'emailconf',
				parent::VALEUR  => $this->valeurMailConf,
				parent::CLASSE  => 'class',
				parent::REQUIS  => '',
				parent::LIBELLE => self::TAB_MAILCONF_LABEL,
			];
		}

		private function createTabPass()
		{
			$this->tabPass = [
				parent::TYPE    => 'password',
				parent::NOM     => 'pass',
				parent::ID      => 'pass',
				parent::VALEUR  => $this->valeurMdp,
				parent::CLASSE  => 'class',
				parent::REQUIS  => '',
				parent::LIBELLE => self::TAB_PASSWORD_LABEL,
			];
		}

		private function createTabPassConf()
		{
			$this->tabPassConf = [
				parent::TYPE    => 'password',
				parent::NOM     => 'passconf',
				parent::ID      => 'passconf',
				parent::VALEUR  => $this->valeurMdpConf,
				parent::CLASSE  => 'class',
				parent::REQUIS  => '',
				parent::LIBELLE => self::TAB_PASSWORDCONF_LABEL,
			];
		}

		private function createTabNom()
		{
			$this->tabNom = [
				parent::TYPE    => 'texte',
				parent::NOM     => 'nom',
				parent::ID      => 'nom',
				parent::VALEUR  => $this->valeurNom,
				parent::CLASSE  => 'class',
				parent::REQUIS  => '',
				parent::LIBELLE => self::TAB_NOM_LABEL,
			];
		}

		private function createTabPrenom()
		{
			$this->tabPrenom = [
				parent::TYPE    => 'texte',
				parent::NOM     => 'prenom',
				parent::ID      => 'prenom',
				parent::VALEUR  => $this->valeurPrenom,
				parent::CLASSE  => 'class',
				parent::REQUIS  => '',
				parent::LIBELLE => self::TAB_PRENOM_LABEL,
			];
		}

		private function createTabDate()
		{
			$this->tabDate = [
				parent::TYPE    => 'date',
				parent::NOM     => 'datenai',
				parent::ID      => 'datenai',
				parent::VALEUR  => $this->valeurDateNaissance,
				parent::CLASSE  => 'class',
				parent::REQUIS  => '',
				parent::LIBELLE => self::TAB_DATE_LABEL,
			];
		}
		//</editor-fold>

		//<editor-fold desc="Fonction générant le code HTML">
		function image()
		{
			parent::imgBalise(self::TAB_IMAGE, self::IMAGE);
		}

		function titre()
		{
			parent::remplacePage(self::TITRE, 'Votre compte');
		}

		function formBegin()
		{
			parent::formBeginBalise(self::BEGIN, self::TAB_BEGIN);
		}

		function mail()
		{
			parent::inputBalise(self::MAIL, $this->tabMail);
		}

		function mailConf()
		{
			parent::inputBalise(self::MAILCONF, $this->tabMailConf);
		}

		function password()
		{
			parent::inputBalise(self::PASSWORD, $this->tabPass);
		}

		function passwordConf()
		{
			parent::inputBalise(self::PASSWORDCONF, $this->tabPassConf);
		}

		function civilite()
		{
			//Tableau du select des civilités
			$tab_Civ = [
				parent::ID      => 'selectCiv',
				parent::NOM     => 'selectCiv',
				parent::CLASSE  => 'Class_OPT1',
				parent::LIBELLE => self::TAB_CIV_LABEL,
				parent::SQL     => true,
				parent::OPTIONS => $this->tabCivSQL,
			];

			parent::selectBalise(self::CIVILITE, $tab_Civ);
		}

		function nom()
		{
			parent::inputBalise(self::NOM, $this->tabNom);
		}

		function prenom()
		{
			parent::inputBalise(self::PRENOM, $this->tabPrenom);
		}

		function date()
		{
			parent::inputBalise(self::DATE, $this->tabDate);
		}

		function submit()
		{
			parent::inputBalise(self::SUBMIT, self::TAB_SUBMIT);
		}

		function formEnd()
		{
			parent::formEndBalise(self::END);
		}

		function formBeginAvatar()
		{
			parent::formBeginBalise(self::BEGIN_AVATAR, self::TAB_BEGIN_AVATAR);
		}

		function chemin()
		{
			parent::inputBalise(self::CHEMIN_AVATAR, self::TAB_CHEMIN);
		}

		function submitAvatar()
		{
			parent::inputBalise(self::SUBMIT_AVATAR, self::TAB_SUBMIT_AVATAR);
		}

		function transaction()
		{
			parent::aBalise(self::TAB_TRANSACTION, self::TRANSACTION);
		}
		//</editor-fold>

	}