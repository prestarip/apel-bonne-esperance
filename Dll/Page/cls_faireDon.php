<?php
	/**
	 * Created by PhpStorm.
	 * User: pythonirc
	 * Date: 18/07/18
	 * Time: 09:44
	 */

	namespace Apel\Dll\Page;

	use Apel\Dll\Framework\cls_ConstruitTemplate;

	class cls_faireDon extends cls_ConstruitTemplate
	{
		const    TITRE      = '#title#';
		const BOUTON = '#bouton_paypal#';

		function titre($contenu)
		{
			parent::remplacePage(self::TITRE, $contenu);
		}

	}