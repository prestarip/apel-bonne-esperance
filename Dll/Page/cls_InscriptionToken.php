<?php
	/**
	 * Created by PhpStorm.
	 * User: mathieu
	 * Date: 26/07/2018
	 * Time: 12:12
	 */

	namespace Apel\Dll\Page;

	use Apel\Dll\Framework\cls_ConstruitTemplate;

	class cls_InscriptionToken extends cls_ConstruitTemplate
	{
		//<editor-fold desc="Constante contenant les balises du template">
		const BEGIN   = '#begin#';
		const CLE     = '#cle#';
		const SUBMIT  = '#submit#';
		const END     = '#end#';
		const MESSAGE = '#message#';
		//</editor-fold>

		//<editor-fold desc="Constante TABLEAU">
		//Tableau pour le début du formulaire
		const TAB_BEGIN = [
			parent::ACTION  => '\Dll\Controleur\ControleurInscriptionToken.php',
			parent::METHODE => 'POST',
			'onsubmit'      => 'MonAppli.confirmToken(this)',
		];
		//Tableau pour le label du mail
		const TAB_CLE_LABEL = [
			parent::ID     => 'lblcle',
			parent::VALEUR => 'Clé d\'activation * :',
		];
		const TAB_CLE       = [
			parent::TYPE      => 'texte',
			parent::NOM       => 'cle',
			parent::ID        => 'cle',
			parent::CLASSE    => '',
			parent::AUTOFOCUS => '',
			parent::REQUIS    => '',
			parent::LIBELLE   => self::TAB_CLE_LABEL,
		];
		//Tableau pour le bouton submit
		const TAB_SUBMIT = [
			parent::TYPE     => 'submit',
			parent::NOM      => 'valider',
			parent::ID       => 'valider',
			parent::CLASSE   => '',
			parent::DISABLED => '',
			parent::VALEUR   => 'Confirmer',
		];

		//</editor-fold>

		function formBegin()
		{
			parent::formBeginBalise(self::BEGIN, self::TAB_BEGIN);
		}

		function cle()
		{
			parent::inputBalise(self::CLE, self::TAB_CLE);
		}

		function submit()
		{
			parent::inputBalise(self::SUBMIT, self::TAB_SUBMIT);
		}

		function formEnd()
		{
			parent::formEndBalise(self::END);
		}

		/**
		 * @param string $contenu Message contenu dans la variable de session 'message' (peut etre vide)
		 */
		function message($contenu)
		{
			parent::remplacePage(self::MESSAGE, $contenu);
		}
	}