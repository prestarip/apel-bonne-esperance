<?php
	/**
	 * Created by PhpStorm.
	 * User: pythonirc
	 * Date: 09/07/18
	 * Time: 19:42
	 */

	namespace Apel\Dll\Page;

	use Apel\Dll\Framework\cls_ConstruitTemplate;

	class cls_rss extends cls_ConstruitTemplate
	{
		const TITRE    = '#title#';
		const MESSAGE  = '#message#';
		const LIEN_RSS = '#lienRss#';

		function titre($contenu)
		{
			parent::remplacePage(self::TITRE, $contenu);
		}

		function message($contenu)
		{
			parent::remplacePage(self::MESSAGE, $contenu);
		}

		function lienRss($contenu)
		{
			parent::remplacePage(self::LIEN_RSS, $contenu);
		}
	}