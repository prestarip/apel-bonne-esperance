<?php
	/**
	 * Created by PhpStorm.
	 * User: mathieu
	 * Date: 13/06/2018
	 * Time: 20:28
	 */

	namespace Apel\Dll\Page;

	use Apel\Dll\Framework\cls_ConstruitTemplate;

	class cls_Login extends cls_ConstruitTemplate
	{
		//Constante contenant les balises du template login.htm
		const BEGIN    = '#begin#';
		const LOGIN    = '#login#';
		const PASSWORD = '#pass#';
		const SUBMIT   = '#submit#';
		const END      = '#end#';
		const MESSAGE  = '#message#';
		//Tableau pour le début du formulaire
		const TAB_BEGIN = [
			parent::ACTION  => '/Dll/Controleur/ControleurLogin.php',
			parent::METHODE => 'POST',
		];
		//Tableau pour le label du login
		const TAB_LOGIN_LABEL = [
			parent::ID     => 'lblLogin',
			parent::VALEUR => 'Votre login : ',
		];
		//Tableau du input du login
		const TAB_LOGIN = [
			parent::TYPE      => 'email',
			parent::NOM       => 'login',
			parent::ID        => 'login',
			parent::CLASSE    => 'class',
			parent::REQUIS    => '',
			parent::AUTOFOCUS => '',
			parent::LIBELLE   => self::TAB_LOGIN_LABEL,
		];
		//Tableau pour le label du password
		const TAB_PASSWORD_LABEL = [
			parent::ID     => 'lblPassword',
			parent::VALEUR => 'Votre mot de passe : ',
		];
		//Tableau du input du password
		const TAB_PASSWORD = [
			parent::TYPE    => 'password',
			parent::NOM     => 'pass',
			parent::ID      => 'pass',
			parent::CLASSE  => 'class',
			parent::REQUIS  => '',
			parent::LIBELLE => self::TAB_PASSWORD_LABEL,
		];
		//Tableau pour le bouton submit
		const TAB_SUBMIT = array(
			parent::TYPE   => 'submit',
			parent::NOM    => 'valider',
			parent::ID     => 'valider',
			parent::CLASSE => 'class',
			parent::VALEUR => 'Confirmer',
		);

		//Fonction qui appel les fonctions de cls_ConstruitTemplate
		function formBegin()
		{
			parent::formBeginBalise(self::BEGIN, self::TAB_BEGIN);
		}

		function login()
		{
			parent::inputBalise(self::LOGIN, self::TAB_LOGIN);
		}

		function password()
		{
			parent::inputBalise(self::PASSWORD, self::TAB_PASSWORD);
		}

		function submit()
		{
			parent::inputBalise(self::SUBMIT, self::TAB_SUBMIT);
		}

		function formEnd()
		{
			parent::formEndBalise(self::END);
		}

		/**
		 * @param string $contenu Message contenu dans la variable de session 'message' (peut etre vide)
		 */
		function message($contenu)
		{
			parent::remplacePage(self::MESSAGE, $contenu);
		}
	}