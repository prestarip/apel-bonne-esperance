<?php
	/**
	 * Created by PhpStorm.
	 * User: mathieu
	 * Date: 12/06/2018
	 * Time: 22:34
	 */

	namespace Apel\Dll\Page;

	use Apel\Dll\Framework\cls_ConstruitTemplate;

	class cls_Exemple extends cls_ConstruitTemplate
	{
		// <editor-fold desc="Constantes">

		const TITRE        = '#title#';
		const BEGIN        = '#begin#';
		const LOGIN        = '#login#';
		const PASSWORD     = '#pass#';
		const SELECT       = '#level#';
		const SUBMIT       = '#submit#';
		const END          = '#end#';
		const TABLE1       = '#table1#';
		const TABLE2       = '#table2#';
		const IMAGE        = '#image#';
		const A_LIEN       = '#a_lien#';
		const A_IMG        = '#a_img#';
		const LI           = '#li#';
		const UL           = '#ul#';
		const NAV          = '#nav#';
		const TITRE_VALEUR = 'INDEX - Test classe';
		const AREA         = '#textarea#';
// </editor-fold>
//
// <editor-fold desc="Form">

		const TAB_BEGIN = [
			parent::ACTION  => 'init.php',
			parent::METHODE => 'POST',
		];
		/**
		 * Tableau du Label
		 */
		const TAB_TEXT_LABEL = [
			parent::ID     => 'lblLogin',
			parent::VALEUR => 'Votre login : ',
		];
		/**
		 * Tableau du login (Input Text)
		 */
		const TAB_TEXT = [
			parent::TYPE      => 'text',
			parent::NOM       => 'login',
			parent::ID        => 'login',
			parent::CLASSE    => 'class',
			parent::REQUIS    => '',
			parent::AUTOFOCUS => '',
			parent::LIBELLE   => self::TAB_TEXT_LABEL,
		];
		// </editor-fold>
//
// <editor-fold desc="Login">
		/**
		 * Tableau du Label
		 */
		const TAB_PASSWORD_LABEL = [
			parent::ID     => 'lblPassword',
			parent::VALEUR => 'Votre mot de passe : ',
		];
		/**
		 * Tableau du mot de passe (Input Password)
		 */
		const TAB_PASSWORD = [
			parent::TYPE    => 'password',
			parent::NOM     => 'mdp',
			parent::ID      => 'mdp',
			parent::CLASSE  => 'class',
			parent::REQUIS  => '',
			parent::LIBELLE => self::TAB_PASSWORD_LABEL,
		];
		/**
		 * Tableau du bouton 'Confirmer' (Input Submit)
		 */
		const TAB_SUBMIT = array(
			parent::TYPE   => 'submit',
			parent::NOM    => 'valider',
			parent::ID     => 'valider',
			parent::CLASSE => 'class',
			parent::VALEUR => 'Confirmer',
		);
// </editor-fold>
//
// <editor-fold desc="Password">
		/**
		 * Tableau du Label
		 */
		const TAB_SELECT_LABEL = [
			parent::ID     => 'lblSelect',
			parent::VALEUR => 'Votre niveau : ',
		];
		/**
		 * Tableau des options 1 pour le groupe option 1
		 */
		const TAB_SELECT_OPTION1 = [
			'1' => [parent::VALEUR => 'facile1'],
			'2' => [parent::VALEUR => 'moyen1'],
			'3' => [parent::VALEUR => 'difficile1', parent::SELECTIONNER => ''],
		];
		/**
		 * Tableau des options 2 pour le groupe option 2
		 */
		const TAB_SELECT_OPTION2 = [
			'1' => [parent::VALEUR => 'facile2'],
			'2' => [parent::VALEUR => 'moyen2'],
			'3' => [parent::VALEUR => 'difficile2'],
		];
// </editor-fold>
//
// <editor-fold desc="Submit">
		/**
		 * Tableau des options 3 pour le groupe option 3
		 */
		const TAB_SELECT_OPTION3 = [
			'1' => [parent::VALEUR => 'facile3'],
			'2' => [parent::VALEUR => 'moyen3'],
			'3' => [parent::VALEUR => 'difficile3', parent::GRISER => ''],
		];
		/**
		 * Tableau des groupes options 1
		 */
		const TAB_OPTGROUP1 = [
			parent::ID      => 'TAB_OPTGROUP1',
			parent::LIBELLE => 'Label_OPT1',
			parent::CLASSE  => 'Class_OPT1',
			parent::OPTIONS => self::TAB_SELECT_OPTION1,
		];
// </editor-fold>
//
// <editor-fold desc="Select">
		/**
		 * Tableau des groupes options 2
		 */
		const TAB_OPTGROUP2 = [
			parent::ID      => 'TAB_OPTGROUP2',
			parent::LIBELLE => 'Label_OPT2',
			parent::CLASSE  => 'Class_OPT2',
			parent::GRISER  => '',
			parent::OPTIONS => self::TAB_SELECT_OPTION2,
		];
		/**
		 * Tableau des options
		 */
		/*
		  const TAB_SELECT_OPTION0 = [
		  '1' => [parent::VALEUR => 'facile0'],
		  '2' => [parent::VALEUR => 'moyen0', parent::SELECTIONNER => ''],
		  '3' => [parent::VALEUR => 'difficle0'],
		  ];
		 */
		/**
		 * Tableau des groupes options 3
		 */
		const TAB_OPTGROUP3 = [
			parent::ID      => 'TAB_OPTGROUP3',
			parent::LIBELLE => 'Label_OPT3',
			parent::CLASSE  => 'Class_OPT3',
			parent::OPTIONS => self::TAB_SELECT_OPTION3,
		];
		/**
		 * Tableau des niveaux (select + opt groupe)
		 */
		const TAB_SELECT = [
			parent::ID      => 'select',
			parent::LIBELLE => self::TAB_SELECT_LABEL,
			parent::OPTGRP  => [self::TAB_OPTGROUP1, self::TAB_OPTGROUP2, self::TAB_OPTGROUP3],
			//parent::OPTIONS => self::TAB_SELECT_OPTION0,
		];
		/**
		 * Tableau du caption
		 */
		const CAPTION = [
			parent::ID     => 'Table_Cap',
			parent::VALEUR => 'Table Caption : ',
		];
		/**
		 * Tableau du thead
		 */
		const TAB_THEAD = [
			'Prénom', 'Nom', 'Age',
		];
		/**
		 * Tableau du tbody
		 */
		const TAB_TBODY = [
			['GuiGui', 'PHP Storm', '22'],
			['Vince', 'Chef', '21'],
		];
		/**
		 * Tableau du tfoot
		 */
		const TAB_TFOOT = [
			['Foot 1', 'Foot 2', 'Foot 3'],
		];
		/**
		 * Tableau de la table 1
		 */
		const TAB_TABLE1 = [
			parent::ID      => 'table_ID',
			parent::THEAD   => self::TAB_THEAD,
			parent::TBODY   => self::TAB_TBODY,
			parent::TFOOT   => self::TAB_TFOOT,
			parent::CAPTION => self::CAPTION,
			parent::CLASSE  => 'Class_Table',
		];
		/**
		 * Tableau des TH
		 */
		const TAB_TH = [
			'Prénom', 'Nom', 'Age',
		];
// </editor-fold>
//
// <editor-fold desc="Fonctions">
		/**
		 * Tableau des TD
		 */
		const TAB_TD = [
			['Papi', 'Marechet', '30'],
			['Mami', 'Marechet', '29'],
		];
		// </editor-fold>
//
// <editor-fold desc="Table avec thead, tbody, tfoot">
		/**
		 * Tableau de la table 2
		 */
		const TAB_TABLE2 = [
			parent::ID      => 'table_ID',
			parent::TH      => self::TAB_TH,
			parent::TD      => self::TAB_TD,
			parent::CAPTION => self::CAPTION,
			parent::CLASSE  => 'Class_Table',
		];
		const TAB_IMG    = [
			parent::ALT    => 'Pornhub',
			parent::SOURCE => 'Images/images.png',
		];
		const TAB_A_LIEN = [
			parent::HREF   => 'https://www.youporn.com/',
			parent::TARGET => '_blank',
			parent::VALEUR => 'Il clic ici !',
		];
		const TAB_A_IMG  = [
			parent::HREF   => 'https://www.pornhub.com/',
			parent::TARGET => '_blank',
			parent::IMAGE  => self::TAB_IMG,
		];
		const TAB_LI     = [
			0 => [
				parent::CLASSE => 'class',
				parent::A      => self::TAB_A_LIEN
			],
			1 => [
				parent::CLASSE => 'class',
				parent::A      => self::TAB_A_IMG
			],
		];
		const TAB_UL     = [
			0 => [
				parent::CLASSE => 'class1',
				parent::LI     => self::TAB_LI
			],
			1 => [
				parent::CLASSE => 'class2',
				parent::LI     => self::TAB_LI
			],
		];
//
// </editor-fold>
//
// <editor-fold desc="Table avec th, td">
		const TAB_NAV        = [
			parent::CLASSE => 'class',
			parent::UL     => self::TAB_UL,
		];
		const TAB_AREA_LABEL = [
			parent::ID     => 'lblArea',
			parent::VALEUR => 'Exemple Text Area : ',
		];
		const TAB_AREA       = [
			parent::CLASSE  => 'class',
			parent::LIBELLE => self::TAB_AREA_LABEL,
			'row'           => '6',
			parent::VALEUR  => 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles,',
		];

		function textArea()
		{
			parent::textAreaBalise(self::AREA, self::TAB_AREA);
		}

		/**
		 * Permet de générer le début du formulaire
		 */
		function formBegin()
		{
			parent::formBeginBalise(self::BEGIN, self::TAB_BEGIN);
		}

		/**
		 * Permet de générer la fin du formulaire
		 */
		function formEnd()
		{
			parent::formEndBalise(self::END);
		}

		/**
		 * Permet de générer le login
		 */
		function login()
		{
			parent::inputBalise(self::LOGIN, self::TAB_TEXT);
		}

//
// </editor-fold>
//

		/**
		 * Permet de générer le password
		 */
		function password()
		{
			parent::inputBalise(self::PASSWORD, self::TAB_PASSWORD);
		}

		/**
		 * Permet de générer le bouton submit
		 */
		function submit()
		{
			parent::inputBalise(self::SUBMIT, self::TAB_SUBMIT);
		}

		/**
		 * Permet de générer le choix du level
		 */
		function select()
		{
			parent::selectBalise(self::SELECT, self::TAB_SELECT);
		}

		/**
		 * Permet de générer le titre
		 */
		function titre()
		{
			parent::remplacePage(self::TITRE, self::TITRE_VALEUR);
		}

		/**
		 * Permet de générer un tableau
		 */
		function table1()
		{
			parent::tableBalise(self::TABLE1, self::TAB_TABLE1);
		}

		/**
		 * Permet de générer un tableau
		 */
		function table2()
		{
			parent::tableBalise(self::TABLE2, self::TAB_TABLE2);
		}

		function image()
		{
			parent::imgBalise(self::TAB_IMG, self::IMAGE);
		}

		function a_lien()
		{
			parent::aBalise(self::TAB_A_LIEN, self::A_LIEN);
		}

		function a_img()
		{
			parent::aBalise(self::TAB_A_IMG, self::A_IMG);
		}

		function li()
		{
			parent::liBalise(self::TAB_LI, self::LI);
		}

		function ul()
		{
			parent::ulBalise(self::TAB_UL, self::UL);
		}

		function nav()
		{
			parent::navBalise(self::NAV, self::TAB_NAV);
		}
	}