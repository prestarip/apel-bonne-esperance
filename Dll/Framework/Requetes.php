<?php
	/**
	 * Created by PhpStorm.
	 * User: GuillaumeFixe
	 * Date: 11/06/2018
	 * Time: 19:31
	 */

	namespace Apel\Dll\Framework;

	class Requetes
	{
		/* Civilite */
		const GET_ALL_CIVILITE = 'SELECT idCivilite, libelle FROM tb_Civilite';
		/* Token */
		const GET_ALL_TOKEN   = 'SELECT idtoken, token, fk_idgroupe, dategeneration FROM tb_token';
		const GET_TOKEN_BY_ID = 'SELECT t.* FROM public.tb_token t WHERE idtoken=?';
		/* Compte */
		const GET_LOGIN_BY_MAIL = 'SELECT mdp FROM tb_Compte WHERE mail=?';
		const GET_ALL_COMPTE    = '	SELECT idcompte, nom, prenom, fk_idcivilite, mail, mdp, datenaissance, dateinscription, 
									  fk_idgroupe, codegroupe, idcivilite, tb_civilite.libelle AS libellecivilite, 
									  tb_groupe.libelle AS libellegroupe, idgroupe
									FROM tb_compte
										LEFT JOIN tb_civilite ON tb_compte.fk_idcivilite = tb_civilite.idcivilite
										LEFT JOIN tb_groupe ON tb_groupe.idgroupe = tb_compte.fk_idgroupe';
		/* Commune */
		const GET_ALL_COMMUNE = 'SELECT codepostal,commune FROM tb_commune ORDER BY codepostal';
		/* Chat : Messagerie instantanée */
		const  GET_ALL_MESSAGE_CHAT_BY_USER = '	SELECT idcompte, nom, prenom, tb_message_instant.idmessageinstant, tb_message_instant.fk_idcompte_to,
														tb_message_instant.fk_idcompte_from, tb_message_instant.message, tb_message_instant.dateenvoi
												FROM tb_message_instant
													LEFT JOIN tb_compte ON fk_idcompte_from = idcompte
												WHERE (fk_idcompte_from = :idcompte_from AND fk_idcompte_to = :idcompte_to) 
													OR (fk_idcompte_from = :idcompte_to AND fk_idcompte_to = :idcompte_from )
												ORDER BY dateenvoi';
		const  GET_ALL_NEW_MESSAGE_CHAT_BY_USER = '	SELECT idcompte, nom, prenom, tb_message_instant.idmessageinstant, tb_message_instant.fk_idcompte_to,
														tb_message_instant.fk_idcompte_from, tb_message_instant.message, tb_message_instant.dateenvoi
												FROM tb_message_instant
													LEFT JOIN tb_compte ON fk_idcompte_from = idcompte
												WHERE ((fk_idcompte_from = :idcompte_from AND fk_idcompte_to = :idcompte_to) 
													OR (fk_idcompte_from = :idcompte_to AND fk_idcompte_to = :idcompte_from ))
													 AND tb_message_instant.idmessageinstant > :idLastMessage
												ORDER BY dateenvoi';
//		const  GET_MAX_ID_MESSAGE           = '	SELECT max(idmessageinstant) AS max_id_message_instant
//												FROM tb_message_instant';
		const  ADD_CHAT_MESSAGE             = '	INSERT INTO tb_message_instant(idmessageinstant,
													fk_idcompte_to, fk_idcompte_from,
													message, dateenvoi)
												VALUES (DEFAULT, :idcompte_to, :idcompte_from, :message, :dateenvoi);
';
		/* News */
		const GET_NEWS       = 'SELECT tb_actualite.idactualite, tb_actualite.titre, tb_actualite.contenu, tb_actualite.dateactualite , tb_compte.nom, tb_compte.prenom FROM tb_actualite, tb_compte WHERE fk_idcompte = idcompte ORDER BY tb_actualite.idactualite DESC';
		const GET_NEWS_5     = 'SELECT tb_actualite.idactualite, tb_actualite.titre, tb_actualite.contenu, tb_actualite.dateactualite , tb_compte.nom, tb_compte.prenom FROM tb_actualite, tb_compte WHERE fk_idcompte = idcompte ORDER BY tb_actualite.idactualite DESC  LIMIT 5';
		const GET_NEWS_BY_ID = 'SELECT tb_actualite.idactualite, tb_actualite.titre, tb_actualite.contenu, tb_actualite.dateactualite , tb_compte.nom, tb_compte.prenom FROM tb_actualite, tb_compte WHERE fk_idcompte = idcompte AND idactualite =?';
		const GET_NEWS_ID    = 'SELECT idactualite FROM tb_actualite';
		/* Paiements */
		const INSERT_PAYMENT = 'INSERT INTO tb_mouvement_financier (fk_idtypemouvement, montant, dateMouvement, fk_idcompte) VALUES (?,?,?,?)';
	}