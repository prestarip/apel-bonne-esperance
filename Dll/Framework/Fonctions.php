<?php
	/**
	 * Created by PhpStorm.
	 * User: Guillaume
	 * Date: 29/08/2018
	 * Time: 11:50
	 */

	namespace Apel\Dll\Framework;

	class Fonctions
	{
		public static function deconnexion()
		{
			$_SESSION['loginOK'] = false;
			unset($_SESSION['loginOK']);
		}

		public static function getActualUser()
		{
			if(self::isConnected() && isset($_SESSION['compte']) && empty($_SESSION['compte']) == false)
			{
				return unserialize($_SESSION['compte']);
			}
			else
			{
				return null;
			}
		}

		public static function isConnected()
		{
			if(isset($_SESSION['loginOK']) && $_SESSION['loginOK'] == true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}