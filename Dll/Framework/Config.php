<?php
	/**
	 * Created by PhpStorm.
	 * User: GuillaumeFixe
	 * Date: 04/06/2018
	 * Time: 21:59
	 */

	namespace Apel\Dll\Framework;

	/**
	 * Class Config
	 * @package Apel\Dll\Framework
	 */
	class Config
	{
		const JSON_FILE_CONFIG  = '/Dll/Framework/Config.json';
		const JSON_FILE_ADDRESS = '/Dll/Framework/Address.json';

		/**
		 * Config constructor.
		 */

		public static function getConfig()
		{
			$contenuFichierConfig = file_get_contents($_SERVER['DOCUMENT_ROOT'] . self::JSON_FILE_CONFIG, FILE_USE_INCLUDE_PATH);

			return json_decode($contenuFichierConfig);
		} /**/

		/**
		 * @param String $address
		 *
		 * @return string
		 */
		public static function getAdresse(String $address) : string
		{
			$contenuFichierAdresse = file_get_contents($_SERVER['DOCUMENT_ROOT'] . self::JSON_FILE_ADDRESS, FILE_USE_INCLUDE_PATH);
			$AllAddressObject      = json_decode($contenuFichierAdresse);

			return ($AllAddressObject)->{strtoupper($address)} ?? '';
		}

		/**
		 * @param String $address
		 *
		 * @return string
		 */
		public static function getRequireOnce(String $address) : string
		{
			$file = require_once($_SERVER['DOCUMENT_ROOT'] . self::getAdresse($address));
			$file = ($file <> '1') ? $file : 'L\'inclusion du fichier "'.$address.'" a échoué.<br />' ;

			return $file;
		}
	}