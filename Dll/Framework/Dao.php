<?php
	/**
	 * Created by PhpStorm.
	 * User: GuillaumeFixe
	 * Date: 04/06/2018
	 * Time: 21:58
	 */

	namespace Apel\Dll\Framework;

	use Apel\Dll\Business\cls_Civilite;
	use Apel\Dll\Business\cls_Compte;
	use Apel\Dll\Business\cls_Groupe;
	use Apel\Dll\Business\cls_Token;

	/**
	 * Class Dao
	 * @package Apel\Dll\Framework
	 */
	class Dao
	{
		const REQUETES = '/Dll/Framework/Requetes.json';
		/** @var string $host */
		private $host;
		/** @var string $database */
		private $database;
		/** @var string $user */
		private $user;
		/** @var string $password */
		private $password;
		/** @var \PDO $connexion */
		private $connexion;

		/**
		 * Dao constructor.
		 */
		public function __construct()
		{
			try
			{
				$parsed_json = Config::getConfig();

				$this->host     = $parsed_json->{'HOST'};  /*46.105.58.119*/
				$this->database = $parsed_json->{'DBNAME'};
				$this->user     = $parsed_json->{'WRITE_USER'};
				$this->password = $parsed_json->{'WRITE_PASSWORD'};

				$this->connect();

				$this->connexion->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
				date_default_timezone_set('Europe/Paris'); // récupère fuseaux horaire
			}
			catch(\PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		public function connect()
		{
			$this->connexion = new \PDO('pgsql:dbname=' . $this->database . ';host=' . $this->host, $this->user, $this->password);

		}

		public function getConnexion()
		{
			return $this->connexion;
		}

		public function getAllCivilite()
		{
			$res = $this->connexion->query(Requetes::GET_ALL_CIVILITE);
			$res->setFetchMode(\PDO::FETCH_ASSOC);
			$listeCivilite = $res->fetchAll();

			return $listeCivilite;
		}

		public function getAllCompte()
		{
			$result = $this->connexion->query(Requetes::GET_ALL_COMPTE);
			$result->setFetchMode(\PDO::FETCH_ASSOC);
			$listeCompte = $result->fetchAll();
			foreach($listeCompte as $key => $value)
			{
				$compte = new cls_Compte($value['idcompte'],
										 $value['nom'],
										 $value['prenom'],
										 $value['mail'],
										 $value['mdp'],
										 new \DateTime($value['datenaissance']),
										 new \DateTime($value['dateinscription']),
										 new cls_Civilite($value['idcivilite'], $value['libellecivilite']),
										 new cls_Groupe($value['idgroupe'], $value['codegroupe'], $value['libellegroupe'])
				);

				$listeCompte[$key] = $compte;
			}
//			$arguments = cls_Compte::getPropertiesClass();
//			$listeCivilite = $result->fetchAll( \PDO::FETCH_CLASS, cls_Compte::class , $arguments );
			return $listeCompte;
		}

		public function getTokenById($idToken)
		{
			$result    = $this->connexion->query(Requetes::GET_ALL_TOKEN);
			$arguments = cls_Token::getPropertiesClass();

//			$token = $result->fetchAll( \PDO::FETCH_CLASS, cls_Token::class , $arguments );
			return $token;
		}

		public function getAllMessageChat($idCompteTo, $idLastMessage = null)
		{
			$idUser = unserialize($_SESSION['compte'])->getIdCompte();

			if($idLastMessage <> null)
			{
				$requete = $this->connexion->prepare(Requetes::GET_ALL_NEW_MESSAGE_CHAT_BY_USER);
				$requete->execute(
					array(
						':idcompte_to'   => $idCompteTo,
						':idcompte_from' => $idUser,
						':idLastMessage' => $idLastMessage
					)
				);
			}
			else
			{
				$requete = $this->connexion->prepare(Requetes::GET_ALL_MESSAGE_CHAT_BY_USER);
				$requete->execute(
					array(
						':idcompte_to'   => $idCompteTo,
						':idcompte_from' => $idUser
					)
				);
			}
			$listeMessageChat = $requete->fetchAll();

			return $listeMessageChat;
		}

		public function addChatMessage($idCompteTo, $message)
		{
//			$resultatIdMaxMessage = $this->connexion->query(Requetes::GET_MAX_ID_MESSAGE, \PDO::FETCH_ASSOC)->fetchAll();
//			$resultatIdMaxMessage = ($resultatIdMaxMessage <> false) ? $resultatIdMaxMessage[0]['max_id_message_instant'] + 1 : false;
			/** @var \DateTime $now */
			$now = date("Y-m-d H:i:s");

			$idUser  = unserialize($_SESSION['compte'])->getIdCompte();
			$requete = $this->connexion->prepare(Requetes::ADD_CHAT_MESSAGE);
			try
			{
				$requete->execute(
					array(
						':idcompte_to'   => $idCompteTo,
						':idcompte_from' => $idUser,
						':message'       => $message,
						':dateenvoi'     => $now,
					)
				);
				$resultat = $this->connexion->lastInsertId();
			}
			catch(\Exception $exceptionSQL)
			{
				$resultat = $exceptionSQL->errorInfo[0] . ' : ' . $exceptionSQL->errorInfo[2];
			}

			return $resultat;
		}

		public function getNews($idActu, $limit)
		{
			if($idActu == 0)
			{
				if($limit == false)
				{
					$res = $this->connexion->query(Requetes::GET_NEWS);
					$res->setFetchMode(\PDO::FETCH_ASSOC);
					$listeNews = $res->fetchAll();
				}
				else
				{
					$res = $this->connexion->query(Requetes::GET_NEWS_5);
					$res->setFetchMode(\PDO::FETCH_ASSOC);
					$listeNews = $res->fetchAll();
				}
			}
			else
			{
				$res = $this->connexion->prepare(Requetes::GET_NEWS_BY_ID);
				$res->bindParam(1, $idActu);
				$res->execute();

				$listeNews = $res->fetchAll();
			}

			return $listeNews;
		}

		public function getNewsId()
		{
			$res = $this->connexion->query(Requetes::GET_NEWS_ID);
			$res->setFetchMode(\PDO::FETCH_ASSOC);
			$idActu = $res->fetchAll();

			return $idActu;
		}

		public function insertPayment($item_name, $payment_amount, $date, $id_user)
		{
			$res = $this->connexion->prepare(Requetes::INSERT_PAYMENT);
			$res->bindParam(1, $item_name);
			$res->bindParam(2, $payment_amount);
			$res->bindParam(3, $date);
			$res->bindParam(4, $id_user);
			$res->execute();
		}
	}