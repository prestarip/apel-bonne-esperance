<?php
	/**
	 * Created by PhpStorm.
	 * User: mathieu
	 * Date: 12/06/2018
	 * Time: 22:29
	 */

	namespace Apel\Dll\Framework;

	class cls_ConstruitTemplate
	{
// <editor-fold desc="Variables de la classe">
		//region Variables pour le contenu de la page
		const ACTION = 'action';
		//endregion

		const SQL = 'SQL';
		//region Variables pour le label
		const METHODE = 'method';
		const TYPE    = 'type';
		const NOM     = 'name';
		const ID      = 'id';
		//endregion

		//region Variables pour les options
		const CLASSE    = 'class';
		const REQUIS    = 'required';
		const AUTOFOCUS = 'autofocus';
		const LIBELLE   = 'label';
		const VALEUR    = 'value';
		const DISABLED  = 'disabled';
		//endregion

		//region Variables pour le select
		const OPTIONS = 'tabOption';
		const OPTGRP  = 'OPTGRP';
		//endregion

		//region Variables pour la table
		const SELECTIONNER = 'selected';
		const GRISER       = 'disabled';
		const THEAD        = 'thead';
		const TBODY        = 'tbody';
		const TFOOT        = 'tfoot';
		//endregion

		//region Variables pour la caption
		const CAPTION = 'caption';
		const TH      = 'th';
		const TD      = 'td';
		const SOURCE  = 'src';
		//endregion

		//region Variables pour la li
		const ALT   = 'alt';
		const IMAGE = 'img';
		const HREF  = 'href';
		//endregion

		//region Variables pour la ul
		const TARGET = 'target';
		const A      = 'a';
		const LI     = 'li';
		//endregion

		//region Variables pour l'image
		const UL = 'ul';
		//endregion

		//region Variables pour la a
		const NAV = 'nav';
		private $screen;
		private $label_Str;
		//endregion

		//region Variables pour la nav
		private $label_For;
		private $label_Value;
		private $label_Format;
		//endregion
		private $optionGrp_Str;
		private $optionGrp_Format;
// </editor-fold>

// <editor-fold desc="Constantes de la classe">
		private $option_Str;
		private $option_Value;
		private $option_Format;
		private $select_Str;
		private $select_Format;
		private $table_Format;
		private $table_Str;
		private $table_TH;
		private $table_TD;
		private $table_Caption;
		private $caption_Str;
		private $caption_For;
		private $caption_Value;
		private $caption_Format;
		private $li_Str;
		private $li_Value;
		private $li_Format;
		private $ul_Str;
		private $ul_Value;
		private $ul_Format;
		private $img_Str;
		private $a_Str;
		private $a_Value;
		private $a_Format;
		private $nav_Str;
		private $nav_Value;
		private $nav_Format;
		private $input_Str;
		private $input_Format;
		private $area_Str;
		private $area_Format;
		private $area_Value;
		private $sql;
// </editor-fold>

// <editor-fold desc="Constructeur">

		/**
		 * cls_ConstruitTemplate constructor.
		 *
		 * @param string $pageHtml Page HTML contenant les balises à remplacer
		 * @param bool   $layout
		 */
		function __construct(string $pageHtml, bool $layout)
		{
			$this->screen = file_get_contents($_SERVER['DOCUMENT_ROOT'] . $pageHtml);

			//Initialisation des formats (qui vont servir pour le formatString => sprintf)
			$this->label_Format     = 'for="%1$s">%2$s</label>';
			$this->input_Format     = '%1$s%2$s/>';
			$this->select_Format    = '>%1$s</select>';
			$this->optionGrp_Format = '>%1$s</optgroup>';
			$this->option_Format    = '>%1$s</option>';
			$this->table_Format     = '%1$s%2$s</table>';
			$this->caption_Format   = 'for="%1$s">%2$s</caption>';
			$this->li_Format        = '>%1$s</li>';
			$this->a_Format         = '>%1$s</a>';
			$this->ul_Format        = '>%1$s</ul>';
			$this->nav_Format       = '>%1$s</nav>';
			$this->area_Format      = '%1$s%2$s%3$s</textarea>';

			$this->sql = false;

			//Permet de rénitilaiser les autres variables
			$this->razVariable();
			//Permet d'intégrer automatiquement le layout du site
			switch($layout)
			{
				case 1:
					$this->screen = $this->readLayout('HEADER_PHP') .
						$this->screen .
						$this->readLayout('SIDE_BAR_PHP') .
						$this->readLayout('FOOTER_PHP');
					break;

				case 0:
				default:
					break;
			}
		}

		/**
		 * Remet les variables de classe à vide
		 */
		private function razVariable()
		{
			$this->label_Str     = '';
			$this->label_For     = '';
			$this->label_Value   = '';
			$this->option_Str    = '';
			$this->select_Str    = '';
			$this->optionGrp_Str = '';
			$this->table_Str     = '';
			$this->table_TH      = '';
			$this->table_TD      = '';
			$this->table_Caption = '';
			$this->caption_Str   = '';
			$this->caption_For   = '';
			$this->caption_Value = '';
			$this->li_Str        = '';
			$this->li_Value      = '';
			$this->img_Str       = '';
			$this->a_Str         = '';
			$this->a_Value       = '';
			$this->ul_Str        = '';
			$this->ul_Value      = '';
			$this->nav_Str       = '';
			$this->nav_Value     = '';
			$this->area_Str      = '';
			$this->area_Value    = '';
			$this->sql           = false;
		}
// </editor-fold>

// <editor-fold desc="Mise en page">

		/**
		 * @param $page
		 *
		 * @return string
		 */
		private function readLayout($page)
		{
			return Config::getRequireOnce($page);
		}

		/**
		 * Retourne la page HTML à afficher
		 * @return string Page HTML à afficher
		 */
		function getPage()
		{
			return $this->screen;
		}
// </editor-fold>

// <editor-fold desc="Form">

		/**
		 * Génère une balise <form> (Début)
		 *
		 * @param string $balise Balise à remplacer
		 * @param array  $tab    Tableau des arguments de la balise
		 */
		function formBeginBalise($balise, $tab)
		{
			$l_str = '<form ';

			foreach($tab as $key => $value)
			{
				$l_str .= $key . '="' . $value . '" ';
			}

			$l_str .= '>';

			$this->remplacePage($balise, $l_str);
		}

		/**
		 * Remplace le balise de la page HTML par le contenu
		 * puis remet à vide les variables
		 *
		 * @param string $balise  Balise à remplacer
		 * @param string $contenu Contenu à insérer dans la page
		 *
		 * @author Mathieu
		 */
		function remplacePage($balise, $contenu)
		{
			$this->screen = str_replace($balise, $contenu, $this->screen);
			$this->razVariable();
		}

		/**
		 * Génère une balise </form> (Fin)
		 *
		 * @param string $balise Balise à remplacer
		 *
		 * @author Mathieu
		 */
		function formEndBalise($balise)
		{
			$this->remplacePage($balise, '</form>');
		}

// </editor-fold>

// <editor-fold desc="Label">

		/**
		 * Génère une balise <input>
		 * et une balise <label> si nécessaire
		 *
		 * @param string $balise Balise à remplacer
		 * @param array  $tab    Tableau des arguments de la balise
		 */
		function inputBalise($balise, $tab)
		{
			$this->input_Str = '<input ';

			foreach($tab as $key => $value)
			{
				$this->inputSwitch($key, $value);
			}

			$this->input_Str = sprintf($this->input_Format, $this->label_Str, $this->input_Str);

			$this->remplacePage($balise, $this->input_Str);
		}

// </editor-fold>

// <editor-fold desc="Input">

		/**
		 * Répartit les différentes clés du tableau
		 * afin de créer la balise
		 *
		 * @param string $key   Clé du tableau
		 * @param mixed  $value Valeur du tableau
		 */
		private function inputSwitch($key, $value)
		{
			switch($key)
			{
				case self::ID:
					$this->label_For = $value;
					$this->input_Str .= $key . '="' . $value . '" ';
					break;

				case self::LIBELLE :
					$this->labelBalise($value);
					break;

				default :
					$this->input_Str .= $key . '="' . $value . '" ';
					break;
			}
		}

		/**
		 * Créer une balise <label>
		 * qui sera générée par la fonction inputBalise()
		 *
		 * @param array $tab Tableau des arguments de la balise
		 */
		function labelBalise($tab)
		{
			$this->label_Str = '<label ';

			foreach($tab as $key => $value)
			{
				if($key == self::VALEUR)
				{
					$this->label_Value = $value;
				}
				else
				{
					$this->label_Str .= $key . '="' . $value . '" ';
				}
			}

			$this->label_Str .= sprintf($this->label_Format, $this->label_For, $this->label_Value);
		}

// </editor-fold>

//<editor-fold desc="Select">

		/**
		 * Génère une balise <select> et les balises <option>
		 *
		 * @param string $balise Balise à remplacer
		 * @param array  $tab    Tableau des arguments de la balise
		 */
		function selectBalise($balise, $tab)
		{
			$this->select_Str = '<select ';

			foreach($tab as $key => $value)
			{
				$this->selectSwitch($key, $value);
			}

			if($this->optionGrp_Str == '')
			{
				$this->select_Str .= sprintf($this->select_Format, $this->option_Str);
			}
			else
			{
				$this->select_Str .= sprintf($this->select_Format, $this->optionGrp_Str);
			}

			$l_str = $this->label_Str == '' ? $this->select_Str : $this->label_Str . $this->select_Str;

			$this->remplacePage($balise, $l_str);
		}

		/**
		 * Répartit les différentes clés du tableau
		 * afin de créer la balise
		 *
		 * @param string $key   Clé du tableau
		 * @param mixed  $value Valeur du tableau
		 */
		private function selectSwitch($key, $value)
		{
//			$sql = false;

			switch($key)
			{
				case self::ID:
					$this->label_For  = $value;
					$this->select_Str .= $key . '="' . $value . '" ';
					break;

				case self::LIBELLE :
					$this->labelBalise($value);
					break;

				case self::OPTIONS :
					$this->constructOption($value);
					break;

				case self::OPTGRP :
					$this->constructOptionGroup($value);
					break;

				case self::SQL :
					$this->sql = $value;
					break;

				default:
					$this->select_Str .= $key . '="' . $value . '" ';
					break;
			}
		}

		/**
		 * Construit les différentes options
		 *
		 * @param array $tab Tableau des arguments de la balise
		 */
		private function constructOption($tab)
		{
			if($this->sql)
			{
				// Récupération des noms de champs
				$tKeys = array_keys($tab[0]);

				// Génération des listes de valeurs
				foreach($tab as $tEnreg)
				{
					//$comboStr .= '<option ' . $class . ' value="' . $tEnreg[$tKeys[0]] . '">' . mb_convert_encoding($tEnreg[$tKeys[1]], "UTF-8", "ISO-8859-15") . '</option>';
					$this->option_Str   .= '<option value="' . $tEnreg[$tKeys[0]] . '" ';
					$this->option_Value = $tEnreg[$tKeys[1]];
					$this->option_Str   .= sprintf($this->option_Format, $this->option_Value);
				}
			}
			else
			{
				foreach($tab as $cle => $val)
				{
					$this->option_Str .= '<option value="' . $cle . '" ';

					foreach($val as $key => $value)
					{
						if($key == self::VALEUR)
						{
							$this->option_Value = $value;
						}
						else
						{
							$this->option_Str .= $key . '="' . $value . '" ';
						}
					}
					$this->option_Str .= sprintf($this->option_Format, $this->option_Value);
				}
			}
		}

		/**
		 * Construit les différents groupe options
		 *
		 * @param array $tab Tableau des arguments de la balise
		 */
		private function constructOptionGroup($tab)
		{
			foreach($tab as $cle => $val)
			{
				$this->optionGrp_Str .= '<optgroup ';

				foreach($val as $key => $value)
				{
					if($key == self::OPTIONS)
					{
						$this->constructOption($value);
					}
					else
					{
						$this->optionGrp_Str .= $key . '="' . $value . '" ';
					}
				}

				$this->optionGrp_Str .= sprintf($this->optionGrp_Format, $this->option_Str);
				$this->option_Str    = '';
			}
		}
//</editor-fold>

//<editor-fold desc="Table">
		/**
		 * Génère une balise <table>
		 * et toutes les autres balises nécéssaires
		 * <tr>,<th>,<td> ...
		 *
		 * @param string $balise Balise à remplacer
		 * @param array  $tab    Tableau des arguments de la balise
		 */
		function tableBalise($balise, $tab)
		{
			$this->table_Str = '<table ';

			foreach($tab as $key => $value)
			{
				$this->tableSwitch($key, $value);
			}

			$this->table_Str .= '>';

			$this->table_Str .= $this->caption_Str == '' ? '' : $this->caption_Str;

			$this->table_Str .= sprintf($this->table_Format, $this->table_TH, $this->table_TD);

			$this->remplacePage($balise, $this->table_Str);
		}

		/**
		 * Répartit les différentes clés du tableau
		 * afin de créer la balise
		 *
		 * @param string $key   Clé du tableau
		 * @param mixed  $value Valeur du tableau
		 */
		private function tableSwitch($key, $value)
		{
			switch($key)
			{
				case self::TH :
					$this->thTable($value);
					break;

				case self::TD :
					$this->tdTable($value);
					break;

				case self::THEAD :
					$this->thTable($value, $key);
					break;

				case self::TFOOT:
				case self::TBODY:
					$this->tdTable($value, $key);
					break;

				case self::CAPTION:
					$this->captionBalise($value);
					break;

				case self::ID:
					$this->caption_For = $value;
					$this->table_Str   .= $key . '="' . $value . '" ';
					break;

				default:
					$this->table_Str .= $key . '="' . $value . '" ';
					break;
			}
		}

		/**
		 * Construit les différentes th
		 *
		 * @param array  $value  Tableau contenant les th
		 * @param string $balise Balise à remplacer (Optionnel)
		 */
		private function thTable($value, $balise = '')
		{
			$this->table_TH .= $balise == '' ? '<tr>' : '<' . $balise . '><tr>';

			foreach($value as $valueth)
			{
				$this->table_TH .= '<th>' . $valueth . '</th>';
			}

			$this->table_TH .= $balise == '' ? '</tr>' : '</' . $balise . '></tr>';
		}

		/**
		 * Construit les différentes td
		 *
		 * @param array  $value  Tableau contenant les td
		 * @param string $balise Balise à remplacer (Optionnel)
		 */
		private function tdTable($value, $balise = '')
		{
			$this->table_TD .= $balise == '' ? '' : '<' . $balise . '>';

			foreach($value as $line)
			{
				$this->table_TD .= '<tr>';

				foreach($line as $valuetd)
				{
					$this->table_TD .= '<td>' . $valuetd . '</td>';
				}

				$this->table_TD .= '</tr>';
			}

			$this->table_TD .= $balise == '' ? '' : '</' . $balise . '>';
		}

		/**
		 * Construit la caption du tableau
		 *
		 * @param array $tab Tableau des arguments de la balise
		 */
		private function captionBalise($tab)
		{
			$this->caption_Str = '<caption ';

			foreach($tab as $key => $value)
			{
				if($key == self::VALEUR)
				{
					$this->caption_Value = $value;
				}
				else
				{
					$this->caption_Str .= $key . '="' . $value . '" ';
				}
			}

			$this->caption_Str .= sprintf($this->caption_Format, $this->caption_For, $this->caption_Value);
		}
//</editor-fold>

//<editor-fold desc="Nav">

		/**
		 * Génère une balise <nav>
		 * et toutes les autres balises nécéssaires
		 * <ul>,<li>,<a>,<img> ...
		 *
		 * @param string $balise Balise à remplacer
		 * @param array  $tab    Tableau des arguments de la balise
		 */
		function navBalise($balise, $tab)
		{
			$this->nav_Str = '<nav ';

			foreach($tab as $key => $value)
			{
				$this->navSwitch($key, $value);
			}

			$l_Value = $this->nav_Value == '' ? $this->ul_Str : $this->nav_Value;

			$this->nav_Str .= sprintf($this->nav_Format, $l_Value);

			$this->remplacePage($balise, $this->nav_Str);
		}

		/**
		 * Répartit les différentes clés du tableau
		 * afin de créer la balise
		 *
		 * @param string $key   Clé du tableau
		 * @param mixed  $value Valeur du tableau
		 */
		private function navSwitch($key, $value)
		{
			switch($key)
			{
				case self::VALEUR:
					$this->nav_Value = $value;
					break;

				case self::UL:
					$this->ulBalise($value);
					break;

				default:
					$this->nav_Str .= $key . '="' . $value . '" ';
					break;
			}
		}
//</editor-fold>

//<editor-fold desc="Ul">

		/**
		 * Génère une balise <ul>
		 * et toutes les autres balises nécéssaires
		 * <li>,<a>,<img> ...
		 *
		 * @param array  $tab    Tableau des arguments de la balise
		 * @param string $balise Balise à remplacer (Optionnel)
		 */
		function ulBalise($tab, $balise = '')
		{
			foreach($tab as $Key => $Value)
			{
				$this->ul_Str .= '<ul ';

				foreach($Value as $key => $value)
				{
					$this->ulSwitch($key, $value);
				}

				$l_Value = $this->ul_Value == '' ? $this->li_Str : $this->ul_Value;

				$this->ul_Str .= sprintf($this->ul_Format, $l_Value);

				$this->li_Str = '';
			}

			if($balise != '')
			{
				$this->remplacePage($balise, $this->ul_Str);
			}
		}

		/**
		 * Répartit les différentes clés du tableau
		 * afin de créer la balise
		 *
		 * @param string $key   Clé du tableau
		 * @param mixed  $value Valeur du tableau
		 */
		private function ulSwitch($key, $value)
		{
			switch($key)
			{
				case self::VALEUR:
					$this->ul_Value = $value;
					break;

				case self::LI:
					$this->liBalise($value);
					break;

				default:
					$this->ul_Str .= $key . '="' . $value . '" ';
					break;
			}
		}
//</editor-fold>

//<editor-fold desc="Li">

		/**
		 * Génère une balise <li>
		 * et toutes les autres balises nécéssaires
		 * <a>,<img> ...
		 *
		 * @param array  $tab    Tableau des arguments de la balise
		 * @param string $balise Balise à remplacer (Optionnel)
		 */
		function liBalise($tab, $balise = '')
		{
			foreach($tab as $Key => $Value)
			{
				$this->li_Str .= '<li ';

				foreach($Value as $key => $value)
				{
					$this->liSwitch($key, $value);
				}

				if($this->li_Value == '')
				{
					if($this->a_Str == '')
					{
						$l_Value = $this->img_Str;
					}
					else
					{
						$l_Value = $this->a_Str;
					}
				}
				else
				{
					$l_Value = $this->li_Value;
				}

				$this->li_Str .= sprintf($this->li_Format, $l_Value);

				$this->a_Str   = '';
				$this->img_Str = '';
			}

			if($balise != '')
			{
				$this->remplacePage($balise, $this->li_Str);
			}
		}

		/**
		 * Répartit les différentes clés du tableau
		 * afin de créer la balise
		 *
		 * @param string $key   Clé du tableau
		 * @param mixed  $value Valeur du tableau
		 */
		private function liSwitch($key, $value)
		{
			switch($key)
			{
				case self::VALEUR:
					$this->li_Value = $value;
					break;

				case self::A:
					$this->aBalise($value);
					break;

				case self::IMAGE:
					$this->imgBalise($value);
					break;

				default:
					$this->li_Str .= $key . '="' . $value . '" ';
					break;
			}
		}

//</editor-fold>

//<editor-fold desc="a">

		/**
		 * Génère une balise <a>
		 * et toutes les autres balises nécéssaires
		 * <img> ...
		 *
		 * @param array  $tab    Tableau des arguments de la balise
		 * @param string $balise Balise à remplacer (Optionnel)
		 */
		function aBalise($tab, $balise = '')
		{
			$this->a_Str = '<a ';

			foreach($tab as $key => $value)
			{
				$this->aSwitch($key, $value);
			}

			$l_Value = $this->a_Value == '' ? $this->img_Str : $this->a_Value;

			$this->a_Str .= sprintf($this->a_Format, $l_Value);

			$this->a_Value = '';

			if($balise != '')
			{
				$this->remplacePage($balise, $this->a_Str);
			}
		}

		/**
		 * Répartit les différentes clés du tableau
		 * afin de créer la balise
		 *
		 * @param string $key   Clé du tableau
		 * @param mixed  $value Valeur du tableau
		 */
		private function aSwitch($key, $value)
		{
			switch($key)
			{
				case self::VALEUR:
					$this->a_Value = $value;
					break;

				case self::IMAGE:
					$this->imgBalise($value);
					break;

				default:
					$this->a_Str .= $key . '="' . $value . '" ';
					break;
			}
		}
//</editor-fold>

//<editor-fold desc="img">

		/**
		 * Génère une balise <a>
		 *
		 * @param array  $tab    Tableau des arguments de la balise
		 * @param string $balise Balise à remplacer (Optionnel)
		 */
		function imgBalise($tab, $balise = '')
		{
			$this->img_Str = '<img ';

			foreach($tab as $key => $value)
			{
				$this->img_Str .= $key . '="' . $value . '" ';
			}

			$this->img_Str .= '/>';

			if($balise != '')
			{
				$this->remplacePage($balise, $this->img_Str);
			}
		}

//</editor-fold>

//<editor-fold desc="TextArea">

		/**
		 * @param string $balise Balise à remplacer
		 * @param array  $tab    Tableau des arguments de la balise
		 */
		function textAreaBalise($balise, $tab)
		{
			$this->area_Str = '<textarea ';

			foreach($tab as $key => $value)
			{
				$this->textAreaSwitch($key, $value);
			}

			$this->area_Str .= '>';

			$this->area_Str = sprintf($this->area_Format, $this->label_Str, $this->area_Str, $this->area_Value);

			$this->remplacePage($balise, $this->area_Str);
		}

		/**
		 * Répartit les différentes clés du tableau
		 * afin de créer la balise
		 *
		 * @param string $key   Clé du tableau
		 * @param mixed  $value Valeur du tableau
		 */
		private function textAreaSwitch($key, $value)
		{
			switch($key)
			{
				case self::ID:
					$this->label_For = $value;
					$this->area_Str  .= $key . '="' . $value . '" ';
					break;

				case self::LIBELLE :
					$this->labelBalise($value);
					break;

				case self::VALEUR:
					$this->area_Value = $value;
					break;

				default :
					$this->area_Str .= $key . '="' . $value . '" ';
					break;
			}
		}
		//</editor-fold>

// // // <editor-fold desc="OLD CODE - Options avec BDD">
//        /*
//          ///////// commenter cas lier a la BDD
//          // Récupération des noms de champs
//          $tKeys = array_keys($tSQL[0]);
//
//          // Génération des listes de valeurs
//          $comboStr = '';
////          foreach ($tSQL as $tEnreg) {
////          $comboStr .= '<option '.$class.' value="'.$tEnreg[$tKeys[0]].'">'.mb_convert_encoding($tEnreg[$tKeys[1]],"UTF-8","ISO-8859-15").'</option>';
////          }
//         */
//
//           function tableForm($balise, $tSQL, $class = "")
//    {
//
//        // Récupération des noms de champs
//        $tKeys = array_keys($tSQL[0]);
//
//        // Génération de la classe CSS
//        $class = $class != "" ? ' class="' . $class . '" ' : " ";
//
//        // Génération du tableau
//        $tableStr = '<table ' . $class . '>';
//
//        // Entêtes
//        $tableStr .= '<tr>';
//        foreach ($tKeys as $name)
//        {
//            $tableStr .= '<th>' . $name . '</th>';
//        }
//        $tableStr .= '</tr>';
//
//        // Contenu du tableau
//        foreach ($tSQL as $tEnreg)
//        {
//            $tableStr .= '<tr>';
//            foreach ($tEnreg as $value)
//            {
//                $tableStr .= '<td>' . $value . '</td>';
//            }
//            $tableStr .= '</tr>';
//        }
//        $tableStr .= '</table>';
//
//        $this->remplacePage($balise, $tableStr);
//    }
//
		//</editor-fold>

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return "Erreur, classe template affiché";
		}
	}