<?php
	/**
	 * Created by PhpStorm.
	 * User: GuillaumeFixe
	 * Date: 11/06/2018
	 * Time: 21:03
	 */

	namespace Apel\Dll\Framework;

	ini_set('display_errors', 1);

	session_start();

	spl_autoload_register(function($class) {

		$class = str_replace("Apel\\", "", $class);
		$class = str_replace("\\", "/", $class);
		require_once($class . ".php");

	}
	);