<?php
	/**
	 * Created by PhpStorm.
	 * User: pythonirc
	 * Date: 03/07/18
	 * Time: 08:53
	 */

	namespace Apel;

	use Apel\Dll\Controleur\ControleurNews;
	use Apel\Dll\Framework\
	{
		cls_ConstruitTemplate, Config
	};

	return new class()
	{
		/** @var \Apel\Dll\Page\cls_rss $page */
		private $page;
		private $data;

		public function __construct($layout = false)
		{
			$this->getData();
			$this->generatePage($layout);
		}

		private function getData()
		{
			$this->data                   = array();
			$controleurNews               = new ControleurNews();
			$listeActualite               = $controleurNews->recupererActualite(0, false);
			$this->data['listeActualite'] = $listeActualite;
		}

		private function getActualiteXmlFormat()
		{
			if(count($this->data['listeActualite']) > 0)
			{
				$colonnes     = array_keys($this->data['listeActualite'][0]);
				$tableau      = '';
				$tableau      .= '<item>';
				$colonnesHtml = '';
				$nbColonne    = count($colonnes);
				$i            = 0;
				foreach($colonnes as $value)
				{
					$i++;
					$colonnesHtml .= '<th ' . (($i == $nbColonne) ? 'colspan="2"' : '') . ' >' . $value . '</th>';
				}
				$tableau .= '<thead><tr>' . $colonnesHtml . '</tr></thead><tbody>';

				foreach($this->data['listeActualite'] as $element)
				{
					$tableau .= '
						<title> ' . $element['titre'] . ' </title>
						<link>/news.php?id=' . $element['idactualite'] . '</link>
						<description> ' . $element['contenu'] . ' </description>
						<author>' . $element['nom'] . '  ' . $element['prenom'] . '</author>
						<category> Evenements </category>
						<pubDate>' . $element['dateactualite'] . '</pubDate>';

				}
				$tableau .= '</body><tfoot><tr>' . $colonnesHtml . '</tr></tfoot>';
				$tableau .= '</item>';
			}

			return $tableau;
		}

		private function generatePage(bool $layout)
		{
			//Initialisation du template
			$this->page = new cls_ConstruitTemplate(Config::getAdresse('RSS_XML'), $layout);

			$this->page->remplacePage('#liste_item#', $this->getActualiteXmlFormat());
			echo $this->afficherPage();
		}

		private function afficherPage()
		{
			//Affichage de la page
			return $this->page->getPage();
		}

		public function __toString()
		{
			return $this->afficherPage();
		}
	};