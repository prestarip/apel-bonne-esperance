<?php
	/**
	 * Created by PhpStorm.
	 * User: mathieu
	 * Date: 25/06/2018
	 * Time: 13:53
	 */

	namespace Apel;

	use Apel\Dll\Framework\Config;
	use Apel\Dll\Page\cls_Inscription;

	return new class()
	{
		/** @var \Apel\Dll\Page\cls_Inscription $page */
		private $page;

		public function __construct($layout = true)
		{
			$layout = (isset($_SESSION['noLayout']) && $_SESSION['noLayout'] <> null) ? false : $layout;

			$this->generatePage($layout);
		}

		private function generatePage(bool $layout)
		{
			$message = '';

			//Initialisation du template
			$this->page = new cls_Inscription(Config::getAdresse('INSCRIPTION'), $layout);

			if(isset($_SESSION['message']))
			{
				//Récupération du message contenu dans la variable de session
				$message = '<p>' . $_SESSION['message'] . '</p>';
			}

			//Appel des différents fonctions permettant de générer le code HTML
			$this->page->message($message);
			$this->page->formBegin();
			$this->page->mail();
			$this->page->mailConf();
			$this->page->password();
			$this->page->passwordConf();
			$this->page->civilite();
			$this->page->nom();
			$this->page->prenom();
			$this->page->date();
			$this->page->submit();
			$this->page->formEnd();

			if($layout)
			{
				echo $this->afficherPage();
			}

			//Vidage de la variable
			unset($_SESSION['message']);
			unset($_SESSION['valeursFormulaireInscription']);
		}

		private function afficherPage()
		{
			//Affichage de la page
			return $this->page->getPage();
		}

		public function __toString() : string
		{
			return $this->afficherPage();
		}
	};