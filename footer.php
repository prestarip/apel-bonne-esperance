<?php
	/**
	 * Created by PhpStorm.
	 * User: Guillaume
	 * Date: 29/06/2018
	 * Time: 00:30
	 */

	namespace Apel;

	use Apel\Dll\Framework\cls_ConstruitTemplate;
	use Apel\Dll\Framework\Config;

	return new class()
	{
		private $footer;

		public function __construct()
		{
			$this->generatePage();
		}

		private function generatePage()
		{
			// Génération de la page
			/** @var cls_ConstruitTemplate $pageFooter */
			$pageFooter = new cls_ConstruitTemplate(Config::getAdresse('FOOTER'), false);
			$pageFooter->remplacePage('#message#', ((isset($_SESSION['message']) && !empty($_SESSION['message'])) ? '
				<div class="message">
					<div class="closeMessage" onclick="MonAppli.closeMessageInfo(this, event)">X</div>
					<p >' . $_SESSION['message'] . '</p>
				</div>' : '')
			);
			unset($_SESSION['message']);
			//Affichage du contenu
			$this->footer = $pageFooter->getPage();
		}

		/**
		 * @return string
		 */
		public function __toString()
		{
			return $this->footer;
		}
	};