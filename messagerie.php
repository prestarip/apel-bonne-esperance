<?php
	/**
	 * Created by PhpStorm.
	 * User: Vincent
	 * Date: 12/06/2018
	 * Time: 13:22
	 */

	//Déclaration du namespace
	namespace Apel;

	$page = file_get_contents('Templates/header.html');

	$page .= file_get_contents('Templates/messagerie.html');
	$page = str_replace('#titre#', 'Messagerie', $page);
	$page = str_replace('#menu_edition_message#', 'Bouton d\'edition de message', $page);
	$page = str_replace('#selection_dossier#', 'Cadre avec la liste des dossiers de la messagerie', $page);
	$page = str_replace('#selection_message#', 'Cadre avec la liste des messages de l\'utilisateur', $page);

	$page .= file_get_contents('Templates/footer.html');

	echo $page;


