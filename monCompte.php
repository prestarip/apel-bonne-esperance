<?php
	/**
	 * Created by PhpStorm.
	 * User: mathieu
	 * Date: 31/07/2018
	 * Time: 11:20
	 */

	namespace Apel;

	use Apel\Dll\Framework\Config;
	use Apel\Dll\Page\cls_MonCompte;

	return new class()
	{
		/** @var \Apel\Dll\Page\cls_Inscription $page */
		private $page;

		public function __construct($layout = true)
		{
			$layout = (isset($_SESSION['noLayout']) && $_SESSION['noLayout'] <> null) ? false : $layout;

			$this->generatePage($layout);
		}

		private function generatePage(bool $layout)
		{
			//$message = '';

			//Initialisation du template
			$this->page = new cls_MonCompte(Config::getAdresse('MONCOMPTE'), $layout);
			$this->page->remplacePage('#begin_section#', (($layout) ? '<section>' : ''));

			/*
						if(isset($_SESSION['message']))
						{
							//Récupération du message contenu dans la variable de session
							$message = $_SESSION['message'];
						}
			*/
			//Appel des différents fonctions permettant de générer le code HTML
			//$this->page->message($message);

			$this->page->image();
			$this->page->titre();
			$this->page->formBegin();
			$this->page->mail();
			$this->page->mailConf();
			$this->page->password();
			$this->page->passwordConf();
			$this->page->civilite();
			$this->page->nom();
			$this->page->prenom();
			$this->page->date();
			$this->page->submit();
			$this->page->formEnd();
			$this->page->formBeginAvatar();
			$this->page->chemin();
			$this->page->submitAvatar();
			$this->page->formEnd();
			$this->page->transaction();

			$this->page->remplacePage('#end_section#', (($layout) ? '</section>' : ''));
			if($layout)
			{
				echo $this->afficherPage();
			}
/*
			//Vidage de la variable
			unset($_SESSION['message']);
			unset($_SESSION['email']);
			unset($_SESSION['emailConf']);
			unset($_SESSION['mdp']);
			unset($_SESSION['mdpConf']);
			unset($_SESSION['idCiv']);
			unset($_SESSION['nom']);
			unset($_SESSION['prenom']);
			unset($_SESSION['dateNaissance']);
*/
		}

		private function afficherPage()
		{
			//Affichage de la page
			return $this->page->getPage();
		}

		public function __toString() : string
		{
			return $this->afficherPage();
		}
	};