<?php
	/**
	 * Created by PhpStorm.
	 * User: Vincent
	 * Date: 12/06/2018
	 * Time: 14:10
	 */

	//Déclaration du namespace
	namespace Apel;

	use Apel\Dll\Framework\cls_ConstruitTemplate;
	use Apel\Dll\Framework\Config;

	return new class()
	{
		private $page;

		public function __construct($layout = true)
		{
			$this->generatePage($layout);
		}

		private function generatePage(bool $layout)
		{
			//Initialisation du template
			$this->page = new cls_ConstruitTemplate(Config::getAdresse('BILAN_FINANCIER'), $layout);
			$this->page->remplacePage('#begin_section#', (($layout) ? '<section>' : ''));

			$this->page->remplacePage('#title#', 'Bilan financier');

			$this->page->remplacePage('#tableaudepenses#', 'tableau des depenses');
			$this->page->remplacePage('#tableauRecettes#', 'Tableau des recettes');
			$this->page->remplacePage('#resulatBilan#', '200 euros');

			$this->page->remplacePage('#end_section#', (($layout) ? '</section>' : ''));

			if($layout)
			{
				echo $this->page->getPage();
			}
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return $this->page;
		}
	};