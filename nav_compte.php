<?php
	/**
	 * Created by PhpStorm.
	 * User: Guillaume
	 * Date: 31/08/2018
	 * Time: 11:04
	 */

	namespace Apel;

	use Apel\Dll\Framework\cls_ConstruitTemplate;
	use Apel\Dll\Framework\Config;

	return new class()
	{
		/** @var cls_ConstruitTemplate $page */
		private $page;

		public function __construct($layout = false)
		{
			$layout = (isset($_SESSION['noLayout']) && $_SESSION['noLayout'] <> null) ? false : $layout;
			$this->generatePage($layout);
		}

		private function generatePage(bool $layout)
		{
			//Initialisation du template
			$this->page = new cls_ConstruitTemplate(Config::getAdresse('NAV_COMPTE'), $layout);


			if($layout)
			{
				echo $this->afficherPage();
			}
		}

		private function afficherPage()
		{
			//Affichage de la page
			return $this->page->getPage();
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return $this->afficherPage();
		}
	};