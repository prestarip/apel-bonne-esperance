<?php
	/**
	 * Created by PhpStorm.
	 * User: Vincent
	 * Date: 12/06/2018
	 * Time: 13:33
	 */

	//Déclaration du namespace
	namespace Apel;

	$page = file_get_contents('Templates/header.html');

	$page .= file_get_contents('Templates/paiement.html');
	$page = str_replace('#titre#', 'Paiements', $page);
	$page = str_replace('#cotisation#', 'formulaire paypal pour payer sa cotisation', $page);
	$page = str_replace('#don#', 'formulaire paypal pour faire un don', $page);

	$page .= file_get_contents('Templates/footer.html');

	echo $page;