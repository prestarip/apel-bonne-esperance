<?php

	/**
	 * Created by PhpStorm.
	 * User: Vincent
	 * Date: 12/06/2018
	 * Time: 12:42
	 */

	namespace Apel;

	use Apel\Dll\Framework\Config;
	use Apel\Dll\Page\cls_Login;

	return new class()
	{
		/** @var \Apel\Dll\Page\cls_Login $page */
		private $page;

		public function __construct($layout = true)
		{
			$layout = (isset($_SESSION['noLayout']) && $_SESSION['noLayout'] <> null) ? false : $layout;
			$this->generatePage($layout);
		}

		private function generatePage(bool $layout)
		{
			$message = '';

			//Initialisation du template
			$this->page = new cls_Login(Config::getAdresse('LOGIN'), $layout);

			//Vérification que la variable de session $_SESSION['loginOK'] existe
			//Si elle existe c'est qu'elle contient un message
			if(isset($_SESSION['loginOK']) && isset($_SESSION['message']))
			{
				//Récupération du message contenu dans la variable de session
				$message = $_SESSION['message'];
			}

			//Appel des différents fonctions permettant de générer le code HTML
			$this->page->remplacePage('#begin_section#', (($layout) ? '<section>' : ''));
			$this->page->message($message);
			$this->page->formBegin();
			$this->page->login();
			$this->page->password();
			$this->page->submit();
			$this->page->formEnd();
			$this->page->remplacePage('#end_section#', (($layout) ? '</section>' : ''));

			if($layout)
			{
				echo $this->afficherPage();
			}

			//Vidage de la variable de session $_SESSION['loginOK']
			//Afin d'éviter de réafficher le message si actualisation de la page
			unset($_SESSION['loginOK']);
		}

		private function afficherPage()
		{
			//Affichage de la page
			return $this->page->getPage();
		}

		public function __toString() : string
		{
			return $this->afficherPage();
		}
	};